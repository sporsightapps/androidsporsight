package org.tensorflow.lite.examples.posenet.lib

import android.content.Context
import android.graphics.*
//import android.media.ExifInterface
import android.os.SystemClock
import android.util.Log
import androidx.core.graphics.ColorUtils
import androidx.exifinterface.media.ExifInterface
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.GpuDelegate
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import kotlin.random.Random

data class ModelExecutionResult(
        val bitmapResult: Bitmap,
        val bitmapOriginal: Bitmap,
        val bitmapMaskOnly: Bitmap,
        val executionLog: String,
        val itemsFound: Set<Int>
)

abstract class ImageUtils {
    companion object {

        /**
         * Helper function used to convert an EXIF orientation enum into a transformation matrix
         * that can be applied to a bitmap.
         *
         * @param orientation - One of the constants from [ExifInterface]
         */
        private fun decodeExifOrientation(orientation: Int): Matrix {
            val matrix = Matrix()

            // Apply transformation corresponding to declared EXIF orientation
            when (orientation) {
                ExifInterface.ORIENTATION_NORMAL, ExifInterface.ORIENTATION_UNDEFINED -> Unit
                ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90F)
                ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180F)
                ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270F)
                ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> matrix.postScale(-1F, 1F)
                ExifInterface.ORIENTATION_FLIP_VERTICAL -> matrix.postScale(1F, -1F)
                ExifInterface.ORIENTATION_TRANSPOSE -> {
                    matrix.postScale(-1F, 1F)
                    matrix.postRotate(270F)
                }
                ExifInterface.ORIENTATION_TRANSVERSE -> {
                    matrix.postScale(-1F, 1F)
                    matrix.postRotate(90F)
                }

                // Error out if the EXIF orientation is invalid
                else -> throw IllegalArgumentException("Invalid orientation: $orientation")
            }

            // Return the resulting matrix
            return matrix
        }

        /**
         * sets the Exif orientation of an image.
         * this method is used to fix the exit of pictures taken by the camera
         *
         * @param filePath - The image file to change
         * @param value - the orientation of the file
         */
        fun setExifOrientation(
                filePath: String,
                value: String
        ) {
            val exif = ExifInterface(filePath)
            exif.setAttribute(
                    ExifInterface.TAG_ORIENTATION, value
            )
            exif.saveAttributes()
        }

        /** Transforms rotation and mirroring information into one of the [ExifInterface] constants */
        fun computeExifOrientation(rotationDegrees: Int, mirrored: Boolean) = when {
            rotationDegrees == 0 && !mirrored -> ExifInterface.ORIENTATION_NORMAL
            rotationDegrees == 0 && mirrored -> ExifInterface.ORIENTATION_FLIP_HORIZONTAL
            rotationDegrees == 180 && !mirrored -> ExifInterface.ORIENTATION_ROTATE_180
            rotationDegrees == 180 && mirrored -> ExifInterface.ORIENTATION_FLIP_VERTICAL
            rotationDegrees == 270 && mirrored -> ExifInterface.ORIENTATION_TRANSVERSE
            rotationDegrees == 90 && !mirrored -> ExifInterface.ORIENTATION_ROTATE_90
            rotationDegrees == 90 && mirrored -> ExifInterface.ORIENTATION_TRANSPOSE
            rotationDegrees == 270 && mirrored -> ExifInterface.ORIENTATION_ROTATE_270
            rotationDegrees == 270 && !mirrored -> ExifInterface.ORIENTATION_TRANSVERSE
            else -> ExifInterface.ORIENTATION_UNDEFINED
        }

        /**
         * Decode a bitmap from a file and apply the transformations described in its EXIF data
         *
         * @param file - The image file to be read using [BitmapFactory.decodeFile]
         */
        fun decodeBitmap(file: File): Bitmap {
            // First, decode EXIF data and retrieve transformation matrix
            val exif = ExifInterface(file.absolutePath)
            val transformation =
                    decodeExifOrientation(
                            exif.getAttributeInt(
                                    ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_ROTATE_90
                            )
                    )

            // Read bitmap using factory methods, and transform it using EXIF data
            val options = BitmapFactory.Options()
            val bitmap = BitmapFactory.decodeFile(file.absolutePath, options)
            return Bitmap.createBitmap(
                    BitmapFactory.decodeFile(file.absolutePath),
                    0, 0, bitmap.width, bitmap.height, transformation, true
            )
        }

        fun scaleBitmapAndKeepRatio(
                targetBmp: Bitmap,
                reqHeightInPixels: Int,
                reqWidthInPixels: Int
        ): Bitmap {
            if (targetBmp.height == reqHeightInPixels && targetBmp.width == reqWidthInPixels) {
                return targetBmp
            }
            val matrix = Matrix()
            matrix.setRectToRect(
                    RectF(
                            0f, 0f,
                            targetBmp.width.toFloat(),
                            targetBmp.width.toFloat()
                    ),
                    RectF(
                            0f, 0f,
                            reqWidthInPixels.toFloat(),
                            reqHeightInPixels.toFloat()
                    ),
                    Matrix.ScaleToFit.FILL
            )
            return Bitmap.createBitmap(
                    targetBmp, 0, 0,
                    targetBmp.width,
                    targetBmp.width, matrix, true
            )
        }

        fun bitmapToByteBuffer(
                bitmapIn: Bitmap,
                width: Int,
                height: Int,
                mean: Float = 0.0f,
                std: Float = 255.0f
        ): ByteBuffer {
            val bitmap = scaleBitmapAndKeepRatio(bitmapIn, width, height)
            val inputImage = ByteBuffer.allocateDirect(1 * width * height * 3 * 4)
            inputImage.order(ByteOrder.nativeOrder())
            inputImage.rewind()

            val intValues = IntArray(width * height)
            bitmap.getPixels(intValues, 0, width, 0, 0, width, height)
            var pixel = 0
            for (y in 0 until height) {
                for (x in 0 until width) {
                    val value = intValues[pixel++]

                    // Normalize channel values to [-1.0, 1.0]. This requirement varies by
                    // model. For example, some models might require values to be normalized
                    // to the range [0.0, 1.0] instead.
                    inputImage.putFloat(((value shr 16 and 0xFF) - mean) / std)
                    inputImage.putFloat(((value shr 8 and 0xFF) - mean) / std)
                    inputImage.putFloat(((value and 0xFF) - mean) / std)
                }
            }

            inputImage.rewind()
            return inputImage
        }

        fun createEmptyBitmap(imageWidth: Int, imageHeigth: Int, color: Int = 0): Bitmap {
            val ret = Bitmap.createBitmap(imageWidth, imageHeigth, Bitmap.Config.RGB_565)
            if (color != 0) {
                ret.eraseColor(color)
            }
            return ret
        }
    }
}

class ImageSegmentationModelExecutor(
        context: Context,
        private var useGPU: Boolean = false
) {
    private var gpuDelegate: GpuDelegate? = null

    private val segmentationMasks: ByteBuffer
    private val interpreter: Interpreter

    private var fullTimeExecutionTime = 0L
    private var preprocessTime = 0L
    private var imageSegmentationTime = 0L
    private var maskFlatteningTime = 0L

    private var numberThreads = 4

    init {
        Log.d("deeplab", "test")
        interpreter = getInterpreter(context, imageSegmentationModel, useGPU)
        segmentationMasks = ByteBuffer.allocateDirect(1 * imageSize * imageSize * NUM_CLASSES * 4)
        segmentationMasks.order(ByteOrder.nativeOrder())
    }

    fun execute(data: Bitmap): ModelExecutionResult {
        try {
            fullTimeExecutionTime = SystemClock.uptimeMillis()

            preprocessTime = SystemClock.uptimeMillis()
            val scaledBitmap =
                    ImageUtils.scaleBitmapAndKeepRatio(
                            data,
                            imageSize, imageSize
                    )

            val contentArray =
                    ImageUtils.bitmapToByteBuffer(
                            scaledBitmap,
                            imageSize,
                            imageSize,
                            IMAGE_MEAN,
                            IMAGE_STD
                    )
            preprocessTime = SystemClock.uptimeMillis() - preprocessTime

            imageSegmentationTime = SystemClock.uptimeMillis()
            interpreter.run(contentArray, segmentationMasks)
            imageSegmentationTime = SystemClock.uptimeMillis() - imageSegmentationTime
            Log.d(TAG, "Time to run the model $imageSegmentationTime")

            maskFlatteningTime = SystemClock.uptimeMillis()
            val (maskImageApplied, maskOnly, itensFound) =
                    convertBytebufferMaskToBitmap(
                            segmentationMasks, imageSize, imageSize, scaledBitmap,
                            segmentColors
                    )
            maskFlatteningTime = SystemClock.uptimeMillis() - maskFlatteningTime
            Log.d(TAG, "Time to flatten the mask result $maskFlatteningTime")

            fullTimeExecutionTime = SystemClock.uptimeMillis() - fullTimeExecutionTime
            Log.d(TAG, "Total time execution $fullTimeExecutionTime")

            return ModelExecutionResult(
                    maskImageApplied,
                    scaledBitmap,
                    maskOnly,
                    formatExecutionLog(),
                    itensFound
            )
        } catch (e: Exception) {
            val exceptionLog = "something went wrong: ${e.message}"
            Log.d(TAG, exceptionLog)

            val emptyBitmap =
                    ImageUtils.createEmptyBitmap(
                            imageSize,
                            imageSize
                    )
            return ModelExecutionResult(
                    emptyBitmap,
                    emptyBitmap,
                    emptyBitmap,
                    exceptionLog,
                    HashSet(0)
            )
        }
    }

    // base: https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/java/demo/app/src/main/java/com/example/android/tflitecamerademo/ImageClassifier.java
    @Throws(IOException::class)
    private fun loadModelFile(context: Context, modelFile: String): MappedByteBuffer {
        val fileDescriptor = context.assets.openFd(modelFile)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength
        val retFile = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
        fileDescriptor.close()
        return retFile
    }

    @Throws(IOException::class)
    private fun getInterpreter(
            context: Context,
            modelName: String,
            useGpu: Boolean = false
    ): Interpreter {
        val tfliteOptions = Interpreter.Options()
        tfliteOptions.setNumThreads(numberThreads)

        gpuDelegate = null
        if (useGpu) {
            gpuDelegate = GpuDelegate()
            tfliteOptions.addDelegate(gpuDelegate)
        }

        return Interpreter(loadModelFile(context, modelName), tfliteOptions)
    }

    private fun formatExecutionLog(): String {
        val sb = StringBuilder()
        sb.append("Input Image Size: $imageSize x $imageSize\n")
        sb.append("GPU enabled: $useGPU\n")
        sb.append("Number of threads: $numberThreads\n")
        sb.append("Pre-process execution time: $preprocessTime ms\n")
        sb.append("Model execution time: $imageSegmentationTime ms\n")
        sb.append("Mask flatten time: $maskFlatteningTime ms\n")
        sb.append("Full execution time: $fullTimeExecutionTime ms\n")
        return sb.toString()
    }

    fun close() {
        interpreter.close()
        if (gpuDelegate != null) {
            gpuDelegate!!.close()
        }
    }

    private fun convertBytebufferMaskToBitmap(
            inputBuffer: ByteBuffer,
            imageWidth: Int,
            imageHeight: Int,
            backgroundImage: Bitmap,
            colors: IntArray
    ): Triple<Bitmap, Bitmap, Set<Int>> {
        val conf = Bitmap.Config.ARGB_8888
        val maskBitmap = Bitmap.createBitmap(imageWidth, imageHeight, conf)
        val resultBitmap = Bitmap.createBitmap(imageWidth, imageHeight, conf)
        val scaledBackgroundImage =
                ImageUtils.scaleBitmapAndKeepRatio(
                        backgroundImage,
                        imageWidth,
                        imageHeight
                )
        val mSegmentBits = Array(imageWidth) { IntArray(imageHeight) }
        val itemsFound = HashSet<Int>()
        inputBuffer.rewind()

        for (y in 0 until imageHeight) {
            for (x in 0 until imageWidth) {
                var maxVal = 0f
                mSegmentBits[x][y] = 0

                for (c in 0 until NUM_CLASSES) {
                    val value = inputBuffer
                            .getFloat((y * imageWidth * NUM_CLASSES + x * NUM_CLASSES + c) * 4)
                    if (c == 0 || value > maxVal) {
                        maxVal = value
                        mSegmentBits[x][y] = c
                    }
                }

                itemsFound.add(mSegmentBits[x][y])
                val newPixelColor = ColorUtils.compositeColors(
                        colors[mSegmentBits[x][y]],
                        scaledBackgroundImage.getPixel(x, y)
                )
                resultBitmap.setPixel(x, y, newPixelColor)
                maskBitmap.setPixel(x, y, colors[mSegmentBits[x][y]])
            }
        }

        return Triple(resultBitmap, maskBitmap, itemsFound)
    }

    companion object {

        private const val TAG = "ImageSegmentationMExec"
        private const val imageSegmentationModel = "deeplabv3_257_mv_gpu.tflite"
        private const val imageSize = 257
        const val NUM_CLASSES = 21
        private const val IMAGE_MEAN = 128.0f
        private const val IMAGE_STD = 128.0f

        val segmentColors = IntArray(NUM_CLASSES)
        val labelsArrays = arrayOf(
                "background", "aeroplane", "bicycle", "bird", "boat", "bottle", "bus",
                "car", "cat", "chair", "cow", "dining table", "dog", "horse", "motorbike",
                "person", "potted plant", "sheep", "sofa", "train", "tv"
        )

        init {

            val random = Random(System.currentTimeMillis())
            segmentColors[0] = Color.TRANSPARENT
            for (i in 1 until NUM_CLASSES) {
                segmentColors[i] = Color.argb(
                        (128),
                        getRandomRGBInt(
                                random
                        ),
                        getRandomRGBInt(
                                random
                        ),
                        getRandomRGBInt(
                                random
                        )
                )
            }
        }

        private fun getRandomRGBInt(random: Random) = (255 * random.nextFloat()).toInt()
    }
}