package com.dev.sporsightmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.entities.video.VideoDBHelper;
import com.dev.sporsightmobile.services.BlobStorageGetVideos;
import com.dev.sporsightmobile.services.BlobStorageService;
import com.microsoft.azure.storage.blob.ListBlobItem;
//import com.microsoft.azure.storage.blob.ListBlobItem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class VideoGallery extends FragmentActivity {



    public static ArrayList<String> listOfVideos(Activity activity, Context context, String category) throws ExecutionException, InterruptedException {
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;
        ArrayList<String> listofAllVideos = new ArrayList<String>();
        String absolutePathOfVideo;
        VideoDBHelper videoDBHelper;

        uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.BUCKET_DISPLAY_NAME};

        String orderBy = MediaStore.MediaColumns.DATE_TAKEN;

        if(category.contains("Cloud"))
        {
            BlobStorageGetVideos task = new BlobStorageGetVideos(activity);

            List<ListBlobItem> blobItems = (List<ListBlobItem>) task.execute().get();

            for (ListBlobItem blobItem : blobItems) {
                listofAllVideos.add(blobItem.getUri().toString());
            }

            return listofAllVideos;


        }
        else if(category.contains("Annotations"))
        {
            cursor = context.getContentResolver().query(uri, projection, MediaStore.Images.Media.DATA + " like ? ", new String[] {"%Annotations%"}, orderBy+" DESC");
        }
        else if(category.contains("Recordings"))
        {

            cursor = context.getContentResolver().query(uri, projection, MediaStore.Images.Media.DATA + " like ? ", new String[] {"%Recordings%"}, orderBy+" DESC");
        }
        else
        {
            cursor = context.getContentResolver().query(uri, projection, null, null, orderBy+" DESC");
        }

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

        while(cursor.moveToNext())
        {
            absolutePathOfVideo = cursor.getString(column_index_data);

            listofAllVideos.add(absolutePathOfVideo);

          //  Log.d("videopath", absolutePathOfVideo);
        }



        return listofAllVideos;

    }

}
