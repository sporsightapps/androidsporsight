package com.dev.sporsightmobile.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.fragment.app.FragmentActivity;

import com.dev.sporsightmobile.R;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import static android.webkit.ConsoleMessage.MessageLevel.LOG;

public class CompareVideosActivity extends FragmentActivity {

    Uri video1;
    Uri video2;

    private ExoPlayer exoPlayer1;
    private PlayerView playerView1;
    private PlayerControlView controls1;
    private Boolean playWhenReady1;
    private long playbackPosition1;
    private int currentWindow1;
    public static long totalDuration1;
    public static long currentPosition1;

    private ExoPlayer exoPlayer2;
    private PlayerView playerView2;
    private PlayerControlView controls2;
    private Boolean playWhenReady2;
    private long playbackPosition2;
    private int currentWindow2;
    public static long totalDuration2;
    public static long currentPosition2;
    public static boolean isPlaying1;
    public static boolean isPlaying2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        Log.v("TAG", "Initializing");
        setContentView(R.layout.activity_compare);
        Log.v("TAG", "Initialized");

        isPlaying1 = false;
        isPlaying2 = false;

        video1 = Uri.parse(getIntent().getStringExtra("Video1"));
        video2 = Uri.parse(getIntent().getStringExtra("Video2"));

        playerView1 = findViewById(R.id.video1);
        playerView2 = findViewById(R.id.video2);

        Log.v("video1", video1.toString());
        Log.v("video2", video2.toString());

        controls1 = findViewById(R.id.controls1);

        playerView1.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);

        controls2 = findViewById(R.id.controls2);

        LinearLayout topbar1 = controls1.findViewById(R.id.annotation_positionToggle);
        topbar1.setVisibility(View.INVISIBLE);
        LinearLayout topbar2 = controls2.findViewById(R.id.annotation_positionToggle);
        topbar2.setVisibility(View.INVISIBLE);

        ImageButton trim1 = controls1.findViewById(R.id.trim);
        trim1.setVisibility(View.INVISIBLE);
        ImageButton trim2 = controls2.findViewById(R.id.trim);
        trim2.setVisibility(View.INVISIBLE);

        ImageButton share1 = controls1.findViewById(R.id.share);
        share1.setVisibility(View.INVISIBLE);
        ImageButton share2 = controls2.findViewById(R.id.share);
        share2.setVisibility(View.INVISIBLE);

//        ImageButton save1 = controls1.findViewById(R.id.save);
//        save1.setVisibility(View.INVISIBLE);
//
//        ImageButton save2 = controls2.findViewById(R.id.save);
//        save2.setVisibility(View.INVISIBLE);

        ImageButton play1 = controls1.findViewById(R.id.exo_play);
        ImageButton play2 = controls2.findViewById(R.id.exo_play);


        playerView2.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);

        initializeExoPlayers();
        startExoPlayers();

    }

    private void initializeExoPlayers() {
        if (exoPlayer1 == null) {

            //Always show Portrait Mode
           // ((Activity)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

          //  Log.v(TAG, "Initializing ExoPlayer");
            exoPlayer1 = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), new DefaultTrackSelector());
            playerView1.setPlayer(exoPlayer1);
            controls1.setPlayer(exoPlayer1);

            playerView1.setRotation(0);

            playerView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(controls1.getVisibility() == View.VISIBLE)
                    {
                        controls1.setVisibility(View.GONE);
                    }
                    else
                    {
                        controls1.setVisibility(View.VISIBLE);
                    }
                }
            } );

            //Setting up the Seek Parameters to use the default seek configuration
//            SeekParameters seekParameters = new SeekParameters(null,null);
            exoPlayer1.setSeekParameters(null);


            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), "Sporsight"));
            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(video1);
            exoPlayer1.prepare(videoSource);
            playWhenReady1 = true;
            exoPlayer1.setPlayWhenReady(playWhenReady1);
            exoPlayer1.seekTo(currentWindow1, playbackPosition1);


            exoPlayer1.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                   exoPlayer2.setPlayWhenReady(playWhenReady);

                    // Each time we scroll through a video, we get the current position we are in in milliseconds
                    // and we set it to this.currentPosition
                    // Likewise we always retrieve the total duration because there are multiple instances where we need it
                    setCurrentPosition1(exoPlayer1.getCurrentPosition());
                    setTotalDuration1(exoPlayer1.getDuration());
                }
            });

        }

        if (exoPlayer2 == null) {

            //Always show Portrait Mode
            // ((Activity)).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

            //  Log.v(TAG, "Initializing ExoPlayer");
            exoPlayer2 = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), new DefaultTrackSelector());
            playerView2.setPlayer(exoPlayer2);
            controls2.setPlayer(exoPlayer2);

            playerView2.setRotation(0);

            playerView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(controls2.getVisibility() == View.VISIBLE)
                    {
                        controls2.setVisibility(View.GONE);
                    }
                    else
                    {
                        controls2.setVisibility(View.VISIBLE);
                    }
                }
            } );

            //Setting up the Seek Parameters to use the default seek configuration
//            SeekParameters seekParameters = new SeekParameters(null,null);
            exoPlayer2.setSeekParameters(null);


            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), "Sporsight"));
            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(video2);
            exoPlayer2.prepare(videoSource);
            playWhenReady2 = true;
            exoPlayer2.setPlayWhenReady(playWhenReady2);
            exoPlayer2.seekTo(currentWindow2, playbackPosition2);


            exoPlayer2.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                    exoPlayer1.setPlayWhenReady(playWhenReady);

                    // Each time we scroll through a video, we get the current position we are in in milliseconds
                    // and we set it to this.currentPosition
                    // Likewise we always retrieve the total duration because there are multiple instances where we need it
                    setCurrentPosition1(exoPlayer2.getCurrentPosition());
                    setTotalDuration1(exoPlayer2.getDuration());
                }
            });
        }
    }

    public boolean isPlaying(ExoPlayer exoPlayer) {
        return exoPlayer.getPlayWhenReady();
    }

    private void startExoPlayers() {
        playWhenReady1 = true;
        exoPlayer1.setPlayWhenReady(false);

        playWhenReady2 = true;
        exoPlayer2.setPlayWhenReady(false);
    }


    public void setCurrentPosition1(long currentPosition) {
        this.currentPosition1 = currentPosition;
    }

    public long getCurrentPosition1() {
        return this.currentPosition1;
    }

    public void setTotalDuration1(long totalDuration) {
        this.totalDuration1 = totalDuration;
    }

    public long getTotalDuration1() {
        return this.totalDuration1;
    }

    public void setCurrentPosition2(long currentPosition) {
        this.currentPosition2 = currentPosition;
    }

    public long getCurrentPosition2() {
        return this.currentPosition2;
    }

    public void setTotalDuration2(long totalDuration) {
        this.totalDuration2 = totalDuration;
    }

    public long getTotalDuration2() {
        return this.totalDuration2;
    }


}
