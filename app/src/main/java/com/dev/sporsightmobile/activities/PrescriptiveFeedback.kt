package com.dev.sporsightmobile.activities

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Selection
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dev.sporsightmobile.R
import com.dev.sporsightmobile.activities.MyRecyclerViewAdapter.ItemClickListener
import com.dev.sporsightmobile.fragments.AnnotationFragment
import com.dev.sporsightmobile.services.PrescriptiveFeedbackService
import com.google.android.material.internal.ContextUtils.getActivity

class PrescriptiveFeedback : AppCompatActivity(), ItemClickListener {
  var adapter: MyRecyclerViewAdapter? = null
  private val mData: List<String> = PrescriptiveFeedbackService.prescriptiveFeedbackResult!!

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_prescriptive_feedback)

    // set up view
    val emptyView = findViewById<TextView>(R.id.empty_view)
    val recyclerView = findViewById<RecyclerView>(R.id.prescriptive_feedback_view)
    recyclerView.layoutManager = LinearLayoutManager(this)
    adapter = MyRecyclerViewAdapter(this, mData)
    adapter!!.setClickListener(this)
    recyclerView.adapter = adapter

    if (mData.isEmpty()) {
      recyclerView.visibility = View.GONE
      emptyView.visibility = View.VISIBLE
    }
    else {
      recyclerView.visibility = View.VISIBLE
      emptyView.visibility = View.GONE
    }
  }

  override fun onItemClick(view: View?, position: Int) {
    Toast.makeText(this, "You clicked " + adapter!!.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show()
  }

  override fun onBackPressed() {
    AnnotationFragment.seekToTime = 0
    super.onBackPressed()
  }
}

class MyRecyclerViewAdapter internal constructor(context: Context?, data: List<String>) : RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder>() {
  private val mData: List<String> = data
  private val mInflater: LayoutInflater = LayoutInflater.from(context)
  private var mClickListener: ItemClickListener? = null
  private var mContext = context

  // inflates the row layout from xml when needed
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view: View = mInflater.inflate(R.layout.prescriptive_feedback_text_view, parent, false)
    return ViewHolder(view)
  }

  // binds the data to the TextView in each row
  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    val feedback = mData[position]
    holder.myTextView.text = feedback
    val timeStamps = findTimeStamps(feedback)
    val timeStampPairs = createTimeStampPairs(timeStamps)
    makeLinks(*timeStampPairs.toTypedArray(), textView = holder.myTextView)
  }

  // total number of rows
  override fun getItemCount(): Int {
    return mData.size
  }


  // stores and recycles views as they are scrolled off screen
  inner class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    var myTextView: TextView
    override fun onClick(view: View?) {
      if (mClickListener != null) mClickListener!!.onItemClick(view, adapterPosition)
    }

    init {
      myTextView = itemView.findViewById(R.id.prescriptive_feedback_text)
      itemView.setOnClickListener(this)
    }
  }

  // convenience method for getting data at click position
  fun getItem(id: Int): String {
    return mData[id]
  }

  // allows clicks events to be caught
  fun setClickListener(itemClickListener: ItemClickListener?) {
    mClickListener = itemClickListener
  }

  // parent activity will implement this method to respond to click events
  interface ItemClickListener {
    fun onItemClick(view: View?, position: Int)
  }

  // data is passed into the constructor
  init {
    Log.d(this.toString(), "PrescriptiveFeedback result ${PrescriptiveFeedbackService.prescriptiveFeedbackResult}")
  }

  fun makeLinks(vararg links: Pair<String, View.OnClickListener>, textView: TextView) {
    val spannableString = SpannableString(textView.text)
    for (link in links) {
      val clickableSpan = object : ClickableSpan() {
        override fun onClick(view: View) {
          Selection.setSelection((view as TextView).text as Spannable, 0)
          view.invalidate()
          link.second.onClick(view)
        }
      }
      val startIndexOfLink = spannableString.toString().indexOf(link.first)
      spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
              Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
    textView.movementMethod = LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
    textView.setText(spannableString, TextView.BufferType.SPANNABLE)
  }

  // returns list of timestamp strings
  fun findTimeStamps(feedback: String) : List<String> {

    // colon is anchor point, the first number starts 2 indices after that
    val listStart = feedback.indexOf(':') + 2

    // obtain each timestamp
    return feedback.substring(listStart).split(",").map { it -> it.trim() }
  }

  fun timeStampToMillisecond(timeStamp: String): Int {
    val seconds = timeStamp.substringBefore('.').toInt()
    val milliseconds = timeStamp.substringAfter('.').toInt() + (seconds * 1000)
    return milliseconds;
  }


  // attach time stamps to onclick handlers
  @SuppressLint("RestrictedApi")
  fun createTimeStampPairs(timeStamps: List<String>) : List<Pair<String, View.OnClickListener>> {
    val activity = getActivity(this.mContext)
    return timeStamps.map { timeStamp -> Pair(timeStamp, View.OnClickListener {
      AnnotationFragment.seekToTime = timeStampToMillisecond(timeStamp.substringBefore(" "))
      activity!!.finish();
    })}
  }
}