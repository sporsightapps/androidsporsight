package com.dev.sporsightmobile.activities;


import android.app.Application;
import android.content.res.Configuration;

import com.microsoft.identity.client.AuthenticationResult;
import com.microsoft.identity.client.PublicClientApplication;


/**
 * Created by dadaboli on 4/17/17.
 * <p>
 * Edited by Max Orr on 08/05/2018
 * <p>
 * This class is used to maintain auth state across the app
 * This allows us to access the authentication context and result in any activity we want.
 */


public class AppSubClass extends Application {
    private final static String TAG = AppSubClass.class.getSimpleName();

    private AuthenticationResult authenticationResult;
    private PublicClientApplication publicClientApplication;
    private static AppSubClass appSubClass;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appSubClass = this;
        authenticationResult = null;
        publicClientApplication = null;
    }


    public static AppSubClass getInstance() {
        return appSubClass;
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    public AuthenticationResult getAuthenticationResult() {
        return authenticationResult;
    }

    public void setAuthenticationResult(AuthenticationResult authenticationResult) {
        this.authenticationResult = authenticationResult;
    }


    public PublicClientApplication getPublicClient() {
        return publicClientApplication;
    }

    public void setPublicClient(PublicClientApplication app) {
        this.publicClientApplication = app;
    }


}
