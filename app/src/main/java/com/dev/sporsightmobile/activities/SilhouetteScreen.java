// Jacob Rogers 9-10-2020
// for questions: jrr@knights.ucf.edu

package com.dev.sporsightmobile.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.fragments.AnnotationFragment;

import java.util.ArrayList;

public class SilhouetteScreen extends AppCompatActivity implements View.OnClickListener {

    private static ArrayList<String> selectedJoints;

    private ImageButton headRight;
    private ImageButton headLeft;
    private ImageButton shoulderRight;
    private ImageButton shoulderLeft;
    private ImageButton elbowRight;
    private ImageButton elbowLeft;
    private ImageButton wristRight;
    private ImageButton wristLeft;
    private ImageButton hipRight;
    private ImageButton hipLeft;
    private ImageButton kneeRight;
    private ImageButton kneeLeft;
    private ImageButton ankleRight;
    private ImageButton ankleLeft;
    private ImageView pastCircle1;
    private ImageView futureCircle1;
    private ImageView pastCircle2;
    private ImageView futureCircle2;
    private TextView joint2Text;
    private TextView joint1Text;
    private Button applyButton;
    private TextView selectedJointsText;

    AnnotationFragment annotationFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_silhouette_screen);

        selectedJoints = new ArrayList<String>();

        annotationFragment = new AnnotationFragment();

        headRight = findViewById(R.id.HeadRight);
        headLeft = findViewById(R.id.HeadLeft);
        shoulderRight = findViewById(R.id.ShoulderRight);
        shoulderLeft = findViewById(R.id.ShoulderLeft);
        elbowRight = findViewById(R.id.ElbowRight);
        elbowLeft = findViewById(R.id.ElbowLeft);
        wristRight = findViewById(R.id.wristRight);
        wristLeft = findViewById(R.id.WristLeft);
        hipRight = findViewById(R.id.HipRight);
        hipLeft = findViewById(R.id.HipLeft);
        kneeRight = findViewById(R.id.KneeRight);
        kneeLeft = findViewById(R.id.KneeLeft);
        ankleRight = findViewById(R.id.AnkleRight);
        ankleLeft = findViewById(R.id.AnkleLeft);
        pastCircle1 = findViewById(R.id.PastColorCircle_1);
        futureCircle1 = findViewById(R.id.FutureColorCircle_1);
        pastCircle2 = findViewById(R.id.pastColorCircle_2);
        futureCircle2 = findViewById(R.id.futureColorCircle_2);
        joint2Text = findViewById(R.id.joint_2_Text);
        joint1Text = findViewById(R.id.Joint_1_text);
        applyButton = findViewById(R.id.ApplyButton);
        selectedJointsText = findViewById(R.id.SelectedJointsText);

        selectedJointsText.setText("Selected Joints (0/2)");

        pastCircle1.setVisibility(View.INVISIBLE);
        futureCircle1.setVisibility(View.INVISIBLE);
        pastCircle2.setVisibility(View.INVISIBLE);
        futureCircle2.setVisibility(View.INVISIBLE);
        joint1Text.setVisibility(View.INVISIBLE);
        joint2Text.setVisibility(View.INVISIBLE);

        headRight.setOnClickListener(this);
        headLeft.setOnClickListener(this);
        shoulderRight.setOnClickListener(this);
        shoulderLeft.setOnClickListener(this);
        elbowRight.setOnClickListener(this);
        elbowLeft.setOnClickListener(this);
        wristRight.setOnClickListener(this);
        wristLeft.setOnClickListener(this);
        hipRight.setOnClickListener(this);
        hipLeft.setOnClickListener(this);
        kneeRight.setOnClickListener(this);
        kneeLeft.setOnClickListener(this);
        ankleRight.setOnClickListener(this);
        ankleLeft.setOnClickListener(this);
        applyButton.setOnClickListener(this);

        headRight.setTag("white");
        headLeft.setTag("white");
        shoulderRight.setTag("white");
        shoulderLeft.setTag("white");
        elbowRight.setTag("white");
        elbowLeft.setTag("white");
        wristRight.setTag("white");
        wristLeft.setTag("white");
        hipRight.setTag("white");
        hipLeft.setTag("white");
        kneeRight.setTag("white");
        kneeLeft.setTag("white");
        ankleRight.setTag("white");
        ankleLeft.setTag("white");

        initalizeValues();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.HeadRight:
                updateSelectedJoints(headRight, "R Head");
                break;
            case R.id.HeadLeft:
                updateSelectedJoints(headLeft, "L Head");
                break;
            case R.id.ShoulderRight:
                updateSelectedJoints(shoulderRight, "R Shoulder");
                break;
            case R.id.ShoulderLeft:
                updateSelectedJoints(shoulderLeft, "L Shoulder");
                break;
            case R.id.ElbowRight:
                updateSelectedJoints(elbowRight, "R Elbow");
                break;
            case R.id.ElbowLeft:
                updateSelectedJoints(elbowLeft, "L Elbow");
                break;
            case R.id.wristRight:
                updateSelectedJoints(wristRight, "R Wrist");
                break;
            case R.id.WristLeft:
                updateSelectedJoints(wristLeft, "L Wrist");
                break;
            case R.id.HipRight:
                updateSelectedJoints(hipRight, "R Hip");
                break;
            case R.id.HipLeft:
                updateSelectedJoints(hipLeft, "L Hip");
                break;
            case R.id.KneeRight:
                updateSelectedJoints(kneeRight, "R Knee");
                break;
            case R.id.KneeLeft:
                updateSelectedJoints(kneeLeft, "L Knee");
                break;
            case R.id.AnkleRight:
                updateSelectedJoints(ankleRight, "R Ankle");
                break;
            case R.id.AnkleLeft:
                updateSelectedJoints(ankleLeft, "L Ankle");
                break;
            case R.id.ApplyButton:
                backToAnnotationScreen();
                break;
        }
    }
    
    private void initalizeValues(){
        ArrayList<String> tmpList = annotationFragment.getSelectedJoints();
        String current;

        for(int i = 0; i < tmpList.size(); i++){
            current = tmpList.get(i);
            switch (current){
                case "R Head":
                    headRight.performClick();
                    break;
                case "L Head":
                    headLeft.performClick();
                    break;
                case "R Shoulder":
                    shoulderRight.performClick();
                    break;
                case "L Shoulder":
                    shoulderLeft.performClick();
                    break;
                case "R Elbow":
                    elbowRight.performClick();
                    break;
                case "L Elbow":
                    elbowLeft.performClick();
                    break;
                case "R Wrist":
                    wristRight.performClick();
                    break;
                case "L Wrist":
                    wristLeft.performClick();
                    break;
                case "R Hip":
                    hipRight.performClick();
                    break;
                case "L Hip":
                    hipLeft.performClick();
                    break;
                case "R Knee":
                    kneeRight.performClick();
                    break;
                case "R Ankle":
                    ankleRight.performClick();
                    break;
                case "L Ankle":
                    ankleLeft.performClick();
                    break;
            }
        }
    }

    private void updateSelectedJoints(ImageButton joint, String name){
        // Remove joint from the list.
        if(joint.getTag() == "red"){
            selectedJoints.remove(name);
            updateSelectedJointsUIList();
            changeJointColor(joint);
        }
        // Add joint to the lst.
        else if(joint.getTag() == "white"){
            if(selectedJoints.size() < 2){
                selectedJoints.add(name);
                updateSelectedJointsUIList();
                changeJointColor(joint);
            }
            else{
                Toast.makeText(this, "Maximum number of joints already added.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void updateSelectedJointsUIList(){
        if(selectedJoints.size() == 1) {
            joint1Text.setText(selectedJoints.get(0));
            joint1Text.setVisibility(View.VISIBLE);
            pastCircle1.setVisibility(View.VISIBLE);
            futureCircle1.setVisibility(View.VISIBLE);

            joint2Text.setVisibility(View.INVISIBLE);
            pastCircle2.setVisibility(View.INVISIBLE);
            futureCircle2.setVisibility(View.INVISIBLE);

            selectedJointsText.setText("Selected Joints (1/2)");
        }
        else if(selectedJoints.size() == 2){
            joint1Text.setText(selectedJoints.get(0));
            joint1Text.setVisibility(View.VISIBLE);
            pastCircle1.setVisibility(View.VISIBLE);
            futureCircle1.setVisibility(View.VISIBLE);

            joint2Text.setText(selectedJoints.get(1));
            joint2Text.setVisibility(View.VISIBLE);
            pastCircle2.setVisibility(View.VISIBLE);
            futureCircle2.setVisibility(View.VISIBLE);

            selectedJointsText.setText("Selected Joints (2/2)");
        }
        else{
            joint1Text.setVisibility(View.INVISIBLE);
            pastCircle1.setVisibility(View.INVISIBLE);
            futureCircle1.setVisibility(View.INVISIBLE);

            joint2Text.setVisibility(View.INVISIBLE);
            pastCircle2.setVisibility(View.INVISIBLE);
            futureCircle2.setVisibility(View.INVISIBLE);

            selectedJointsText.setText("Selected Joints (0/2)");
        }
    }

    private void changeJointColor(ImageButton joint){
        if(joint.getTag() == "white"){
            joint.setImageResource(R.drawable.circle_red);
            joint.setTag("red");
        }
        else{
            joint.setImageResource(R.drawable.circle_white);
            joint.setTag("white");
        }
    }

    private void backToAnnotationScreen(){
        annotationFragment.setSelectedJoints(selectedJoints);
        annotationFragment.seekToTime = 0;
        finish();
    }

    public ArrayList getSelectedJoints(){
        return selectedJoints;
    }


}