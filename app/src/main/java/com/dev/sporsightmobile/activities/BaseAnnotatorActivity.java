package com.dev.sporsightmobile.activities;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.enums.BodyPosition;
import com.dev.sporsightmobile.fragments.AnnotationFragment;
import com.dev.sporsightmobile.fragments.ExoPlayerFragment;
import com.dev.sporsightmobile.interfaces.OnFragmentInteractionListener;
import com.dev.sporsightmobile.services.VideoAnalysisService;
import com.dev.sporsightmobile.utilities.FrameUtilities;
import com.dev.sporsightmobile.utilities.PoseEstimationUtilities;
import com.dev.sporsightmobile.views.CustomExoPlayerControlView;

import java.util.concurrent.CompletableFuture;


public class BaseAnnotatorActivity extends FragmentActivity
        implements OnFragmentInteractionListener<Object> {

    private final static String TAG = BaseAnnotatorActivity.class.getSimpleName();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (VideoAnalysisService.analysisResult != null && !VideoAnalysisService.analysisResult.isDone()) {
            FrameUtilities.cancelPromise = true;
        }
    }

    private void setCheckboxStates() {
        RadioButton sideRadioButton = findViewById(R.id.side);
        RadioButton frontRadioButton = findViewById(R.id.front);
        RadioButton leftRadioButton = findViewById(R.id.left);
        RadioButton rightRadioButton = findViewById(R.id.right);

        switch (PoseEstimationUtilities.bodyPosition) {
            case SIDE_LEFT:
                sideRadioButton.setChecked(true);
                leftRadioButton.setChecked(true);
                break;
            case SIDE_RIGHT:
                sideRadioButton.setChecked(true);
                rightRadioButton.setChecked(true);
                break;
            case FRONT_LEFT:
                frontRadioButton.setChecked(true);
                leftRadioButton.setChecked(true);
                break;
            case FRONT_RIGHT:
                frontRadioButton.setChecked(true);
                rightRadioButton.setChecked(true);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCheckboxStates();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setCheckboxStates();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_annotator_activity);


        if (null == savedInstanceState) {
            String mStartCode = getIntent().getStringExtra("EXTRA_STARTCODE");


            if (mStartCode.equals("Player")) {
                Log.v(TAG, "Starting Just The Video Player Fragment");
                String mParcableString = getIntent().getParcelableExtra("EXTRA_URI").toString();
                Uri mVideoUri = Uri.parse(mParcableString);

//                LinearLayout topControls = (LinearLayout)findViewById(R.id.annotation_positionToggle);
//                topControls.setVisibility(View.INVISIBLE);


                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.base_exoPlayer_container, ExoPlayerFragment.newInstance(mVideoUri), "ExoPlayerFragment")
                        .commit();

            }


            //TODO: THE ORDER IN WHICH THESE FRAGMENTS ARE PLACED ON TOP OF ONE ANOTHER CAN POSSIBLY EFFECT THE ONCLICKLISTENERS FOR A GIVEN POSITION
            if (mStartCode.equals("Annotator")) {
                Log.v(TAG, "Starting ExoPlayer, ScreenCapture, & Drawing Fragments");
                String mParcableString = getIntent().getParcelableExtra("EXTRA_URI").toString();
                Uri mVideoUri = Uri.parse(mParcableString);
                String mVideoTitle = getIntent().getStringExtra("EXTRA_TITLE");
                Log.d(TAG, mVideoUri.toString());
                Log.d(TAG, "Video processing started");

                VideoAnalysisService.processVideo(mVideoUri, BaseAnnotatorActivity.this);


//                LinearLayout topControls = (LinearLayout)findViewById(R.id.annotation_positionToggle);
//                topControls.setVisibility(View.VISIBLE);

                //Loading the ExoPlayer Fragment
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.base_exoPlayer_container, ExoPlayerFragment.newInstance(mVideoUri), "ExoPlayerFragment")
                        .commit();

                //TODO: PUT THIS IN ITS OWN CONTAINER????? <- stop yelling.
                //Loading the Annotation Fragment
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.base_exoPlayer_container, AnnotationFragment.newInstance(), "AnnotationFragment")
                        .commit();
            }


        }


    }//endof onCreate()

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("ExoPlayerFragment");
        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onFragmentMessage(String TAG, Object data) {

        Log.d(TAG, "TOGGLING THE DRAWBAR");

        if (TAG.equals(CustomExoPlayerControlView.class.getSimpleName())) {
            String input = (String) data;
            if (input.equals("DrawBar")) {
                Log.d(TAG, "TOGGLING THE DRAWBAR");
//                myDrawingFragment.toggleDrawingBarView();
            }
        }

    }

    public void onRadioButtonClicked(View view) {
        if (((RadioButton) view).isChecked()) {
            switch (view.getId()) {
                case R.id.side:
                    PoseEstimationUtilities.isSide = true;
                    if (PoseEstimationUtilities.isLeftSide) {
                        PoseEstimationUtilities.bodyPosition = BodyPosition.SIDE_LEFT;
                    } else {
                        PoseEstimationUtilities.bodyPosition = BodyPosition.SIDE_RIGHT;
                    }
                    break;
                case R.id.front:
                    PoseEstimationUtilities.isSide = false;
                    if (PoseEstimationUtilities.isLeftSide) {
                        PoseEstimationUtilities.bodyPosition = BodyPosition.FRONT_LEFT;
                    } else {
                        PoseEstimationUtilities.bodyPosition = BodyPosition.FRONT_RIGHT;
                    }
                    break;
                case R.id.left:
                    PoseEstimationUtilities.isLeftSide = true;
                    if (PoseEstimationUtilities.isSide) {
                        PoseEstimationUtilities.bodyPosition = BodyPosition.SIDE_LEFT;
                    } else {
                        PoseEstimationUtilities.bodyPosition = BodyPosition.FRONT_LEFT;
                    }
                    break;
                case R.id.right:
                    PoseEstimationUtilities.isLeftSide = false;
                    if (PoseEstimationUtilities.isSide) {
                        PoseEstimationUtilities.bodyPosition = BodyPosition.SIDE_RIGHT;
                    } else {
                        PoseEstimationUtilities.bodyPosition = BodyPosition.FRONT_RIGHT;
                    }
                    break;
            }
        }

        Log.v(TAG, "PoseEstimationUtility Body Position: " + PoseEstimationUtilities.bodyPosition);
    }

    public static Boolean trajectorySelected = false;

    @Override
    public void onBackPressed()
    {
        if (trajectorySelected) {
//            trajectory.setImageResource(R.drawable.ic_trajectory_white);
            View linearLayout = this.findViewById(R.id.topbar_annotationFragment);
            linearLayout.setVisibility(View.VISIBLE);
//            toSilhouetteScreen.setVisibility(View.VISIBLE);
            LinearLayout toolBarTop = (LinearLayout) this.findViewById(R.id.annotation_positionToggle);
            toolBarTop.setVisibility(View.VISIBLE);
//            mDrawingView.setDrawingType("Clear");
            trajectorySelected = false;
        } else {
            Intent mBaseAnnotatorActivity = new Intent(getApplicationContext(), BaseHomePageActivity.class);
            mBaseAnnotatorActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mBaseAnnotatorActivity.putExtra("Tab", "Gallery");
            getApplicationContext().startActivity(mBaseAnnotatorActivity);
            finish();
        }


        // code here to show dialog
      //  super.onBackPressed();  // optional depending on your needs
    }

}//endof BaseAnnotatorActivity
