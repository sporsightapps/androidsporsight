package com.dev.sporsightmobile.activities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.fragments.AnnotationFragment;
import com.dev.sporsightmobile.fragments.ExoPlayerFragment;
import com.dev.sporsightmobile.model.FrameAnalysisResult;
import com.dev.sporsightmobile.services.SevenKeyFrameService;
import com.dev.sporsightmobile.services.VideoAnalysisService;
import com.dev.sporsightmobile.utilities.PoseEstimationUtilities;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Pair;

import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.lite.examples.posenet.lib.KeyPoint;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FramesActivity extends AppCompatActivity {

    List<FrameAnalysisResult> poseResults;
    ImageView frame1, frame2, frame3, frame4, frame5, frame6, frame7;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        // get analysis results
        VideoAnalysisService.analysisResult.thenAccept(results -> {this.poseResults = results;});

        setContentView(R.layout.activity_key_frames);

        // Abdool - logic to get 7 key frames
        SevenKeyFrameService keys = new SevenKeyFrameService(getBaseContext(), poseResults);
        ArrayList<Bitmap> sequence = keys.getFrames();

        // Abdool - set the 7 key frames
        ImageView frame1 = findViewById(R.id.frame1);
        frame1.setImageBitmap(sequence.get(0));
        ImageView frame2 = findViewById(R.id.frame2);
        frame2.setImageBitmap(sequence.get(1));
        ImageView frame3 = findViewById(R.id.frame3);
        frame3.setImageBitmap(sequence.get(2));
        ImageView frame4 = findViewById(R.id.frame4);
        frame4.setImageBitmap(sequence.get(3));
        ImageView frame5 = findViewById(R.id.frame5);
        frame5.setImageBitmap(sequence.get(4));
        ImageView frame6 = findViewById(R.id.frame6);
        frame6.setImageBitmap(sequence.get(5));
        ImageView frame7 = findViewById(R.id.frame7);
        frame7.setImageBitmap(sequence.get(6));
    }

    @Override
    public void onBackPressed() {
        AnnotationFragment.seekToTime = 0;
        super.onBackPressed();
    }
}