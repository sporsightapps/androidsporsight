/*
 * Copyright 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dev.sporsightmobile.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.fragments.BluetoothCameraFragment;
import com.dev.sporsightmobile.fragments.Camera2VideoFragment;
import com.gowtham.library.utils.TrimVideo;

public class BaseCameraActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_camera_activity);

        if (null == savedInstanceState) {
            String mStartCode = getIntent().getStringExtra("EXTRA_CAMERACODE");


            if (mStartCode.equals("Record")) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.camera_fragment_container, Camera2VideoFragment.newInstance())
                        .commit();
            }

            if (mStartCode.equals("Bluetooth")) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.camera_fragment_container, BluetoothCameraFragment.newInstance())
                        .commit();
            }

        }

    }



}
