package com.dev.sporsightmobile.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.fragments.Camera2VideoFragment;

public class CameraSettings extends AppCompatActivity {

    private Button backButton;
    RadioButton zeroSecs, threeSecs, tenSecs;
    RadioGroup timerRadioGroup;
    private static String timerSetting;
    Camera2VideoFragment camera2VideoFragment = new Camera2VideoFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_settings);

        timerRadioGroup = findViewById(R.id.timerRadioGroup);
        zeroSecs = findViewById(R.id.zero_sec);
        threeSecs = findViewById(R.id.three_sec);
        tenSecs = findViewById(R.id.ten_sec);

        // Assign the correct timer radioButton to be on based on what is saved in Camera2VideoFragment.
        timerSetting = camera2VideoFragment.getTimerSetting();
        switch (timerSetting){
            case "three":
                threeSecs.setChecked(true);
                break;
            case "ten":
                tenSecs.setChecked(true);
                break;
            default:
                zeroSecs.setChecked(true);
                break;
        }

        // Back Button:
        backButton = (Button) findViewById(R.id.BackToCameraButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                backToCameraScreen();
            }
        });
    }

    private void backToCameraScreen() {
        if(threeSecs.isChecked()){
            timerSetting = "three";
        }
        else if(tenSecs.isChecked()){
            timerSetting = "ten";
        }
        else{
            timerSetting = "zero";
        }
        Log.d("settings","timerSetting = " + timerSetting);
        camera2VideoFragment.setTimer(timerSetting);
        camera2VideoFragment.setTimerIcon(timerSetting);
        finish();
    }

    public String getTimerSetting(){
        return timerSetting;
    }

}