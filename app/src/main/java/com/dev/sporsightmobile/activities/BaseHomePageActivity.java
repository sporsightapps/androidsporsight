package com.dev.sporsightmobile.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;

import com.androidnetworking.AndroidNetworking;
import com.dev.sporsightmobile.entities.video.VideoDBHelper;
import com.dev.sporsightmobile.fragments.GalleryFragment;

import com.dev.sporsightmobile.fragments.Camera2VideoFragment;
import com.dev.sporsightmobile.services.BlobStorageService;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.fragments.EmptyHomePageFragment;
import com.dev.sporsightmobile.fragments.VideoArchiveFragment;
import com.dev.sporsightmobile.fragments.my_stats.MyStatsFragment;
import com.dev.sporsightmobile.fragments.my_stats.MyStatsReportFragment;

import com.dev.sporsightmobile.interfaces.OnFragmentInteractionListener;
//import com.dev.sporsightmobile.payments.BillingActivity;
import com.dev.sporsightmobile.utilities.Constants;
import com.dev.sporsightmobile.utilities.Helpers;
import com.dev.sporsightmobile.utilities.PermissionsHelper;
import com.gowtham.library.utils.TrimVideo;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.AuthenticationResult;
import com.microsoft.identity.client.MsalClientException;
import com.microsoft.identity.client.MsalException;
import com.microsoft.identity.client.MsalServiceException;
import com.microsoft.identity.client.MsalUiRequiredException;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.UiBehavior;
import com.microsoft.identity.client.User;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class BaseHomePageActivity extends AppCompatActivity
        implements OnFragmentInteractionListener<Object> {

    private static final String TAG = BaseHomePageActivity.class.getSimpleName();


    private Toolbar toolbar;
    private TextView title;


    /* Azure AD variables */
    AppSubClass appState;
    private AuthenticationResult authResult;
    private PublicClientApplication sampleApp;
    String[] scopes;

    VideoDBHelper videoDBHelper;
    Context mContext;

    // VideoDBHelper dbHelper;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_home_page_activity);

        mContext = BaseHomePageActivity.this;

      //  videoDBHelper = new VideoDBHelper(mContext);



        //.start(getApplication(), "950d8897-abf7-49e3-b33e-df71ea86a4c1", Analytics.class, Crashes.class);
        //AppCenter.start(getApplication(), "950d8897-abf7-49e3-b33e-df71ea86a4c1", Push.class);

     //   SharedPreferences preferences = getSharedPreferences("myPrefs", MODE_PRIVATE);


     //   dbHelper = new VideoDBHelper(this);
    //    SQLiteDatabase mDatabase = dbHelper.getWritableDatabase();


//        toolbar = findViewById(R.id.homepage_toolbar);
//        toolbar.inflateMenu(R.menu.home_page_menu);
//        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                return onOptionsItemSelected(item);
//            }
//        });
//
//
//        title = findViewById(R.id.homepage_toolbar_title);


        // TODO: open newsfeed fragment at once logged in.
//        Fragment startingFragment = EmptyHomePageFragment.newInstance();
//        loadFragment(startingFragment, EmptyHomePageFragment.class.getSimpleName());

        // Abdool - Commented the above two and the added the bottom two to force camera fragment to load
        // todo main issue is that camera is an activity and not a fragment

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        if (bottomNavigationView != null) {

            bottomNavigationView.setOnNavigationItemSelectedListener(
                    new BottomNavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                            Fragment fragment;
                            switch (item.getItemId()) {
                                case R.id.action_record:
                                    //title.setText("Camera");
//                                    Intent intent = new Intent(getApplicationContext(), BaseCameraActivity.class);
//                                    intent.putExtra("EXTRA_CAMERACODE", "Record");
//                                    startActivity(intent);

                                    // Abdool - Commented out above code and added these two to force camera's fragment to load
                                    // todo fix setting page crash
                                    Fragment startingFragment = Camera2VideoFragment.newInstance();
                                    loadFragment(startingFragment, Camera2VideoFragment.class.getSimpleName());

                                    return true;

//                                case R.id.action_my_homepage:
//                                    title.setText("My Homepage");
//                                    fragment = EmptyHomePageFragment.newInstance();
//                                    loadFragment(fragment,EmptyHomePageFragment.class.getSimpleName());
//                                    return true;

                                case R.id.action_my_videos:

                                    if (checkLoadVideos()) {
                                        //   title.setText("My Videos");
                                        fragment = new GalleryFragment();
                                        loadFragment(fragment, GalleryFragment.class.getSimpleName());


                                        return true;
                                    }
                                    return true;

                                // TODO change this to stats
//                                case R.id.action_video_archive:
//
//                                    // Abdool - changed title from Video Archive to Statistics
//                                    title.setText("Statistics");
//                                    fragment = VideoArchiveFragment.newInstance();
//                                    loadFragment(fragment, VideoArchiveFragment.class.getSimpleName());
//                                    return true;
//
////                                case R.id.action_video_archive:
////                                    title.setText("My Stats");
////                                    fragment = MyStatsFragment.newInstance();
////                                    loadFragment(fragment,MyStatsFragment.class.getSimpleName());
////                                    return true;
                            }
                            return false;
                        }
                    });
            //bottomNavigationView.setSelectedItemId(R.id.action_my_homepage);
        }

        String tab = getIntent().getStringExtra("Tab");
        if(tab == null)
        {
            Log.d("extra", "null");

            bottomNavigationView.setSelectedItemId(R.id.action_record);
//
//            Fragment startingFragment = Camera2VideoFragment.newInstance();
//            loadFragment(startingFragment, Camera2VideoFragment.class.getSimpleName());
        }
        else
        {
            Log.d("extra", tab);

            bottomNavigationView.setSelectedItemId(R.id.action_my_videos);

//
//            Fragment startingFragment = new GalleryFragment();
//            loadFragment(startingFragment, GalleryFragment.class.getSimpleName());
        }

//        if(tab == "Gallery")
//        {
//            Fragment startingFragment = new GalleryFragment();
//            loadFragment(startingFragment, GalleryFragment.class.getSimpleName());
//        }
//        else
//        {
//            Fragment startingFragment = Camera2VideoFragment.newInstance();
//            loadFragment(startingFragment, Camera2VideoFragment.class.getSimpleName());
//        }

        if (findViewById(R.id.homepage_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
        }





//
//        // Handling of the Authentication State of the Application
//        appState = AppSubClass.getInstance();
//        sampleApp = appState.getPublicClient();
//        authResult = appState.getAuthenticationResult();
//        scopes = Constants.SCOPES.split("\\s+");
//
//
//
//        /* Write the token status (whether or not we received each token) */
////        this.updateTokenUI();
//
//
//        Log.d(TAG, "___________________AUTH RESULT" + authResult.toString());
//        Log.d(TAG, "___________________AUTH RESULT ACCESS TOKEN: " + authResult.getAccessToken());
//        Log.d(TAG, "___________________AUTH RESULT USER: " + authResult.getUser());
//        Log.d(TAG, "___________________AUTH RESULT IDTOKEN: " + authResult.getIdToken());
//        Log.d(TAG, "___________________AUTH RESULT TenantID: " + authResult.getTenantId());
//
//        Log.d(TAG, "____ABOUT TO callGraphAPI() ");
//        /* Calls API, dump out response from UserInfo endpoint into UI */
//        this.callGraphAPI();
//
//        callNewAPI();

    }//endof onCreate()


//=========== CORE IDENTITY METHODS USED BY MSAL ===================================================


    public Boolean checkLoadVideos() {
        if (PermissionsHelper.checkRequestReadStoragePermissions(this)) {
            Log.v(TAG, "ABOUT TO CALL -> dbhelper.loadvideosfromstorage()");
      //      dbHelper.loadVideosFromStorage(getApplicationContext());
            Log.v(TAG, "Finished -> dbhelper.loadvideosfromstorage()");
            return true;
        }
        return false;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v(TAG, "========== In OnActivityResult");

        super.onActivityResult(requestCode, resultCode, data);

        Log.d("BaseCameraActivity", "Yeet:: ");
        if (requestCode == TrimVideo.VIDEO_TRIMMER_REQ_CODE && data != null) {
            Log.d("BaseCameraActivity", "Data:: " + data);
            Uri uri = Uri.parse(TrimVideo.getTrimmedVideoPath(data));
            Log.d("BaseCameraActivity", "Trimmed path:: " + uri);
            Intent mBaseAnnotatorActivity = new Intent(this, BaseAnnotatorActivity.class);
            mBaseAnnotatorActivity.putExtra("EXTRA_URI", uri);
            mBaseAnnotatorActivity.putExtra("EXTRA_TITLE", "Yeet");
            mBaseAnnotatorActivity.putExtra("EXTRA_STARTCODE", "Annotator");
            mBaseAnnotatorActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);

            //Upload
            Intent blobActivity = new Intent(this, BlobStorageActivity.class);
            blobActivity.putExtra("EXTRA_URI", uri);
            //  this.startActivity(mBaseAnnotatorActivity);

            try {
                sampleApp.handleInteractiveRequestRedirect(requestCode, resultCode, data);
            } catch (Exception e) {
                finish();


                this.startActivity(blobActivity);

               // this.startActivity(mBaseAnnotatorActivity);
            }

        }
    }

    private void callNewAPI() {

        JsonArrayRequest req = new JsonArrayRequest(Constants.SCOPES,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("NEW API RESPONSE NEW API RESPONSE Response", response.toString());

                        try {
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject person = (JSONObject) response.get(i);

                                String name = person.getString("pcat");
                                //  String email = person.getString("pdisc");

//                                Options a=new Options(name,"");
//                                albumList.add(a);
                                Log.e("results uij", name);

//                                adapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Response", "Error: " + error.getMessage());
//                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        // Adding request to request queue
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(req);


    }


    /* Use Volley to request the /me endpoint from API
     *  Sets the UI to what we get back
     */
    private void callGraphAPI() {
        Log.d(TAG, "**************************************   Starting volley request to API");


        RequestQueue queue = Volley.newRequestQueue(this);

        JSONObject parameters = new JSONObject();
        try {
            Log.v(TAG, "***************** PUTTING JSON PARAMETERS");
            parameters.put("client_id", Constants.CLIENT_ID);
            parameters.put("Resource", Constants.B2C_REPLY_URL_TWO);

        } catch (Exception e) {
            Log.d(TAG, "Failed to put parameters: " + e.toString());
        }


        JsonObjectRequest request = new JsonObjectRequest(
                "https://login.microsoftonline.com/tfp/oauth2/nativeclient",
                null,
                new Response.Listener<JSONObject>() {


                    @Override
                    public void onResponse(JSONObject response) {
                        /* Successfully called API */
                        Log.d(TAG, "__________________Response: " + response);
                        Log.v(TAG, "________________Response.toString()" + response.toString());
                        try {
//                    Toast.makeText(getBaseContext(), "Response: " + response.get("name"), Toast.LENGTH_LONG).show();
                            String name = (String) response.get("name");
                        } catch (JSONException e) {
                            Log.d(TAG, "JSONEXception Error: " + e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                Log.d(TAG, "Token: " + authResult.getAccessToken());
                headers.put("Authorization", "Bearer " + authResult.getAccessToken());
                return headers;
            }
        };

        queue.add(request);
    }


//=========== CORE IDENTITY METHODS USED BY MSAL ===================================================


//=========== USER METHODS USED BY MSAL ===================================================

    /* Use Volley to request the /me endpoint from API
     *  Sets the UI to what we get back
     */
    private void editProfile() {
        Log.d(TAG, "Starting volley request to API");
        try {
            String authority = String.format(Constants.AUTHORITY,
                    Constants.TENANT,
                    Constants.EDIT_PROFILE_POLICY);

            User currentUser = Helpers.getUserByPolicy(
                    sampleApp.getUsers(),
                    Constants.EDIT_PROFILE_POLICY);

            sampleApp.acquireToken(
                    this,
                    scopes,
                    currentUser,
                    UiBehavior.SELECT_ACCOUNT,
                    null,
                    null,
                    authority,
                    getEditPolicyCallback());
        } catch (MsalClientException e) {
            /* No User */
            Log.d(TAG, "MSAL Exception Generated while getting users: " + e.toString());
        }

    }


    /* Clears a user's tokens from the cache.
     * Logically similar to "signOut" but only signs out of this app.
     */
    private void clearCache() {
        List<User> users = null;
        try {
            Log.d(TAG, "Clearing app cache");
            users = sampleApp.getUsers();

            if (users == null) {
                /* We have no users */

                Log.d(TAG, "Faield to Sign out/clear cache, no user");
            } else if (users.size() == 1) {
                /* We have 1 user */

                /* Remove from token cache */
                sampleApp.remove(users.get(0));

                Log.d(TAG, "Signed out/cleared cache");

            } else {
                /* We have multiple users */

                for (int i = 0; i < users.size(); i++) {
                    sampleApp.remove(users.get(i));
                }

                Log.d(TAG, "Signed out/cleared cache for multiple users");
            }

            Toast.makeText(getBaseContext(), "Signed Out!", Toast.LENGTH_SHORT)
                    .show();

        } catch (MsalClientException e) {
            /* No token in cache, proceed with normal unauthenticated app experience */
            Log.d(TAG, "MSAL Exception Generated while getting users: " + e.toString());

        } catch (IndexOutOfBoundsException e) {
            Log.d(TAG, "User at this position does not exist: " + e.toString());
        }
    }

//=========== USER METHODS USED BY MSAL ===================================================


//=========== UI & HELPER METHODS ================================================================//


    //TODO: CAN MODIFY THIS TO MAKE A LOGGED IN ICON IN TOP RIGHT CORNER OF HOMESCREEN
    /* Write the token status (whether or not we received each token) */
//    private void updateTokenUI() {
//        if (authResult != null) {
//            TextView it = (TextView) findViewById(R.id.itStatus);
//            TextView at = (TextView) findViewById(R.id.atStatus);
//
//            if(authResult.getIdToken() != null) {
//                it.setText(it.getText() + " " + getString(R.string.tokenPresent));
//            } else {
//                it.setText(it.getText() + " " + getString(R.string.noToken));
//            }
//
//            if (authResult.getAccessToken() != null) {
//                at.setText(at.getText() + " " + getString(R.string.tokenPresent));
//            } else {
//                at.setText(at.getText() + " " + getString(R.string.noToken));
//            }
//
//            /* Only way to check if we have a refresh token is to actually refresh our tokens */
//            hasRefreshToken();
//        } else {
//            Log.d(TAG, "No authResult, something went wrong.");
//        }
//    }


    /* Checks if there's a refresh token in the cache.
     * Only way to check is to refresh the tokens and catch Exception.
     * Also is used to refresh the token.
     */
    private void hasRefreshToken() {

        /* Attempt to get a user and acquireTokenSilently
         * If this fails we will do an interactive request
         */
        List<User> users = null;
        try {
            User currentUser = Helpers.getUserByPolicy(sampleApp.getUsers(), Constants.SISU_POLICY);

            if (currentUser != null) {
                /* We have 1 user */
                boolean forceRefresh = true;
                sampleApp.acquireTokenSilentAsync(
                        scopes,
                        currentUser,
                        String.format(Constants.AUTHORITY, Constants.TENANT, Constants.SISU_POLICY),
                        forceRefresh,
                        getAuthSilentCallback());
            } else {
                /* We have no user for this policy*/
                updateRefreshTokenUI(false);
            }
        } catch (MsalClientException e) {
            /* No token in cache, proceed with normal unauthenticated app experience */
            Log.d(TAG, "MSAL Exception Generated while getting users: " + e.toString());

        } catch (IndexOutOfBoundsException e) {
            Log.d(TAG, "User at this position does not exist: " + e.toString());
        }
    }


    /* Write the token status (whether or not we received each token) */
    private void updateRefreshTokenUI(boolean status) {

//        TextView rt = (TextView) findViewById(R.id.rtStatus);
//
//        if (rt.getText().toString().contains(getString(R.string.noToken))
//                || rt.getText().toString().contains(getString(R.string.tokenPresent))) {
//            rt.setText(R.string.RT);
//        }
//        if (status) {
//            rt.setText(rt.getText() + " " + getString(R.string.tokenPresent));
//        } else {
//            rt.setText(rt.getText() + " " + getString(R.string.noToken) + " or Invalid");
//        }
    }


    /* Callback used in for silent acquireToken calls.
     * Used in here solely to test whether or not we have a refresh token in the cache
     */
    private AuthenticationCallback getAuthSilentCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(AuthenticationResult authenticationResult) {
                /* Successfully got a token */
                updateRefreshTokenUI(true);

                /* If the token is refreshed we should refresh our data */
                callGraphAPI();
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString());
                updateRefreshTokenUI(false);
                if (exception instanceof MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                    assert true;

                } else if (exception instanceof MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                    assert true;

                } else assert !(exception instanceof MsalUiRequiredException) || true;
            }

            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.");
                updateRefreshTokenUI(true);
            }
        };
    }


    /* Callback used in for silent acquireToken calls.
     * Used in here solely to test whether or not we have a refresh token in the cache
     */
    private AuthenticationCallback getEditPolicyCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(AuthenticationResult authenticationResult) {
                /* Successfully got a token */

                /* Use this method to refresh our token with new claims */
                Log.d(TAG, "Edit Profile: " + authenticationResult.getAccessToken());
                hasRefreshToken();
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Edit Profile failed: " + exception.toString());

                if (exception instanceof MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                    assert true;

                } else if (exception instanceof MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                    assert true;

                } else assert !(exception instanceof MsalUiRequiredException) || true;
            }


            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled Edit Profile.");
                Toast.makeText(getBaseContext(), getString(R.string.editFailure), Toast.LENGTH_SHORT)
                        .show();
            }
        };
    }


//=========== UI & HELPER METHODS ================================================================//


//=========== FRAGMENT HANDLING & INTERACTION===================================================

    @Override
    public void onBackPressed()
    {
        //prevent action
    }

    //=== START =======HANDLING OF THE OPTIONS MENU AT THE TOP OF THE HOMEPAGE======================
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.home_page_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menuItem_update_subscription:
////                Toast.makeText(getApplicationContext(), "Clicked on Update Subscription", Toast.LENGTH_SHORT).show();
////                startActivity(new Intent(this, BillingActivity.class));
////                return true;
            case R.id.menuItem_homepage_edit_account:
                Toast.makeText(getApplicationContext(), "Clicked on Edit Account", Toast.LENGTH_SHORT).show();
                editProfile();
                return true;
//            case R.id.menuItem_homepage_download_from_blob:
//                Toast.makeText(getApplicationContext(), "Downloading from blob", Toast.LENGTH_SHORT).show();
//                return true;
            case R.id.menuItem_homepage_sign_out:
                Toast.makeText(getApplicationContext(), "Signing Out", Toast.LENGTH_SHORT).show();
                clearCache();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //===  END  ============HANDLING OF THE MAIN MENU FOR THE HOMEPAGE==============================


    //=========== FRAGMENT HANDLING & INTERACTION===================================================
    @Override
    public void onFragmentMessage(String TAG, Object data) {

        if (TAG.equals(MyStatsFragment.class.getSimpleName())) {

            Integer position = (Integer) data;

            MyStatsReportFragment statsReportFrag = (MyStatsReportFragment)
                    getSupportFragmentManager().findFragmentById(R.id.my_stats_report_fragment);

            if (statsReportFrag != null) {
                // If article frag is available, we're in two-pane updated_camera_fragment...
                // Call a method in the MyStatsReportFragment to update its content
                statsReportFrag.updateArticleView(position);

            } else {
                // If the frag is not available, we're in the one-pane updated_camera_fragment and must swap frags...
                // Create fragment and give it an argument for the selected article
                MyStatsReportFragment newFragment = new MyStatsReportFragment();
                Bundle args = new Bundle();
                args.putInt("position", position);
                newFragment.setArguments(args);
                loadFragment(newFragment, MyStatsReportFragment.class.getSimpleName());
            }
        }

//        if (TAG.equals(MyVideosFragment.class.getSimpleName())) {
//            Integer position = (Integer) data;
//            // TODO: MAKE LOAD NEW VIDEO PLAYER NOT OPEN A NEW FRAGMENT
////            SimpleMyVideosFragment newFragment = new SimpleMyVideosFragment();
////            Bundle args = new Bundle();
////            args.putInt("position",position);
////            newFragment.setArguments(args);
////            loadFragment(newFragment);
//
//        }
    }


    private void loadFragment(Fragment fragment, String fragmentTAG) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.homepage_fragment_container, fragment, fragmentTAG);
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }


    //TODO: THINK CAN REMOVE THIS NOW
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.v(TAG, "======================================================================= IS THIS HIT??????");

        switch (requestCode) {
            case PermissionsHelper.REQUESTCODE_READ_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Request Read Storage Permission Granted.");


                    // Reload current fragment
                    Fragment frg = null;
                    frg = getSupportFragmentManager().findFragmentByTag(GalleryFragment.class.getSimpleName());
                    final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    if (frg != null) {
                        ft.detach(frg);
                        ft.attach(frg);
                        ft.commit();
                    } else {
                        //dbHelper.loadVideosFromStorage(getApplicationContext());
                   //     title.setText("My Videos");
                        Fragment fragment = new GalleryFragment();
                        loadFragment(fragment, GalleryFragment.class.getSimpleName());
                    }
                } else {
                    Log.v(TAG, "Permission Denied");
                    break;
                }
            }
        }
    }


}//endof HomePageActivity.class
