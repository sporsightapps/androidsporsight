package com.dev.sporsightmobile.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.dev.sporsightmobile.notifications.SporsightNotificationHandler;
import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.notifications.NotificationSettings;
import com.dev.sporsightmobile.notifications.RegistrationIntentService;
import com.dev.sporsightmobile.utilities.Constants;
import com.dev.sporsightmobile.utilities.Helpers;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.microsoft.appcenter.push.Push;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.microsoft.identity.client.AuthenticationCallback;
import com.microsoft.identity.client.AuthenticationResult;
import com.microsoft.identity.client.ILoggerCallback;
import com.microsoft.identity.client.Logger;
import com.microsoft.identity.client.MsalClientException;
import com.microsoft.identity.client.MsalException;
import com.microsoft.identity.client.MsalServiceException;
import com.microsoft.identity.client.MsalUiRequiredException;
import com.microsoft.identity.client.PublicClientApplication;
import com.microsoft.identity.client.User;
import com.microsoft.windowsazure.notifications.NotificationsManager;

import org.json.JSONException;
import org.json.JSONObject;


public class WelcomeActivity extends AppCompatActivity {
    private static final String TAG = WelcomeActivity.class.getSimpleName();


    /* Azure AD variables */
    private PublicClientApplication publicClientApplication;
    private AuthenticationResult authResult;
    private String[] scopes;

    Button callAPIButton;
    AppSubClass state;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        AppCenter.start(getApplication(), "950d8897-abf7-49e3-b33e-df71ea86a4c1", Analytics.class, Crashes.class);
        AppCenter.start(getApplication(), "950d8897-abf7-49e3-b33e-df71ea86a4c1", Push.class);
        NotificationsManager.handleNotifications(this, NotificationSettings.SenderId, SporsightNotificationHandler.class);
        registerWithNotificationHubs();


        state = (AppSubClass) getApplicationContext();
        scopes = Constants.SCOPES.split("\\s+");


        callAPIButton = findViewById(R.id.APILoginButton);
        callAPIButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onCallAPIClicked(scopes);
            }
        });


        // Initialized the app context using MSAL
        publicClientApplication = state.getPublicClient();


        // Initialize the MSAL App context
        if (publicClientApplication == null) {


            publicClientApplication = new PublicClientApplication(
                    this.getApplicationContext(),
                    Constants.CLIENT_ID,
                    String.format(Constants.AUTHORITY, Constants.TENANT, Constants.SISU_POLICY)
            );


            state.setPublicClient(publicClientApplication);

            Logger.getInstance().setExternalLogger(new ILoggerCallback() {
                @Override
                public void log(String tag, Logger.LogLevel logLevel, String message, boolean containsPII) {
                    Log.d(tag, "MSAL_LOG: " + message);
                }
            });


        }

    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported by Google Play Services.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void registerWithNotificationHubs() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with FCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }


    // onActivityResult() - handles redirect from System browser
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        publicClientApplication.handleInteractiveRequestRedirect(requestCode, resultCode, data);

        super.onActivityResult(requestCode, resultCode, data);
    }


    // onCallApiClicked() - attempts to get tokens for our api, if it succeeds calls api & updates UI
    /* Use MSAL to acquireToken for the end-user
     *  Call API
     *  Pass UserInfo response data to AuthenticatedActivity ( Changed this to HomePageActivity )
     */
    private void onCallAPIClicked(String[] scopes) {
        Log.d(TAG, "Call API Clicked");


//            PublicClientApplication pApp = state.getPublicClient();
//            pApp.acquireToken(getActivity(),scopes,getAuthInteractiveCallback());

        /* Attempt to get a user and acquireTokenSilently
         * If this fails we will do an interactive request
         */
        try {
            User currentUser = Helpers.getUserByPolicy(publicClientApplication.getUsers(), Constants.SISU_POLICY);

            if (currentUser != null) {
                /* We have 1 user */

                publicClientApplication.acquireTokenSilentAsync(
                        scopes,
                        currentUser,
                        String.format(Constants.AUTHORITY, Constants.TENANT, Constants.SISU_POLICY),
                        false,
                        getAuthSilentCallback());

            } else {
                /* We have no user */
                publicClientApplication.acquireToken(this, scopes, getAuthInteractiveCallback());
            }
        } catch (MsalClientException e) {
            /* No token in cache, proceed with normal unauthenticated app experience */
            Log.d(TAG, "MSAL Exception Generated while getting users: " + e.toString());

        } catch (IndexOutOfBoundsException e) {
            Log.d(TAG, "User at this position does not exist: " + e.toString());
        }
    }


//===============================================================================================//
    // APP CALLBACKS FOR MSAL
//    ================================


    //returns activity so we can acquireToken within a callback for MSAL
    public Activity getActivity() {
        return this;
    }


    // getAuthSilentCallback() - callback defined to handle acquireTokenSilent() case
    /* Callback used in for silent acquireToken calls.
     * Looks if tokens are in the cache (refreshes if necessary and if we don't forceRefresh)
     * else errors that we need to do an interactive request.
     */
    private AuthenticationCallback getAuthSilentCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(AuthenticationResult authenticationResult) {
                /* Successfully got a token, call api now */
                Log.d(TAG, "Successfully authenticated");
                authResult = authenticationResult;
                state.setAuthenticationResult(authResult);
                Log.d(TAG, "@@___________________AUTH RESULT" + authResult.toString());
                Log.d(TAG, "@@___________________AUTH RESULT ACCESS TOKEN: " + authResult.getAccessToken());
                Log.d(TAG, "@@___________________AUTH RESULT USER: " + authResult.getUser());
                Log.d(TAG, "@@___________________AUTH RESULT IDTOKEN: " + authResult.getIdToken());
                Log.d(TAG, "@@___________________AUTH RESULT TenantID: " + authResult.getTenantId());
                Log.d(TAG, "@@___________________AUTH RESULT uniqueId: " + authResult.getUniqueId());
                Log.d(TAG, "@@___________________AUTH RESULT name: " + authResult.getUser().getName());

                /* Start authenticated activity */

               CheckUserInfo(authResult.getUniqueId(), authResult.getUser().getName());

            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString());

                if (exception instanceof MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                    assert true;

                } else if (exception instanceof MsalServiceException) {
                    /* Exception when communicating with the STS, likely config issue */
                    assert true;

                } else if (exception instanceof MsalUiRequiredException) {
                    /* Tokens expired or no session, retry with interactive */
                    publicClientApplication.acquireToken(getActivity(), scopes, getAuthInteractiveCallback());
                }
            }

            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.");
            }
        };
    }

    private void CheckUserInfo(String uniqueId, String name) {

        //Check if User Exists

        AndroidNetworking.get("https://sporsightapi.azurewebsites.net/api/Users/" + uniqueId)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("jsonex", "here");

                        //save user data locally
                        SharedPreferences prefs = getSharedPreferences("Sporsight", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("userGuid", uniqueId);
                        try {
                            editor.putString("userId", response.getString("id"));
                            editor.putString("name", name);
                            editor.commit();

                            Log.d("id", response.getString("id"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        goToHomePage();

                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.d("jsonex", "User Not Found...Creating New One");

                        if(error.getErrorCode() == 404)
                        {
                            AddUser(uniqueId, name);
                        }
                    }
                });
    }

    private void AddUser(String uniqueId, String name) {

        JSONObject jsonObject = null;
        try{
            jsonObject = new JSONObject();
            jsonObject.put("userGuid", uniqueId);
            jsonObject.put("FirstName", name);

        }catch(JSONException e)
        {
            Log.d("jsonex", e.getMessage());
        }

        AndroidNetworking.post("https://sporsightapi.azurewebsites.net/api/Users")
                .addJSONObjectBody(jsonObject) // posting json
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("jsonex", response.toString());

                        //save user data locally
                        SharedPreferences prefs = getSharedPreferences("Sporsight", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("userGuid", uniqueId);
                        editor.putString("name", name);

                        try {
                            editor.putString("userId", response.getString("id"));
                            editor.commit();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        goToHomePage();

                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.d("jsonex", error.getMessage());

                    }
                });
    }


    // getAuthInteractiveCallback() - callback defined to handle acquireToken() case.
    // Callback used for interactive request.
    // If succeeds we use the access token to call the api.
    // Does not check cache.
    private AuthenticationCallback getAuthInteractiveCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(AuthenticationResult authenticationResult) {
                /* Successfully got a token, call api now */
                Log.d(TAG, "$$$$$$$$$$$$ Successfully authenticated");
                Log.d(TAG, "$$$$$$$$$$$$ ID Token: " + authenticationResult.getIdToken());
                Log.d(TAG, "$$$$$$$$$$$$ User: " + authenticationResult.getUser());
                Log.d(TAG, "$$$$$$$$$$$$ TenantId: " + authenticationResult.getTenantId());
                Log.d(TAG, "$$$$$$$$$$$$ AccessToken: " + authenticationResult.getAccessToken());
                authResult = authenticationResult;
                state.setAuthenticationResult(authResult);


                /* Start authenticated activity */
                goToHomePage();
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString());

                if (exception instanceof MsalClientException) {
                    /* Exception inside MSAL, more info inside MsalError.java */
                    assert true;
                } else assert !(exception instanceof MsalServiceException) || true;
            }

            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.");
            }
        };
    }


    public void goToHomePage() {
        startActivity(new Intent(this, BaseHomePageActivity.class));
    }

}
