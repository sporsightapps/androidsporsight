package com.dev.sporsightmobile.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.model.DBVideo;
import com.dev.sporsightmobile.services.BlobStorageGetVideos;
import com.dev.sporsightmobile.services.BlobStorageService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
//import com.microsoft.azure.storage.blob.ListBlobItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.util.concurrent.ExecutionException;
import java.util.Date;


public class BlobStorageActivity extends AppCompatActivity {

    public static Uri videoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blob_storage);

        AndroidNetworking.initialize(getApplicationContext());


        if(getIntent().getParcelableExtra("EXTRA_URI").toString() != null)
        {
            String mParcableString = getIntent().getParcelableExtra("EXTRA_URI").toString();
            videoUri = Uri.parse(mParcableString);
            System.out.println(getIntent().getParcelableExtra("EXTRA_URI").toString());
            System.out.println(videoUri);

            Button save = findViewById(R.id.submitTitle);

            save.performClick();
        }

    }

    public void generateTask(View view) throws ExecutionException, InterruptedException {

      //  EditText videoName = findViewById(R.id.videoTitle);


        System.out.println("Calling generateTask()");

        if(videoUri != null)
        {
            BlobStorageService task = new BlobStorageService(new BlobStorageService.AsyncResponse() {

                Uri uri = videoUri;

                @Override
                public void processFinish(Boolean output) {
                    Intent mBaseAnnotatorActivity = new Intent(getApplicationContext(), BaseAnnotatorActivity.class);
                    mBaseAnnotatorActivity.putExtra("EXTRA_URI", uri);
                    mBaseAnnotatorActivity.putExtra("EXTRA_TITLE", "Yeet");
                    mBaseAnnotatorActivity.putExtra("EXTRA_STARTCODE", "Annotator");
                    mBaseAnnotatorActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(mBaseAnnotatorActivity);
                }
            });

            String userId = "";
            try {
                SharedPreferences prefs = getSharedPreferences("Sporsight", Context.MODE_PRIVATE);
                userId = prefs.getString("userId", "");

                Log.d("prefs", prefs.getString("userId", "none"));
            }catch(Exception e)
            {
                Log.d("Get user error", e.getMessage());

            }


            task.context = this.getApplicationContext();

            Date uploadDate = new Date();

            task.videoTitle = userId + "_video_" + uploadDate.toString().replaceAll("\\s", "_");
            task.videoUri = videoUri;

//            DBVideo video = new DBVideo(task.videoTitle, 1); //admin
//
//          Gson gson = new Gson();
//            String result = gson.toJson(video);


            JSONObject jsonObject = null;
            try{
                 jsonObject = new JSONObject();
                 jsonObject.put("title", task.videoTitle);
                 jsonObject.put("userId", Long.parseLong(userId));

            }catch(JSONException e)
            {
                Log.d("jsonex", e.getMessage());
            }

            AndroidNetworking.post("https://sporsightapi.azurewebsites.net/api/Videos")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setTag("test")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("jsonex", "here");

                            task.execute();
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle error
                            Log.d("jsonex", error.getMessage());

                        }
                    });


        }




    }


}
