package com.dev.sporsightmobile.interfaces;

public interface OnFragmentInteractionListener<T> {
     void onFragmentMessage(String TAG, T data);
}
