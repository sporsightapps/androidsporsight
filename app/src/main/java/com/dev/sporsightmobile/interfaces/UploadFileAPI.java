package com.dev.sporsightmobile.interfaces;

import com.dev.sporsightmobile.Retrofit.UploadFile;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface UploadFileAPI {

    // We are passing in a file here in order to do a POST request
    @Multipart
    // Our POST request includes the route 'openpose_upload'
    @POST("/openpose_upload")

    // We pass in the file we want to upload
    // We receive a response object of type UploadFile
    Call<UploadFile> uploadFile(@Part MultipartBody.Part file);

    // We are passing in the name of the video we want to retrieve after it is has finished processing
    @GET("/outputVideos/{nameOfVideo}")
    @Streaming
    // We pass in the name of the video that we want to retrieve
    Call<ResponseBody> getVideo(@Path("nameOfVideo") String nameOfVideo);

    // In order to receive a message indicating whether processing on the VM has concluded,
    // We send a GET request to this route including the name of the video we are checking to see whether it has finished
    @GET("/outputVideos/checkIfFileIsCreated/{nameOfVideo}")
    @Streaming
    Call<UploadFile> getMessage(@Path("nameOfVideo") String nameOfVideo);

    // In order to retrieve the JSON file containing data associated with each frame in a video,
    // We send a GET request to this route and we must pass in the name of the JSON file we want as well
    // The name of the JSON file is associated with the name of the processed video, e.g processed_player.json
    @GET("/outputJSON/{nameOfJSONFile}")
    @Streaming
    Call<JsonObject> getJSONFile(@Path("nameOfJSONFile") String nameOfJSONFile);

}
