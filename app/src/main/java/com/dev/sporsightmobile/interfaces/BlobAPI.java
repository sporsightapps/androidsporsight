package com.dev.sporsightmobile.interfaces;

import com.dev.sporsightmobile.Retrofit.Blobs;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface BlobAPI {

    // Our get request URL is "https://sporsightmobile.blob.core.windows.net/openpose/get_blobs/containerName"
    // We pass in the container name, "openpose" to retrieve all of the blobs located in this container
    @GET("get_blobs/{containerName}")
    @Streaming
    Call<Blobs> getBlobs(@Path("containerName") String containerName);

}
