//package com.dev.sporsight_mobile.activities.registration;
//
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.text.TextUtils;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.NumberPicker;
//import android.widget.Spinner;
//import android.widget.Toast;
//
//import com.dev.sporsight_mobile.R;
//import com.dev.sporsight_mobile.application.HomePageActivity;
//import com.dev.sporsight_mobile.entites.SchoolItem;
//import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
//import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
//
//import java.net.MalformedURLException;
//
//public class SchoolRegistrationActivity extends AppCompatActivity {
//
//    private MobileServiceClient mClient;
//    private MobileServiceTable<SchoolItem> mSchoolItemTable;
//    private EditText mSchoolName;
//    private EditText mAddress;
//    private Spinner mSport;
//    private NumberPicker mTeamSize;
//    private EditText mEmail;
//    private EditText mPassword;
//    private EditText mConfirmPassword;
//    private  Spinner mSecurityQuestionOne;
//    private  Spinner mSecurityQuestionTwo;
//    private  Spinner mSecurityQuestionThree;
//    private EditText mSecurityQuestionOneResponse;
//    private EditText mSecurityQuestionTwoResponse;
//    private EditText mSecurityQuestionThreeResponse;
//    public Button submit;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_school_registration);
//
//        try {
//            mClient = new MobileServiceClient("https://sporsight-mobile.azurewebsites.net", this);
//            mSchoolItemTable = mClient.getTable(SchoolItem.class);
//
//            mSchoolName = findViewById(R.id.editTextSchoolName);
//            mAddress = findViewById(R.id.editTextPostalAddress);
//            mSport = findViewById(R.id.spinnerSport);
//            mEmail = findViewById(R.id.editTextEmail);
//            mPassword = findViewById(R.id.editTextPassword);
//            mConfirmPassword = findViewById(R.id.editTextConfirmPassword);
//            mSecurityQuestionOne = findViewById(R.id.spinnerSecurityQuestionOne);
//            mSecurityQuestionTwo = findViewById(R.id.spinnerSecurityQuestionTwo);
//            mSecurityQuestionThree = findViewById(R.id.spinnerSecurityQuestionThree);
//            mSecurityQuestionOneResponse = findViewById(R.id.editTextSecurityQuestionOneResponse);
//            mSecurityQuestionTwoResponse =  findViewById(R.id.editTextSecurityQuestionTwoResponse);
//            mSecurityQuestionThreeResponse = findViewById(R.id.editTextSecurityQuestionThreeResponse);
//
//            mTeamSize =  findViewById(R.id.numberPickerPlayers);
//            String[] numbers = new String[20];
//            for(int i=0; i<numbers.length; i++) numbers[i] = Integer.toString(i);
//            mTeamSize.setMinValue(1);
//            mTeamSize.setMaxValue(20);
//            mTeamSize.setWrapSelectorWheel(false);
//            mTeamSize.setDisplayedValues(numbers);
//            mTeamSize.setValue(1);
//            mTeamSize.setEnabled(true);
//
//            submit =  findViewById(R.id.submitButton);
//            submit.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View view){
//                    if(checkDataEntered()) {addItem();}
//                }
//            });
//        } catch (MalformedURLException e) {
//            createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"));
//            e.printStackTrace();
//        } catch (Exception e){
//            createAndShowDialog(e);
//        }
//    }
//
//
//    public void addItem() {
//
//        if (mClient == null) return;
//
//        final SchoolItem item = new SchoolItem();
//
//        item.setmSchoolName(mSchoolName.getText().toString());
//        item.setmAddress(mAddress.getText().toString());
//        item.setmSport(mSport.getSelectedItem().toString());
//        item.setmTeamSize(mTeamSize.getValue());
//        item.setmEmail(mEmail.getText().toString());
//        item.setmPassword(mPassword.getText().toString());
//        item.setmConfirmPassword(mConfirmPassword.getText().toString());
//        item.setmSecurityQuestionOne(mSecurityQuestionOne.getSelectedItem().toString());
//        item.setmSecurityQuestionTwo(mSecurityQuestionTwo.getSelectedItem().toString());
//        item.setmSecurityQuestionThree(mSecurityQuestionThree.getSelectedItem().toString());
//        item.setmSecurityQuestionOneResponse(mSecurityQuestionOneResponse.getText().toString());
//        item.setmSecurityQuestionTwoResponse(mSecurityQuestionTwoResponse.getText().toString());
//        item.setmSecurityQuestionThreeResponse(mSecurityQuestionThreeResponse.getText().toString());
//
//        // Insert the new item
//        mSchoolItemTable.insert(item);
//        Toast.makeText(this,"Registration Sumbitted",Toast.LENGTH_LONG).show();
//        goToHomePage();
//    }
//
//    private void createAndShowDialog(Exception exception) {
//        Throwable ex = exception;
//        if(exception.getCause() != null){ ex = exception.getCause(); }
//        createAndShowDialog(ex.getCause().getMessage());
//    }
//    private void createAndShowDialog(final String message) {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage(message);
//        builder.setTitle("Error");
//        builder.create().show();
//    }
//
//    boolean checkDataEntered() {
//
//        if(isEmpty(mSchoolName)){
//            mSchoolName.setError("School Name Required");
//            Toast.makeText(this,"Please enter the name of the school.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//        if(isEmpty(mAddress)){
//            mAddress.setError("Address Required");
//            Toast.makeText(this,"Please enter address.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//        if(!isEmail(mEmail)){
//            mEmail.setError("Invalid Email Address");
//            Toast.makeText(this,"Please enter valid email address.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//        if(sportIsNone(mSport)){
//            Toast.makeText(this,"Please choose a sport.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//
//        //==== NEED TO IMPLEMENT Check Password
//
//        if(isEmpty(mSecurityQuestionOneResponse)){
//            mSecurityQuestionOneResponse.setError("Answer Required");
//            Toast.makeText(this,"Please answer security question one.",Toast.LENGTH_SHORT).show();
//
//            return false;
//        }
//        if(isEmpty(mSecurityQuestionTwoResponse)){
//            mSecurityQuestionTwoResponse.setError("Answer Required");
//            Toast.makeText(this,"Please answer security question two.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if(isEmpty(mSecurityQuestionThreeResponse)){
//            mSecurityQuestionThreeResponse.setError("Answer Required");
//            Toast.makeText(this,"Please answer security question three.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//    return true;
//    }
//
//    boolean sportIsNone(Spinner spinner){
//        return spinner.getSelectedItem().toString().equals("None");
//    }
//
//    boolean isEmpty(EditText text){
//        CharSequence str = text.getText().toString();
//        return TextUtils.isEmpty(str);
//    }
//
//    boolean isEmail(EditText text){
//        CharSequence email = text.getText().toString();
//        return (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches());
//    }
//
//    public void goToHomePage() {
//        Intent intent = new Intent(this, HomePageActivity.class);
//        startActivity(intent);
//    }
//
//}
//
