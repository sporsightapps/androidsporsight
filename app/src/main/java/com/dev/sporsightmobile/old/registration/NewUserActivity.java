//package com.dev.sporsight_mobile.activities.registration;
//
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//
//import com.dev.sporsight_mobile.R;
////import com.dev.sporsight_mobile.activities.login.LoginActivity;
//import com.dev.sporsight_mobile.application.MainActivity;
//
//public class NewUserActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_new_user);
//    }
//
//    public void goLoginPage(View view){
//        Intent intent = new Intent(this,MainActivity.class);
//        startActivity(intent);
//
//    }
//
//    public void goToPlayerRegistrationActivity(View view){
//        Intent intent = new Intent(this,PlayerRegistrationActivity.class);
//        startActivity(intent);
//    }
//
//    public void goToSchoolRegistrationActivity(View view){
//        Intent intent = new Intent(this,SchoolRegistrationActivity.class);
//        startActivity(intent);
//    }
//
//}
