package com.dev.sporsightmobile.old.registration;

public class PlayerItem {

    public PlayerItem() { }

    @com.google.gson.annotations.SerializedName("id")
    private String mId;
    public String getmId() { return mId; }
    public void setmId(String mId) { this.mId = mId; }

    @com.google.gson.annotations.SerializedName("firstName")
    private String mFirstName;
    public String getmFirstName() { return mFirstName; }
    public void setmFirstName(String mFirstName) { this.mFirstName = mFirstName; }

    @com.google.gson.annotations.SerializedName("lastName")
    private String mLastName;
    public String getmLastName() {return mLastName;}
    public void setmLastName(String mLastName) {this.mLastName = mLastName;}

    @com.google.gson.annotations.SerializedName("email")
    private String mEmail;
    public String getmEmail() { return mEmail;}
    public void setmEmail(String mEmail) { this.mEmail = mEmail; }

    @com.google.gson.annotations.SerializedName("password")
    private String mPassword;
    public String getmPassword() { return mPassword; }
    public void setmPassword(String mPassword) { this.mPassword = mPassword; }

    @com.google.gson.annotations.SerializedName("confirmPassword")
    private String mConfirmPassword;
    public String getmConfirmPassword() { return mConfirmPassword; }
    public void setmConfirmPassword(String mConfirmPassword) { this.mConfirmPassword = mConfirmPassword; }

    @com.google.gson.annotations.SerializedName("securityQuestionOne")
    private String mSecurityQuestionOne;
    public String getmSecurityQuestionOne() { return mSecurityQuestionOne; }
    public void setmSecurityQuestionOne(String mSecurityQuestionOne) { this.mSecurityQuestionOne = mSecurityQuestionOne; }

    @com.google.gson.annotations.SerializedName("securityQuestionTwo")
    private String mSecurityQuestionTwo;
    public String getmSecurityQuestionTwo() { return mSecurityQuestionTwo; }
    public void setmSecurityQuestionTwo(String mSecurityQuestionTwo) { this.mSecurityQuestionTwo = mSecurityQuestionTwo; }

    @com.google.gson.annotations.SerializedName("securityQuestionThree")
    private String mSecurityQuestionThree;
    public String getmSecurityQuestionThree() { return mSecurityQuestionThree; }
    public void setmSecurityQuestionThree(String mSecurityQuestionThree) { this.mSecurityQuestionThree = mSecurityQuestionThree; }



    @com.google.gson.annotations.SerializedName("securityQuestionOneResponse")
    private String mSecurityQuestionOneResponse;
    public String getmSecurityQuestionOneResponse() { return mSecurityQuestionOneResponse; }
    public void setmSecurityQuestionOneResponse(String mSecurityQuestionOneResponse) {
        this.mSecurityQuestionOneResponse = mSecurityQuestionOneResponse;
    }
    @com.google.gson.annotations.SerializedName("securityQuestionTwoResponse")
    private String mSecurityQuestionTwoResponse;
    public String getmSecurityQuestionTwoResponse() { return mSecurityQuestionTwoResponse; }
    public void setmSecurityQuestionTwoResponse(String mSecurityQuestionTwoResponse) {
        this.mSecurityQuestionTwoResponse = mSecurityQuestionTwoResponse;
    }

    @com.google.gson.annotations.SerializedName("securityQuestionThreeResponse")
    private String mSecurityQuestionThreeResponse;
    public String getmSecurityQuestionThreeResponse() { return mSecurityQuestionThreeResponse; }
    public void setmSecurityQuestionThreeResponse(String mSecurityQuestionThreeResponse) {
        this.mSecurityQuestionThreeResponse = mSecurityQuestionThreeResponse; }


    @com.google.gson.annotations.SerializedName("registrationCode")
    private String mRegistrationCode;
    public String getmRegistrationCode() { return mRegistrationCode; }
    public void setmRegistrationCode(String mRegistrationCode) { this.mRegistrationCode = mRegistrationCode; }

    @com.google.gson.annotations.SerializedName("accountStatus")
    private String mAccountStatus;
    public String getmAccountStatus() { return mAccountStatus; }
    public void setmAccountStatus(String mAccountStatus) { this.mAccountStatus = mAccountStatus; }

//    @com.google.gson.annotations.SerializedName("complete")
//    private boolean mComplete;
//    public boolean isComplete() {return mComplete;}
//    public void setComplete(boolean complete){mComplete = complete;}

    public String getName(){ return getmFirstName() + " " + getmLastName(); }

}