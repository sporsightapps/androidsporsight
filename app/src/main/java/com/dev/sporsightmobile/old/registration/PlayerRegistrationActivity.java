//package com.dev.sporsight_mobile.activities.registration;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.text.TextUtils;
//import android.util.Patterns;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Spinner;
//import android.widget.Toast;
//import com.dev.sporsight_mobile.entites.PlayerItem;
//import com.dev.sporsight_mobile.R;
//import com.dev.sporsight_mobile.application.HomePageActivity;
//import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
//import com.microsoft.windowsazure.mobileservices.http.OkHttpClientFactory;
//import com.microsoft.windowsazure.mobileservices.table.MobileServiceTable;
//import com.squareup.okhttp.OkHttpClient;
//import java.net.MalformedURLException;
//import java.util.concurrent.TimeUnit;
//
//public class PlayerRegistrationActivity extends AppCompatActivity {
//
//    private MobileServiceClient mClient;
//    private MobileServiceTable<PlayerItem> mPlayerTable;
//    private EditText mFirstName;
//    private EditText mLastName;
//    private EditText mEmail;
//    private EditText mPassword;
//    private EditText mConfirmPassword;
//    private Spinner  mSecurityQuestionOne;
//    private EditText mSecurityQuestionOneResponse;
//    private Spinner  mSecurityQuestionTwo;
//    private EditText mSecurityQuestionTwoResponse;
//    private Spinner  mSecurityQuestionThree;
//    private EditText mSecurityQuestionThreeResponse;
//    private EditText mRegistrationCode;
//    public  Button submit;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_player_registration);
//
//        try {
//            mClient = new MobileServiceClient(
//                    "https://sporsight-mobile.azurewebsites.net",
//                    this);
//
//            mClient.setAndroidHttpClientFactory(new OkHttpClientFactory() {
//                @Override
//                public OkHttpClient createOkHttpClient() {
//                    OkHttpClient client = new OkHttpClient();
//                    client.setReadTimeout(20, TimeUnit.SECONDS);
//                    client.setWriteTimeout(20, TimeUnit.SECONDS);
//                    return client;
//                }
//            });
//
//            mPlayerTable = mClient.getTable(PlayerItem.class);
//
//            mFirstName = findViewById(R.id.editTextFirstName);
//            mLastName = findViewById(R.id.editTextLastName);
//            mEmail = findViewById(R.id.editTextEmail);
//            mPassword = findViewById(R.id.editTextPassword);
//            mConfirmPassword =  findViewById(R.id.editTextConfirmPassword);
//            mSecurityQuestionOne =    findViewById(R.id.spinnerSecurityQuestionOne);
//            mSecurityQuestionTwo =    findViewById(R.id.spinnerSecurityQuestionTwo);
//            mSecurityQuestionThree =  findViewById(R.id.spinnerSecurityQuestionThree);
//            mSecurityQuestionOneResponse =   findViewById(R.id.editTextSecurityQuestionOneResponse);
//            mSecurityQuestionTwoResponse =   findViewById(R.id.editTextSecurityQuestionTwoResponse);
//            mSecurityQuestionThreeResponse = findViewById(R.id.editTextSecurityQuestionThreeResponse);
//            mRegistrationCode =  findViewById(R.id.editTextRegistrationCode);
//
//            submit =  findViewById(R.id.submitButton);
//            submit.setOnClickListener(new View.OnClickListener(){
//                @Override
//                public void onClick(View view){
//                    if(checkDataEntered()) {addItem();}
//                }
//            });
//        } catch (MalformedURLException e) {
//            createAndShowDialog(new Exception("There was an error creating the Mobile Service. Verify the URL"));
//            e.printStackTrace();
//        } catch (Exception e){
//            createAndShowDialog(e);
//        }
//    }
//
//    public void addItem() {
//
//        if (mClient == null) return;
//
//        final PlayerItem item = new PlayerItem();
//
//        item.setmFirstName(mFirstName.getText().toString());
//        item.setmLastName(mLastName.getText().toString());
//        item.setmEmail(mEmail.getText().toString());
//        item.setmPassword(mPassword.getText().toString());
//        item.setmConfirmPassword(mConfirmPassword.getText().toString());
//        item.setmRegistrationCode(mRegistrationCode.getText().toString());
//        item.setmSecurityQuestionOne(mSecurityQuestionOne.getSelectedItem().toString());
//        item.setmSecurityQuestionTwo(mSecurityQuestionTwo.getSelectedItem().toString());
//        item.setmSecurityQuestionThree(mSecurityQuestionThree.getSelectedItem().toString());
//        item.setmSecurityQuestionOneResponse(mSecurityQuestionOneResponse.getText().toString());
//        item.setmSecurityQuestionTwoResponse(mSecurityQuestionTwoResponse.getText().toString());
//        item.setmSecurityQuestionThreeResponse(mSecurityQuestionThreeResponse.getText().toString());
//        //Make check to set account status depending on their registration code
//        item.setmAccountStatus("New Player");
//
//        mPlayerTable.insert(item);
//
//        Toast.makeText(this,"Registration Submitted",Toast.LENGTH_LONG).show();
//        goToHomePage();
//    }
//
//
//    private void createAndShowDialog(Exception exception) {
//        Throwable ex = exception;
//        if(exception.getCause() != null){ ex = exception.getCause(); }
//        createAndShowDialog(ex.getCause().getMessage());
//    }
//    private void createAndShowDialog(final String message) {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setMessage(message);
//        builder.setTitle("Error");
//        builder.create().show();
//    }
//
//
//    boolean checkDataEntered(){
//
//        if(isEmpty(mFirstName)){
//            mFirstName.setError("First Name Required");
//            Toast.makeText(this,"Please enter first name.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if(isEmpty(mLastName)){
//            mLastName.setError("Last Name Required");
//            Toast.makeText(this,"Please enter last name.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if(!isEmail(mEmail)){
//            mEmail.setError("Invalid Email Address");
//            Toast.makeText(this,"Please enter valid email address.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//
//        //CHECK PASSWORD
//
//        if(isEmpty(mSecurityQuestionOneResponse)){
//            mSecurityQuestionOneResponse.setError("Answer Required");
//            Toast.makeText(this,"Please answer security question one.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if(isEmpty(mSecurityQuestionTwoResponse)){
//            mSecurityQuestionTwoResponse.setError("Answer Required");
//            Toast.makeText(this,"Please answer security question two.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if(isEmpty(mSecurityQuestionThreeResponse)){
//            mSecurityQuestionThreeResponse.setError("Answer Required");
//            Toast.makeText(this,"Please answer security question three.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if(isEmpty(mRegistrationCode)/* || !isValidRegistartionCode(mRegistrationCode)*/ ){
//            mRegistrationCode.setError("Code Required");
//            Toast.makeText(this,"Please enter a registration code.",Toast.LENGTH_SHORT).show();
//            return false;
//        }
////        if(!isValidRegistrationCode(mRegistrationCode)){
////            mRegistrationCode.setError("Invalid Code");
////            Toast.makeText(this,"Invalid Registration Code.",Toast.LENGTH_SHORT).show();
////            return false;
////        }
//        return true;
//    }
//    boolean isEmpty(EditText text){
//        CharSequence str = text.getText().toString();
//        return TextUtils.isEmpty(str);
//    }
//    boolean isEmail(EditText text){
//        CharSequence email = text.getText().toString();
//        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
//    }
////    boolean isValidRegistrationCode(EditText text){
////
////        //NEED TO IMPLEMENT REGISTRATION CODE LOOKUP
////        return true;
////    }
//
//    public void goToHomePage(){
//        Intent intent = new Intent(this,HomePageActivity.class);
//        startActivity(intent);
//    }
//}