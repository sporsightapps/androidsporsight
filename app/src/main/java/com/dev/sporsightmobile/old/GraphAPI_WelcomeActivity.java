//package com.dev.sporsightmobile.old;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.*;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.android.volley.toolbox.Volley;
//import com.microsoft.identity.client.*;
//import org.json.JSONObject;
//import com.dev.sporsightmobile.R;
//
//
//
//// TODO:  NOTE: THIS DOESN'T SHOW UP IN THE MANIFEST
//public class GraphAPI_WelcomeActivity extends AppCompatActivity {
//
//    final static String CLIENT_ID = "972119be-8349-4316-b1c6-9d26f0d31e55";
//    final static String SCOPES[] = {"https://graph.microsoft.com/User.Read"};
//    final static String MSGRAPH_URL = "https://graph.microsoft.com/v1.0/me";
//
//    /* UI & Debugging Variables */
//    private static final String TAG = GraphAPI_WelcomeActivity.class.getSimpleName();
//    Button callGraphButton;
//    Button signOutButton;
//
//    /* Azure AD Variables */
//    private PublicClientApplication sampleApp;
//    private AuthenticationResult authResult;
//    private Handler mHandler;
//
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_graph_api__welcome);
//
//        //Setting up the views
//        callGraphButton = findViewById(R.id.callGraph);
//        signOutButton = findViewById(R.id.clearCache);
//
//
//
//        callGraphButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                onCallGraphClicked();
//            }
//        });
//
//        signOutButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                onSignOutClicked();
//            }
//        });
//
//        /* Configure your sample app and save state for this activity */
//        sampleApp = null;
//        if (sampleApp == null) {
//            sampleApp = new PublicClientApplication(
//                    this.getApplicationContext(),
//                    CLIENT_ID);
//        }
//
//        /* Attempt to get a user and acquireTokenSilent
//         * If this fails we do an interactive request
//         */
//        List<User> users = null;
//
//        try {
//            users = sampleApp.getUsers();
//
//            if (users != null && users.size() == 1) {
//                /* We have 1 user */
//
//                sampleApp.acquireTokenSilentAsync(SCOPES, users.get(0), getAuthSilentCallback());
//            } else {
//                /* We have no user */
//
//                /* Let's do an interactive request */
//                sampleApp.acquireToken(this, SCOPES, getAuthInteractiveCallback());
//            }
//        } catch (MsalClientException e) {
//            Log.d(TAG, "MSAL Exception Generated while getting users: " + e.toString());
//
//        } catch (IndexOutOfBoundsException e) {
//            Log.d(TAG, "User at this position does not exist: " + e.toString());
//        }
//
//    }
//
//
////
//// App callbacks for MSAL
//// ==============================================================================================//
//// getActivity() - returns activity so we can acquireToken within a callback
//// getAuthSilentCallback() - callback defined to handle acquireTokenSilent() case
//// getAuthInteractiveCallback() - callback defined to handle acquireToken() case
////
//
//    public Activity getActivity() {
//        return this;
//    }
//
//    /* Callback method for acquireTokenSilent calls
//     * Looks if tokens are in the cache (refreshes if necessary and if we don't forceRefresh)
//     * else errors that we need to do an interactive request.
//     */
//    private AuthenticationCallback getAuthSilentCallback() {
//        return new AuthenticationCallback() {
//            @Override
//            public void onSuccess(AuthenticationResult authenticationResult) {
//                /* Successfully got a token, call Graph now */
//                Log.d(TAG, "Successfully authenticated");
//
//                /* Store the authResult */
//                authResult = authenticationResult;
//
//                /* call graph */
//                callGraphAPI();
//
//                /* update the UI to post call Graph state */
//                updateSuccessUI();
//            }
//
//            @Override
//            public void onError(MsalException exception) {
//                /* Failed to acquireToken */
//                Log.d(TAG, "Authentication failed: " + exception.toString());
//
//                if (exception instanceof MsalClientException) {
//                    /* Exception inside MSAL, more info inside MsalError.java */
//                } else if (exception instanceof MsalServiceException) {
//                    /* Exception when communicating with the STS, likely config issue */
//                } else if (exception instanceof MsalUiRequiredException) {
//                    /* Tokens expired or no session, retry with interactive */
//                }
//            }
//
//            @Override
//            public void onCancel() {
//                /* User cancelled the authentication */
//                Log.d(TAG, "User cancelled login.");
//            }
//        };
//    }
//
//    /* Callback used for interactive request.  If succeeds we use the access
//     * token to call the Microsoft Graph. Does not check cache
//     */
//    private AuthenticationCallback getAuthInteractiveCallback() {
//        return new AuthenticationCallback() {
//            @Override
//            public void onSuccess(AuthenticationResult authenticationResult) {
//                /* Successfully got a token, call graph now */
//                Log.d(TAG, "Successfully authenticated");
//                Log.d(TAG, "ID Token: " + authenticationResult.getIdToken());
//
//                /* Store the auth result */
//                authResult = authenticationResult;
//
//                /* call Graph */
//                callGraphAPI();
//
//                /* update the UI to post call Graph state */
//                updateSuccessUI();
//            }
//
//            @Override
//            public void onError(MsalException exception) {
//                /* Failed to acquireToken */
//                Log.d(TAG, "Authentication failed: " + exception.toString());
//
//                if (exception instanceof MsalClientException) {
//                    /* Exception inside MSAL, more info inside MsalError.java */
//                } else if (exception instanceof MsalServiceException) {
//                    /* Exception when communicating with the STS, likely config issue */
//                }
//            }
//
//            @Override
//            public void onCancel() {
//                /* User cancelled the authentication */
//                Log.d(TAG, "User cancelled login.");
//            }
//        };
//    }
//
//    /* Set the UI for successful token acquisition data */
//    private void updateSuccessUI() {
//        callGraphButton.setVisibility(View.INVISIBLE);
//        signOutButton.setVisibility(View.VISIBLE);
//        findViewById(R.id.welcome).setVisibility(View.VISIBLE);
//        ((TextView) findViewById(R.id.welcome)).setText("Welcome, " +
//                authResult.getUser().getName());
//        findViewById(R.id.graphData).setVisibility(View.VISIBLE);
//    }
//
//    /* Use MSAL to acquireToken for the end-user
//     * Callback will call Graph api w/ access token & update UI
//     */
//    private void onCallGraphClicked() {
//        sampleApp.acquireToken(getActivity(), SCOPES, getAuthInteractiveCallback());
//    }
//
//    /* Handles the redirect from the System Browser */
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        sampleApp.handleInteractiveRequestRedirect(requestCode, resultCode, data);
//    }
//
//
//    /* Use Volley to make an HTTP request to the /me endpoint from MS Graph using an access token */
//    private void callGraphAPI() {
//        Log.d(TAG, "Starting volley request to graph");
//
//        /* Make sure we have a token to send to graph */
//        if (authResult.getAccessToken() == null) { return; }
//
//
//        RequestQueue queue = Volley.newRequestQueue(this);
//        JSONObject parameters = new JSONObject();
//
//        try {
//            parameters.put("key", "value");
//        } catch (Exception e) {
//            Log.d(TAG, "Failed to put parameters: " + e.toString());
//        }
//
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, MSGRAPH_URL,
//                parameters, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                /* Successfully called graph, process data and send to UI */
//                Log.d(TAG, "Response: " + response.toString());
//
//                updateGraphUI(response);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d(TAG, "Error: " + error.toString());
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() {
//                Map<String, String> headers = new HashMap<>();
//                headers.put("Authorization", "Bearer " + authResult.getAccessToken());
//                return headers;
//            }
//        };
//
//        Log.d(TAG, "Adding HTTP GET to Queue, Request: " + request.toString());
//
//        request.setRetryPolicy(new DefaultRetryPolicy(
//                3000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        queue.add(request);
//    }
//
//    /* Sets the Graph response */
//    private void updateGraphUI(JSONObject graphResponse) {
//        TextView graphText = findViewById(R.id.graphData);
//        graphText.setText(graphResponse.toString());
//    }
//
//
//    /* Clears a user's tokens from the cache.
//     * Logically similar to "sign out" but only signs out of this app.
//     */
//    private void onSignOutClicked() {
//
//        /* Attempt to get a user and remove their cookies from cache */
//        List<User> users = null;
//
//        try {
//            users = sampleApp.getUsers();
//
//            if (users == null) {
//                /* We have no users */
//
//            } else if (users.size() == 1) {
//                /* We have 1 user */
//                /* Remove from token cache */
//                sampleApp.remove(users.get(0));
//                updateSignedOutUI();
//
//            } else {
//                /* We have multiple users */
//                for (int i = 0; i < users.size(); i++) {
//                    sampleApp.remove(users.get(i));
//                }
//            }
//
//            Toast.makeText(getBaseContext(), "Signed Out!", Toast.LENGTH_SHORT).show();
//
//        } catch (MsalClientException e) {
//            Log.d(TAG, "MSAL Exception Generated while getting users: " + e.toString());
//
//        } catch (IndexOutOfBoundsException e) {
//            Log.d(TAG, "User at this position does not exist: " + e.toString());
//        }
//    }
//
//    /* Set the UI for signed-out user */
//    private void updateSignedOutUI() {
//        callGraphButton.setVisibility(View.VISIBLE);
//        signOutButton.setVisibility(View.INVISIBLE);
//        findViewById(R.id.welcome).setVisibility(View.INVISIBLE);
//        findViewById(R.id.graphData).setVisibility(View.INVISIBLE);
//        ((TextView) findViewById(R.id.graphData)).setText(R.string.no_Data);
//    }
//
//    private void showMessage(final String msg) {
//        getHandler().post(new Runnable() {
//
//            @Override
//            public void run() {
//                Toast.makeText(GraphAPI_WelcomeActivity.this, msg, Toast.LENGTH_LONG).show();
//            }
//        });
//    }
//
//    private Handler getHandler() {
//        if (mHandler == null) {
//            return new Handler(GraphAPI_WelcomeActivity.this.getMainLooper());
//        }
//        return mHandler;
//}
//
////    public void goToRegister(View view) {
////        Intent intent = new Intent(this, NewUserActivity.class);
////        startActivity(intent);
////    }
//
////    @Override
////    public void onSuccess(AuthenticationResult authenticationResult) {
////
////    }
////
////    @Override
////    public void onError(MsalException exception) {
////        // Check the exception type.
////        if (exception instanceof MsalClientException) {
////            // This means errors happened in the sdk itself, could be network, Json parse, etc. Check MsalError.java
////            // for detailed list of the errors.
////            showMessage(exception.getMessage());
////            showConnectErrorUI(exception.getMessage());
////        } else if (exception instanceof MsalServiceException) {
////            // This means something is wrong when the sdk is communication to the service, mostly likely it's the client
////            // configuration.
////            showMessage(exception.getMessage());
////            showConnectErrorUI(exception.getMessage());
////        } else if (exception instanceof MsalUiRequiredException) {
////            // This explicitly indicates that developer needs to prompt the user, it could be refresh token is expired, revoked
////            // or user changes the password; or it could be that no token was found in the token cache.
////            AuthenticationManager mgr = AuthenticationManager.getInstance();
////
////            mgr.callAcquireToken(GraphAPI_WelcomeActivity.this, this);
////        }
////
////    }
////
////    @Override
////    public void onError(Exception exception) {
////        showMessage(exception.getMessage());
////        showConnectErrorUI(exception.getMessage());
////    }
////
////    @Override
////    public void onCancel() {
////        showMessage("User cancelled the flow.");
////        showConnectErrorUI("User cancelled the flow.");
////
////    }
////
//
//
//
//
//
//
//    private void showConnectErrorUI() {
////        mConnectButton.setVisibility(View.VISIBLE);
////        mConnectProgressBar.setVisibility(View.GONE);
////        mTitleTextView.setText(R.string.title_text_error);
////        mTitleTextView.setVisibility(View.VISIBLE);
////        mDescriptionTextView.setText(R.string.connect_text_error);
////        mDescriptionTextView.setVisibility(View.VISIBLE);
////        Toast.makeText(
////                ConnectActivity.this,
////                R.string.connect_toast_text_error,
////                Toast.LENGTH_LONG).show();
//    }
//    private void showConnectErrorUI(String errorMessage) {
////        mConnectButton.setVisibility(View.VISIBLE);
////        mConnectProgressBar.setVisibility(View.GONE);
////        mTitleTextView.setText(R.string.title_text_error);
////        mTitleTextView.setVisibility(View.VISIBLE);
////        mDescriptionTextView.setText(errorMessage);
////        mDescriptionTextView.setVisibility(View.VISIBLE);
////        Toast.makeText(
////                ConnectActivity.this,
////                R.string.connect_toast_text_error,
////                Toast.LENGTH_LONG).show();
//    }
//
//
//
//}