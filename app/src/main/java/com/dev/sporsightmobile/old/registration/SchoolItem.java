//package com.dev.sporsightmobile.old.registration;
//
//public class SchoolItem {
//
//    public SchoolItem(){}
//
//    @com.google.gson.annotations.SerializedName("id")
//    private String mId;
//    public String getmId() { return mId; }
//    public void setmId(String mId) { this.mId = mId; }
//
//    @com.google.gson.annotations.SerializedName("schoolName")
//    private String mSchoolName;
//    public String getmSchoolName() { return mSchoolName; }
//    public void setmSchoolName(String mFirstName) { this.mSchoolName = mSchoolName; }
//
//    @com.google.gson.annotations.SerializedName("address")
//    private String mAddress;
//    public String getmAddress() { return mAddress; }
//    public void setmAddress(String mAddress) { this.mAddress = mAddress; }
//
//    @com.google.gson.annotations.SerializedName("sport")
//    private String mSport;
//    public String getmSport() { return mSport; }
//    public void setmSport(String mSport) { this.mSport = mSport; }
//
//    @com.google.gson.annotations.SerializedName("teamSize")
//    private Integer mTeamSize;
//    public Integer getmTeamSize() { return mTeamSize; }
//    public void setmTeamSize(Integer mTeamSize) { this.mTeamSize = mTeamSize; }
//
//    @com.google.gson.annotations.SerializedName("email")
//    private String mEmail;
//    public String getmEmail() { return mEmail;}
//    public void setmEmail(String mEmail) { this.mEmail = mEmail; }
//
//    @com.google.gson.annotations.SerializedName("password")
//    private String mPassword;
//    public String getmPassword() { return mPassword; }
//    public void setmPassword(String mPassword) { this.mPassword = mPassword; }
//
//    @com.google.gson.annotations.SerializedName("confirmPassword")
//    private String mConfirmPassword;
//    public String getmConfirmPassword() { return mConfirmPassword; }
//    public void setmConfirmPassword(String mConfirmPassword) { this.mConfirmPassword = mConfirmPassword; }
//
//    @com.google.gson.annotations.SerializedName("securityQuestionOne")
//    private String mSecurityQuestionOne;
//    public String getmSecurityQuestionOne() { return mSecurityQuestionOne; }
//    public void setmSecurityQuestionOne(String mSecurityQuestionOne) { this.mSecurityQuestionOne = mSecurityQuestionOne; }
//
//    @com.google.gson.annotations.SerializedName("securityQuestionTwo")
//    private String mSecurityQuestionTwo;
//    public String getmSecurityQuestionTwo() { return mSecurityQuestionTwo; }
//    public void setmSecurityQuestionTwo(String mSecurityQuestionTwo) { this.mSecurityQuestionTwo = mSecurityQuestionTwo; }
//
//    @com.google.gson.annotations.SerializedName("securityQuestionThree")
//    private String mSecurityQuestionThree;
//    public String getmSecurityQuestionThree() { return mSecurityQuestionThree; }
//    public void setmSecurityQuestionThree(String mSecurityQuestionThree) { this.mSecurityQuestionThree = mSecurityQuestionThree; }
//
//    @com.google.gson.annotations.SerializedName("securityQuestionOneResponse")
//    private String mSecurityQuestionOneResponse;
//    public String getmSecurityQuestionOneResponse() { return mSecurityQuestionOneResponse; }
//    public void setmSecurityQuestionOneResponse(String mSecurityQuestionOneResponse) {
//        this.mSecurityQuestionOneResponse = mSecurityQuestionOneResponse;
//    }
//    @com.google.gson.annotations.SerializedName("securityQuestionTwoResponse")
//    private String mSecurityQuestionTwoResponse;
//    public String getmSecurityQuestionTwoResponse() { return mSecurityQuestionTwoResponse; }
//    public void setmSecurityQuestionTwoResponse(String mSecurityQuestionTwoResponse) {
//        this.mSecurityQuestionTwoResponse = mSecurityQuestionTwoResponse;
//    }
//
//    @com.google.gson.annotations.SerializedName("securityQuestionThreeResponse")
//    private String mSecurityQuestionThreeResponse;
//    public String getmSecurityQuestionThreeResponse() { return mSecurityQuestionThreeResponse; }
//    public void setmSecurityQuestionThreeResponse(String mSecurityQuestionThreeResponse) {
//        this.mSecurityQuestionThreeResponse = mSecurityQuestionThreeResponse; }
//
//
//
//}
