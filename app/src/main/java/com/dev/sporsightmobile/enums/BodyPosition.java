package com.dev.sporsightmobile.enums;

public enum BodyPosition {
    SIDE_LEFT, SIDE_RIGHT, FRONT_LEFT, FRONT_RIGHT;

    public boolean isSideView() {
        return this == BodyPosition.SIDE_LEFT || this == BodyPosition.SIDE_RIGHT;
    }

    public boolean isFrontView() {
        return !this.isSideView();
    }
}