//package com.dev.sporsightmobile.payments;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.DialogInterface;
//import android.os.Bundle;
//import android.util.Log;
//
//import com.dev.sporsightmobile.payments.util.IabBroadcastReceiver;
//import com.dev.sporsightmobile.payments.util.IabBroadcastReceiver.IabBroadcastListener;
////import com.dev.sporsightmobile.payments.util.IabHelper;
//import com.dev.sporsightmobile.payments.util.IabResult;
//import com.dev.sporsightmobile.payments.util.Purchase;
//
//import com.dev.sporsightmobile.R;
//
//
//public class PaymentsActivity extends Activity implements IabBroadcastListener,
//        DialogInterface.OnClickListener {
//    private static final String TAG = PaymentsActivity.class.getSimpleName();
//
//    // The helper object
//    IabHelper mHelper;
//
//    // Provides purchase notification while this app is running
//    IabBroadcastReceiver mBroadcastReceiver;
//
//
//    /**
//     * Initialize the Google Pay API on creation of the activity
//     *
//     * @see Activity#onCreate(android.os.Bundle)
//     */
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payments);
//
//
//
//
//
//
//
//    }
//
//
//
//
//    @Override
//    public void onClick(DialogInterface dialogInterface, int i) {
//
//    }
//
//
//    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
//        public void onConsumeFinished(Purchase purchase, IabResult result) {
//            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);
//
//            // if we were disposed of in the meantime, quit.
//            if (mHelper == null) return;
//
//            }
//        };
//
//
//
//
//
//
//
//
//    // We're being destroyed. It's important to dispose of the helper here!
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//
//        // very important:
//        if (mBroadcastReceiver != null) {
//            unregisterReceiver(mBroadcastReceiver);
//        }
//
//        // very important:
//        Log.d(TAG, "Destroying helper.");
//        if (mHelper != null) {
//            mHelper.disposeWhenFinished();
//            mHelper = null;
//        }
//    }
//
//
//
//
//
//    void complain(String message) {
//        Log.e(TAG, "Payments Error: " + message);
//        alert("Error: " + message);
//    }
//
//
//    void alert(String message) {
//        AlertDialog.Builder bld = new AlertDialog.Builder(this);
//        bld.setMessage(message);
//        bld.setNeutralButton("OK", null);
//        Log.d(TAG, "Showing alert dialog: " + message);
//        bld.create().show();
//    }
//
//
//    @Override
//    public void receivedBroadcast() {
//
//    }
//}