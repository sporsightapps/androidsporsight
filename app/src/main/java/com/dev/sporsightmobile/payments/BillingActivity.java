///*
// * Copyright 2012 Google Inc. All Rights Reserved.
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *      http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//
//
//package com.dev.sporsightmobile.payments;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Intent;
//import android.content.IntentFilter;
//import androidx.annotation.Nullable;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.ImageButton;
//import android.widget.TextView;
//
//import com.dev.sporsightmobile.R;
//import com.dev.sporsightmobile.payments.util.IabHelper;
//import com.dev.sporsightmobile.payments.util.IabHelper.IabAsyncInProgressException;
//import com.dev.sporsightmobile.payments.util.IabResult;
//import com.dev.sporsightmobile.payments.util.Inventory;
//import com.dev.sporsightmobile.payments.util.Purchase;
//import com.dev.sporsightmobile.payments.util.IabBroadcastReceiver;
//import com.dev.sporsightmobile.payments.util.IabBroadcastReceiver.IabBroadcastListener;
//
//
//public class BillingActivity extends Activity implements IabBroadcastListener {
//
//    private static final String TAG = "SporSight.billing";
//    private static final String BASE_64_ENCODED_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlL5jUDRYk+LiFgQNvAQSivfEmQazMTJ1Ep3dgbpkgakDeMFpWVYREz+94XEeMgyQh/hLQyAamE4KZOcaJ+/h+xRNMjwGPISygRlXGgmnE4t8hpCFx4j5M+oPf/WwQB2Ei/IDPMfqwIUa+I/Ilx0Clffg1TUIRyUZQ/MjLKvjyzl0nsxXxnopawFO9xq5NbuZD/MIZIo2JDdt/euYesmE1RhKKW4x0z9AhHwt2cLoYJAvfEhDI5eormNoc/rIvgyQUK7BKhVBUPnMzLTWDIZ0spaYWKrE0AH9P6CdfFovR7yz4180cWACkLuZiDD5XdvLz8Zjl2cKeL0ZmmkKkO0dAwIDAQAB";
//
//    IabHelper mHelper;
//
//    // Provides purchase notification while this app is running
//    IabBroadcastReceiver mBroadcastReceiver;
//
//
//    private Boolean mIsPremiumUser = false;
//
//    static final String SKU_PREMIUM = "update_account_subscription";
//
//    private TextView mSubscritionStatusView;
//
//
//    // (arbitrary) request code for the purchase flow
//    static final int RC_REQUEST = 10001;
//
//
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_billing);
//
//        //
//        //checkUserSubscription(); TODO: CHECK USER's ACCOUNT TYPE <--------------------------------
//
//
//        mSubscritionStatusView = findViewById(R.id.textView_subscriptionStatus);
//
//        ImageButton mBuyPremiumButton = findViewById(R.id.buyPremiumButton);
////        mBuyPremiumButton.setEnabled(false);
//
//        // Create the helper, passing it our context and the public key to verify signatures with
//        Log.d(TAG, "Creating IAB helper.");
//        mHelper = new IabHelper(this,BASE_64_ENCODED_PUBLIC_KEY);
//
//        // enable debug logging (for a production application, you should set this to false).
////        mHelper.enableDebugLogging(true);
//
//
//        Log.d(TAG, "Starting setup.");
//        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//            @Override
//            public void onIabSetupFinished(IabResult result) {
//                Log.d(TAG, "Setup finished.");
//
//                // Have we been disposed of in the meantime? If so, quit.
//                if (mHelper == null) return;
//
//                if(!result.isSuccess()){
//                    Log.d(TAG, "In-app Billing setup failed: " + result);
//                    complain("Problem setting up in-app billing: " + result);
//                    return;
//
//                }
//                // Have we been disposed of in the meantime? If so, quit.
//                if (mHelper == null) return;
//
//
//                    // Important: Dynamically register for broadcast messages about updated purchases.
//                    // We register the receiver here instead of as a <receiver> in the Manifest
//                    // because we always call getPurchases() at startup, so therefore we can ignore
//                    // any broadcasts sent while the app isn't running.
//
//                    // Note: registering this listener in an Activity is a bad idea, but is done here
//                    // because this is a SAMPLE. Regardless, the receiver must be registered after
//                    // IabHelper is setup, but before first call to getPurchases().
//
//                    mBroadcastReceiver = new IabBroadcastReceiver(BillingActivity.this);
//                    IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);
//                    registerReceiver(mBroadcastReceiver, broadcastFilter);
//
//                // IAB is fully set up. Now, let's get an inventory of stuff we own.
//                Log.d(TAG, "In-app Billing setup successful: " + result);
//
//
//                try {
//                    Log.d(TAG, "Querying inventory.");
//                    mHelper.queryInventoryAsync(mGotInventoryListener);
//                } catch (IabAsyncInProgressException e) {
//                    complain("Error querying inventory. Another async operation in progress.");
//                }
//
//            }
//        });
//
//
//
//    }
//
//
//    // Listener that's called when we finish querying the items and subscriptions we own
//    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
//        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
//            Log.d(TAG, "Query inventory finished.");
//
//
//            // Have we been disposed of in the meantime? If so, quit.
//            if (mHelper == null) return;
//
//            // Is it a failure?
//            if (result.isFailure()) {
//                complain("Failed to query inventory: " + result);
//                return;
//            }
//
//            Log.d(TAG, "Query inventory was successful.");
//
//            /*
//             * Check for items we own. Notice that for each purchase, we check
//             * the developer payload to see if it's correct! See
//             * verifyDeveloperPayload().
//             */
//
//
//            // Do we have a premium account
//            Purchase premiumPurchase = inventory.getPurchase(SKU_PREMIUM);
//            mIsPremiumUser = (premiumPurchase != null && verifyDeveloperPayload(premiumPurchase));
//            Log.d(TAG, "User is " + (mIsPremiumUser ? "PREMIUM" : "NOT PREMIUM"));
//
//
//            updateUi();
//            setWaitScreen(false);
//            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
//
//        }
//
//    };
//
//
//
//
//
//
//    // User Clicked on the Buy Premium Subscription Button.
//    public void buyPremiumClick(View view) {
//        Log.d(TAG, "Buy Premium button clicked.");
//
//        if(mIsPremiumUser){
//            complain("You are already a premium user.");
//            return;
//        }
//
//        // launch the premium subscription purchase UI flow.
//        // We will be notified of completion via mPurchaseFinishedListener
//        setWaitScreen(true);
//        Log.d(TAG, "Launching purchase flow for premium subscription.");
//
//        /* TODO: for security, generate your payload here for verification. See the comments on
//         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
//         *        an empty string, but on a production app you should carefully generate this. */
//        String payload = "";
//
//        try {
//            mHelper.launchPurchaseFlow(this, SKU_PREMIUM, RC_REQUEST, mPurchaseFinishedListener, payload);
//        } catch (IabAsyncInProgressException e) {
//            complain("Error launching purchase flow. Another async operation in progress.");
//            setWaitScreen(false);
//            e.printStackTrace();
//        }
//    }
//
//
//
//    // Callback for when a purchase is finished
//    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
//
//        @Override
//        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
//            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
//
//            // if we were disposed of in the meantime, quit.
//            if (mHelper == null) return;
//
//            if(result.isFailure()){
//                complain("Error purchasing: " + result);
//                setWaitScreen(false);
//                return;
//            }
//
//            if (!verifyDeveloperPayload(purchase)) {
//                complain("Error purchasing. Authenticity verification failed.");
//                setWaitScreen(false);
//                return;
//            }
//
//            Log.d(TAG, "**********Purchase successful.**********************");
//
//            if (purchase.getSku().equals(SKU_PREMIUM)){
////                mBuyPremiumButton.setEnabled(false);
//                mIsPremiumUser = true;
//                updateUserSubscription();
//                try {
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } catch (IabAsyncInProgressException e) {
//                    complain("Error consuming gas. Another async operation in progress.");
//                    setWaitScreen(false);
//                    return;
//                }
//
//
//                updateUi();
//
//            }
//
//
//
//
//        }
//
//
//    };
//
//
//
//
//    //Called when consumption is complete
//    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
//            new IabHelper.OnConsumeFinishedListener() {
//                public void onConsumeFinished(Purchase purchase, IabResult result) {
//                    Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);
//
//                    // if we were disposed of in the meantime, quit.
//                    if (mHelper == null) return;
//
//                    if (result.isSuccess()) {
//                        Log.d(TAG, "Consumption successful.");
//                        alert("Successful Purchase");
//                        mIsPremiumUser = true;
//                        // updateUserSubscription(); //TODO: <===============================================
//                    } else {
//                        complain("Error while consuming: " + result);
//                    }
//                    updateUi();
//                    setWaitScreen(false);
//                    Log.d(TAG, "End consumption flow.");
//
//
//                }
//            };
//
//
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
//        if (mHelper == null) return;
//
//        // Pass on the activity result to the helper for handling
//
//        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
//            // not handled, so handle it ourselves (here's where you'd
//            // perform any handling of activity results not related to in-app
//            // billing...
//            super.onActivityResult(requestCode, resultCode, data);
//        } else {
//            Log.d(TAG, "onActivityResult handled by IABUtil.");
//        }
//
//
//
//    }
//
//
//    // We're being destroyed. It's important to dispose of the helper here!
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//
//        // very important:
//        if (mBroadcastReceiver != null) {
//            unregisterReceiver(mBroadcastReceiver);
//        }
//
//        // very important:
//        Log.d(TAG, "Destroying helper.");
//        if (mHelper != null) {
//            mHelper.disposeWhenFinished();
//            mHelper = null;
//        }
//    }
//
//
//
//    @Override
//    public void receivedBroadcast() {
//        // Received a broadcast notification that the inventory of items has changed
//        Log.d(TAG, "Received broadcast notification. Querying inventory.");
//        try {
//            mHelper.queryInventoryAsync(mGotInventoryListener);
//        } catch (IabAsyncInProgressException e) {
//            complain("Error querying inventory. Another async operation in progress.");
//        }
//    }
//
//    /** Verifies the developer payload of a purchase. */
//    boolean verifyDeveloperPayload(Purchase p) {
//        String payload = p.getDeveloperPayload();
//
//        /*
//         * TODO: verify that the developer payload of the purchase is correct. It will be
//         * the same one that you sent when initiating the purchase.
//         *
//         * WARNING: Locally generating a random string when starting a purchase and
//         * verifying it here might seem like a good approach, but this will fail in the
//         * case where the user purchases an item on one device and then uses your app on
//         * a different device, because on the other device you will not have access to the
//         * random string you originally generated.
//         *
//         * So a good developer payload has these characteristics:
//         *
//         * 1. If two different users purchase an item, the payload is different between them,
//         *    so that one user's purchase can't be replayed to another user.
//         *
//         * 2. The payload must be such that you can verify it even when the app wasn't the
//         *    one who initiated the purchase flow (so that items purchased by the user on
//         *    one device work on other devices owned by the user).
//         *
//         * Using your own server to store and verify developer payloads across app
//         * installations is recommended.
//         */
//
//
//        return true;
//    }
//
//
//
//    void complain(String message) {
//        Log.e(TAG, "**** TrivialDrive Error: " + message);
//        alert("Error: " + message);
//    }
//
//    void alert(String message) {
//        AlertDialog.Builder bld = new AlertDialog.Builder(this);
//        bld.setMessage(message);
//        bld.setNeutralButton("OK", null);
//        Log.d(TAG, "Showing alert dialog: " + message);
//        bld.create().show();
//    }
//
//
//    // Enables or disables the "please wait" screen.
//    void setWaitScreen(boolean set) {
//        findViewById(R.id.screen_main).setVisibility(set ? View.GONE : View.VISIBLE);
//        findViewById(R.id.screen_wait).setVisibility(set ? View.VISIBLE : View.GONE);
//    }
//
//
//
//
//    void checkUserSubscription(){
//        //TODO: here we can double check with the webservice <=====================================
//    }
//
//
//    void updateUserSubscription(){
//        //TODO: CALL WEBSERVICE TO UPDATE USER SUBSCRIPTION <======================================
//    }
//
//
//
//
//    public void updateUi(){
//
//        //Updating the TextView to show account status.
//        if(mIsPremiumUser){
//            mSubscritionStatusView.setText(getString(R.string.premium_user));
//        } else {
//            mSubscritionStatusView.setText(getString(R.string.nonpremium_user));
//        }
//
//        // "Upgrade" button is only visible if the user is not premium
//        findViewById(R.id.buyPremiumButton).setVisibility(mIsPremiumUser ? View.GONE : View.VISIBLE);
//
//
//    }
//
//
//
//
//
//}
//
//
