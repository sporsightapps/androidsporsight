package com.dev.sporsightmobile.utilities;

import java.util.UUID;

public class Constants {


    //Azure Active Directory B2C Configuration
    public final static String AUTHORITY = "https://login.microsoftonline.com/tfp/%s/%s"; // Didn't Change


    public final static String TENANT = "sporsightiam.onmicrosoft.com"; // Tenant name
    public final static String TENANT_ID = "078fc54f-9b91-4a13-ae89-79c07a6a3018"; // Tenant name


    public final static String CLIENT_ID = "7b66c778-b9c6-4e73-acf6-d26a969978be"; // Client ID (this is the webportal application_id)
    public final static String APPLICATION_ID = "8d7c4ba9-bf6b-41f5-a7dd-e1bcc13b8f8a"; // Sporsight_Mobile


    //Used for different types of bluetooth connections.
    public final static UUID MY_UUID_SECURE = UUID.fromString(APPLICATION_ID);
    public final static UUID MY_UUID_INSECURE = UUID.fromString("8d7c4ba9-bf6b-41f5-a7dd-e1bcc13b8f8b");


    public final static String SUBSCRIPTION_ID = "2a7db1db-a074-4b4c-b608-9c98e1c343af"; // For B2C

    public final static String SCOPES = "https://sporsightiam.onmicrosoft.com/hello/demo.read";
    //    final static String SCOPES = "https://graph.microsoft.com/User.Read";


    public final static String B2C_REPLY_URL_TWO = "http://localhost:3000/auth/openid/return";
    public final static String B2C_REPLY_URL = "https://sporsight.azurewebsites.net/auth/openid/return";


//    final static String MSGRAPH_URL = "https://graph.microsoft.com/v1.0/me";

    /* Resource URI of the endpoint which will be accessed */
    public static final String RESOURCE_ID = "https://graph.windows.net/sporsightiam.onmicrosoft.com/tenantDetails/?api-version=1.6";


    //Sign-in/Sign-up policies for B2C.
    public final static String SISU_POLICY_OLD = "B2C_1_SUSI";
    public final static String SISU_POLICY = "b2c_1_susi";

    //Edit policies for B2C.
    public final static String EDIT_PROFILE_POLICY = "B2C_1_edit_profile";


    public static final String MSGRAPH_URL = "https://graph.microsoft.com/v1.0/me";


    // For Bluetooth Chat
    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    //Codes for the Bluetooth Connection to trigger recording start/stop
    public static final String BTCODE_START = "BT_RECORDING_CODE:START";
    public static final String BTCODE_STOP = "BT_RECORDING_CODE:STOP";


    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";


}
