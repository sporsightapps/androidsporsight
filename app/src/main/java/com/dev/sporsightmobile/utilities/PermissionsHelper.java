package com.dev.sporsightmobile.utilities;


import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;

import static androidx.core.app.ActivityCompat.requestPermissions;


public class PermissionsHelper {
    private static final String TAG = PermissionsHelper.class.getSimpleName();

    private static final int GRANT_REQUEST_CODE = 1;

    // Permissions for the Camera Functionality
    private static final String WRITE_EXTERNAL_STORAGE_PERMISSION = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private static final String RECORD_AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO;
    private static final String CAMERA_PERMISSION = Manifest.permission.CAMERA;

    public static final int REQUESTCODE_READ_STORAGE = 113;


    public static boolean checkRequestReadStoragePermissions(Activity activity) {
        Log.v(TAG, "Check/Request - Read Storage Permission");
        Log.v(TAG, "Check/Request - Checking...");
        boolean hasPermission = (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        Log.v(TAG, "Check/Request - Has Permission: " + hasPermission);
        if (!hasPermission) {
            Log.v(TAG, "Check/Request - Requesting Permission");
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUESTCODE_READ_STORAGE);
        }

        return hasPermission;
    }


    // Does the application need Camera Permissions?
    public static boolean needsCameraPermissions(Activity activity) {
        Log.v(TAG, "========= CHECKING IF NEED CAMERA PERMISSION");
        return !((activity.checkSelfPermission(WRITE_EXTERNAL_STORAGE_PERMISSION) == PackageManager.PERMISSION_GRANTED)
                && (activity.checkSelfPermission(RECORD_AUDIO_PERMISSION) == PackageManager.PERMISSION_GRANTED)
                && (activity.checkSelfPermission(CAMERA_PERMISSION) == PackageManager.PERMISSION_GRANTED));

    }

    // Requests Camera Permissions.
    public static void requestCameraPermissions(Activity activity) {
        Log.v(TAG, "============ REQUESTING CAMERA PERMISSIONS");
        activity.requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE_PERMISSION, RECORD_AUDIO_PERMISSION, CAMERA_PERMISSION}, GRANT_REQUEST_CODE);
    }


}
