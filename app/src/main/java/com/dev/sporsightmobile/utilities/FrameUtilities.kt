package com.dev.sporsightmobile.utilities

import android.content.Context
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.util.Log
import com.dev.sporsightmobile.model.FrameAnalysisResult
import com.dev.sporsightmobile.model.FrameInfo
import com.dev.sporsightmobile.services.DrawingViewScalingService
import com.dev.sporsightmobile.services.VideoScalingInformation
import org.tensorflow.lite.examples.posenet.lib.*
import java.util.concurrent.CancellationException
import java.util.stream.Collectors
import kotlin.math.abs

class FrameUtilities(val context: Context) {
    companion object {
        @JvmField
        val modelWidth: Int = 257

        @JvmField
        val modelHeight: Int = 257

        @JvmField
        val modelInputRatio = modelHeight.toFloat() / modelWidth

        @JvmField
        val cropAttenuation = 0.2 // 1 = No attenuation

        @JvmField
        val maxDifference = 1e-5

        @JvmField
        var cancelPromise = false
    }

    private val useGpu = false;
    private val posenet: Posenet = Posenet(context, if (useGpu)
        Device.GPU
    else
        Device.CPU
    )
    private val imageSegmentationModelExecutor: ImageSegmentationModelExecutor = ImageSegmentationModelExecutor(context, useGpu)

    fun getFrameAtMillisecond(videoPath: String, millisecond: Long): Bitmap {
        val mediaMetadataRetriever = MediaMetadataRetriever()
        mediaMetadataRetriever.setDataSource(videoPath)

        val bitmap: Bitmap = mediaMetadataRetriever.getFrameAtTime(millisecond, MediaMetadataRetriever.OPTION_CLOSEST)

        mediaMetadataRetriever.close()

        return bitmap
    }

    fun cropBitmap(bitmap: Bitmap): Triple<Bitmap, Int, Int> {
        val bitmapRatio = bitmap.height.toFloat() / bitmap.width

        // Checks if the bitmap has similar aspect ratio as the required model input.
        return when {
            abs(modelInputRatio - bitmapRatio) < maxDifference -> Triple(bitmap, 0, 0)
            modelInputRatio < bitmapRatio -> {
                // New image is taller so we are height constrained.
                val croppableHeight = bitmap.height - (bitmap.width.toFloat() / modelInputRatio)
                /* Cropping the full croppableHeight might cut into the person we're trying to run pose
                  estimation on. Rather than crop the full amount, we crop a smaller percentage of the
                  overall croppable height and scale out the difference.
                 */
                val attenuatedCroppableHeight = croppableHeight * cropAttenuation
                return Triple(Bitmap.createBitmap(
                        bitmap,
                        0,
                        (attenuatedCroppableHeight / 2).toInt(),
                        bitmap.width,
                        (bitmap.height - attenuatedCroppableHeight).toInt()
                ), 0, attenuatedCroppableHeight.toInt())
            }
            else -> {
                val croppableWidth = bitmap.width - (bitmap.height.toFloat() * modelInputRatio)
                // See above for crop explanation
                val attenuatedCroppableWidth = croppableWidth * cropAttenuation
                return Triple(Bitmap.createBitmap(
                        bitmap,
                        (attenuatedCroppableWidth / 2).toInt(),
                        0,
                        (bitmap.width - attenuatedCroppableWidth).toInt(),
                        bitmap.height
                ), attenuatedCroppableWidth.toInt(), 0)
            }
        }
    }

    fun scaleBitmap(croppedBitmap: Bitmap): Bitmap {
        return Bitmap.createScaledBitmap(croppedBitmap, modelWidth, modelHeight, true)
    }

    private fun processFrame(frameInfo: FrameInfo, frameBitmap: Bitmap): FrameAnalysisResult {
        return processFrame(frameInfo, frameBitmap, false);
    }

    private fun processFrame(frameInfo: FrameInfo, frameBitmap: Bitmap, getImageSegmentation: Boolean): FrameAnalysisResult {
        val (index, timeInMilliseconds) = frameInfo;
        val (croppedBitmap, croppedWidth, croppedHeight) = cropBitmap(frameBitmap)

        // Created scaled version of bitmap for model input.
        val scaledBitmap = scaleBitmap(croppedBitmap)

        val poseEstimation = posenet.estimateSinglePose(scaledBitmap)

        val originalVideoWidth = frameBitmap.width
        val originalVideoHeight = frameBitmap.height
        val videoScalingInformation = VideoScalingInformation(originalVideoWidth, originalVideoHeight, croppedWidth, croppedHeight)

        val scaledPoseEstimation = DrawingViewScalingService.scalePoseEstimationToDrawingView(poseEstimation, videoScalingInformation)

        val imageSegmentation = if (getImageSegmentation) imageSegmentationModelExecutor.execute(scaledBitmap) else null
        val scaledImageSegmentation = if (getImageSegmentation && imageSegmentation != null) DrawingViewScalingService.scaleImageSegmentationToDrawingView(imageSegmentation, videoScalingInformation) else null

        return FrameAnalysisResult(timeInMilliseconds,
                poseEstimation,
                imageSegmentation,
                scaledPoseEstimation,
                scaledImageSegmentation,
                croppedWidth,
                croppedHeight,
                frameInfo)
    }

    fun processFrameForVisualization(videoPath: String, millisecond: Long): FrameAnalysisResult {
        val frameBitmap = getFrameAtMillisecond(videoPath, millisecond)
        val frameInfo = FrameInfo(-1, millisecond);
        return processFrame(frameInfo, frameBitmap, true)
    }

    fun getVideoFrame(mediaMetadataRetriever: MediaMetadataRetriever, frameInfo: FrameInfo): Bitmap? {
        val (frameIndex, timeInMilliseconds) = frameInfo
        try {
            val frame = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                mediaMetadataRetriever.getFrameAtIndex(frameIndex.toInt())
            } else {
                mediaMetadataRetriever.getFrameAtTime(timeInMilliseconds * 1000, MediaMetadataRetriever.OPTION_CLOSEST)
            }

            return frame;
        } catch (exception: IllegalArgumentException) {
            // No more frames
        } catch (exception: Exception) {
            Log.d(FrameUtilities.toString(), "Frame extraction blew up")
            Log.d(FrameUtilities.toString(), exception.stackTrace.toString())
        }

        return null;
    }

    fun getVideoFramesInfo(mediaMetadataRetriever: MediaMetadataRetriever, videoPath: Uri): ArrayList<FrameInfo> {
        val duration = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong();
        val frameCount: Long = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_FRAME_COUNT).toLong();
        val framerate = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CAPTURE_FRAMERATE)
        val timeStep = if (framerate != null && !"".equals(framerate)) 1000.0 / framerate.toDouble()
        else if ((frameCount != null && !"".equals(frameCount)) && (duration != null && !"".equals(duration))) duration.toDouble() / (frameCount)
        else 1000.0 / 30

        val frames = ArrayList<FrameInfo>();

        for (frameIndex in 0 until frameCount) {
            val timeInMilliseconds = (frameIndex * timeStep).toLong()
            frames.add(FrameInfo(frameIndex.toInt(), timeInMilliseconds));
        }

        return frames
    }

    fun processEntireVideo(videoPath: Uri): List<FrameAnalysisResult> {
        val mediaMetadataRetriever = MediaMetadataRetriever()
        mediaMetadataRetriever.setDataSource(videoPath.toString())

        val framesInfo = getVideoFramesInfo(mediaMetadataRetriever, videoPath);

        // TODO: Figure out why parallelStream dies here
        val processedFrames: List<FrameAnalysisResult> = framesInfo.stream().map { frameInfo ->
            if (cancelPromise) {
                cancelPromise = false
                throw CancellationException();
            }
            val frameBitmap = getVideoFrame(mediaMetadataRetriever, frameInfo)
            val processingResult = if (frameBitmap != null)
                processFrame(frameInfo, frameBitmap)
            else null
            Log.d(FrameUtilities.toString(), processingResult.toString());

            return@map processingResult;
        }.collect(Collectors.toList()).filterNotNull()

        mediaMetadataRetriever.close()

        return processedFrames;
    }
}
