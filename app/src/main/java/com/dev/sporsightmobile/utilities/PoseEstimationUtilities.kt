package com.dev.sporsightmobile.utilities

import android.media.MediaMetadataRetriever
import android.util.DisplayMetrics
import com.dev.sporsightmobile.enums.BodyPosition
import com.dev.sporsightmobile.fragments.ExoPlayerFragment
import org.tensorflow.lite.examples.posenet.lib.KeyPoint
import org.tensorflow.lite.examples.posenet.lib.Position
import java.util.function.Function
import java.util.stream.Collectors
import kotlin.math.roundToInt

class PoseEstimationUtilities {
    companion object {
        @JvmField
        var bodyPosition = BodyPosition.SIDE_RIGHT
        @JvmField
        var isSide = true
        @JvmField
        var isLeftSide = false
    }
}
