package com.dev.sporsightmobile.utilities

import android.graphics.Bitmap
import android.util.Log

class ImageMaskUtilities {
    private fun isPopulated(imageSegmentation: Bitmap, x: Int, y: Int): Boolean {
        if (x < 0 || x > imageSegmentation.width || y < 0 || y > imageSegmentation.height) {
            Log.d("ImageMaskUtilities", "We tried to check out of bounds of the image segmentation bitmap");
        }

        val sanitizedX = if (x >= imageSegmentation.width) imageSegmentation.width - 1 else if (x < 0) 0 else x
        val sanitizedY = if (y >= imageSegmentation.height) imageSegmentation.height - 1 else if (y < 0) 0 else y

        return imageSegmentation.getPixel(sanitizedX, sanitizedY) != 0;
    }

    private fun isRowPopulated(imageSegmentation: Bitmap, y: Int, startBound: Int, endBound: Int): Boolean {
        var isPopulated = false;
        for (x in startBound..endBound) {
            if (isPopulated(imageSegmentation, x, y)) {
                isPopulated = true
            }
        }
        return isPopulated;
    }
    private fun isColumnPopulated(imageSegmentation: Bitmap, x: Int, startBound: Int, endBound: Int): Boolean {
        var isPopulated = false;
        for (y in startBound..endBound) {
            if (isPopulated(imageSegmentation, x, y)) {
                isPopulated = true
            }
        }
        return isPopulated;
    }

    fun findMinY(imageSegmentation: Bitmap, startX: Int, startY: Int, endX: Int, endY: Int): Int {
        var minY = startY;

        for (y in startY..endY) {
            if (minY == startY && isRowPopulated(imageSegmentation, y, startX, endX)) {
                minY = y;
            }
        }

        return minY;
    }

    fun findMaxY(imageSegmentation: Bitmap, startX: Int, startY: Int, endX: Int, endY: Int): Int {
        var maxY = endY;

        for (y in endY downTo startY) {
            if (maxY == endY && isRowPopulated(imageSegmentation, y, startX, endX)) {
                maxY = y;
            }
        }

        return maxY;
    }

    fun findMinX(imageSegmentation: Bitmap, startX: Int, startY: Int, endX: Int, endY: Int): Int {
        var minX = startX;

        for (x in startX..endX) {
            if (minX == startX && isColumnPopulated(imageSegmentation, x, startY, endY)) {
                minX = x;
            }
        }

        return minX;
    }

    fun findMaxX(imageSegmentation: Bitmap, startX: Int, startY: Int, endX: Int, endY: Int): Int {
        var maxX = endX;

        for (x in endX downTo startX) {
            if (maxX == endX && isColumnPopulated(imageSegmentation, x, startY, endY)) {
                maxX = x;
            }
        }

        return maxX;
    }
}