package com.dev.sporsightmobile.utilities;

import org.tensorflow.lite.examples.posenet.lib.BodyPart;
import org.tensorflow.lite.examples.posenet.lib.KeyPoint;

import java.util.List;

public class PoseUtility {
    public static KeyPoint findKeypointByBodyPart(List<KeyPoint> keypoints, BodyPart bodypart) {
        for (KeyPoint kp : keypoints) {
            if (kp.getBodyPart().equals(bodypart)) {
                return kp;
            }
        }
        return null;
    }
}