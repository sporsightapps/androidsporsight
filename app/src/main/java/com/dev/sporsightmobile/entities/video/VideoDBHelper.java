package com.dev.sporsightmobile.entities.video;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import android.util.Log;

import com.dev.sporsightmobile.entities.video.VideoContract.VideoEntry;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class VideoDBHelper extends SQLiteOpenHelper {
    private static final String TAG = VideoDBHelper.class.getSimpleName();

    private Context mContext;


    private static final String[] LOAD_VIDEOS_PROJECTION = new String[]{
            MediaStore.Video.Media._ID,           // Unique row ID.
            MediaStore.Video.Media.DATA,          // Path to the file on disk.
            MediaStore.Video.Media.DISPLAY_NAME,  // Display name of the file.
            MediaStore.Video.Media.ARTIST,        // The Artist who created the video file.
            MediaStore.Video.Media.DURATION,      // Duration of the file in ms, Type: Integer(long)
            MediaStore.Video.Media.DATE_ADDED,    // Time the file was added to the media provider
            MediaStore.Video.Media.DATE_MODIFIED  // Time since last modified in seconds since 1970
    };


    public VideoDBHelper(Context context) {
        super(context, VideoEntry.DATABASE_NAME, null, VideoEntry.DATABASE_VERSION);
        mContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_VIDEOLIST_TABLE = "CREATE TABLE " +
                VideoEntry.TABLE_NAME + " (" +
                VideoEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                VideoEntry.COLUMN_VIDEO_TITLE + " TEXT NOT NULL, " +
                VideoEntry.COLUMN_VIDEO_SPORT + " TEXT, " +
                VideoEntry.COLUMN_VIDEO_LENGTH + " TEXT NOT NULL, " +
                VideoEntry.COLUMN_VIDEO_CREATOR + " TEXT, " +
                VideoEntry.COLUMN_VIDEO_CATEGORY + " TEXT, " +
                VideoEntry.COLUMN_VIDEO_FILEPATH + " TEXT, " +
                VideoEntry.COLUMN_VIDEO_ANNOTATED + " BOOLEAN NOT NULL, " +
                VideoEntry.COLUMN_VIDEO_FAVORITED + " BOOLEAN NOT NULL, " +
                VideoEntry.COLUMN_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ");";
        db.execSQL(SQL_CREATE_VIDEOLIST_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + VideoEntry.TABLE_NAME);
        onCreate(db);
    }


    public boolean insertVideo(String video_title, String video_sport, String video_length,
                               String video_creator, String video_category, Boolean video_annotated,
                               Boolean video_favorited, String video_timestamp, String video_filepath) {

        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();

//        contentValues.put("video_id", video_id);
        contentValues.put("video_title", video_title);
        contentValues.put("video_sport", video_sport);
        contentValues.put("video_length", video_length);
        contentValues.put("video_creator", video_creator);
        contentValues.put("video_category", video_category);
        contentValues.put("video_annotated", video_annotated);
        contentValues.put("video_favorited", video_favorited);
        contentValues.put("video_timestamp", video_timestamp);
        contentValues.put("video_filepath", video_filepath);


        Log.v(TAG, "Inserting Video into SQLite DB");
        Log.v(TAG, "title: " + video_title);
        Log.v(TAG, "sport: " + video_sport);
        Log.v(TAG, "length: " + video_length);
        Log.v(TAG, "creator: " + video_creator);
        Log.v(TAG, "category: " + video_category);
        Log.v(TAG, "annotated: " + video_annotated);
        Log.v(TAG, "favorited: " + video_favorited);
        Log.v(TAG, "timestamp: " + video_timestamp);
        Log.v(TAG, "filePath: " + video_filepath);


        db.insert(VideoEntry.TABLE_NAME, null, contentValues);


        return true;
    }


    public Cursor getVideo(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from videoList where id=" + id + "", null);
    }


    public Cursor getAllVideos() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.query(VideoContract.VideoEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                VideoContract.VideoEntry.COLUMN_TIMESTAMP + " DESC" //DECENDING ORDER
        );
        return res;
    }

    // Abdool Shakur's addition to enable filtering videos by categories
    public Cursor getFilteredVideos(String category) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + VideoContract.VideoEntry.TABLE_NAME + " WHERE video_filepath LIKE \"%"+category+"%\"", new String[]{});
        return res;
    }


    public boolean updateVideo(Integer id, String video_title, String video_sport,
                               String video_length, String video_creator, String video_category,
                               Boolean video_annotated, Boolean video_favorited,
                               String video_timestamp) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("video_title", video_title);
        contentValues.put("video_sport", video_sport);
        contentValues.put("video_length", video_length);
        contentValues.put("video_title", video_creator);
        contentValues.put("video_title", video_category);
        contentValues.put("video_annotated", video_annotated);
        contentValues.put("video_favorited", video_favorited);
        contentValues.put("video_timestamp", video_timestamp);

        db.update(VideoEntry.TABLE_NAME, contentValues, "id = ? ", new String[]{Integer.toString(id)});
        return true;
    }


    public Integer deleteVideo(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(VideoEntry.TABLE_NAME, "id = ? ", new String[]{Integer.toString(id)});
    }


    //Would be used for the search function
//    public ArrayList<String> getAllVideoTitles() {
//        ArrayList<String> all_videos_array_list = new ArrayList<String>();
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor res = db.rawQuery("select * from videoList", null);
//        res.moveToFirst();
//        while (res.isAfterLast() == false) {
//            all_videos_array_list.add(res.getString(res.getColumnIndex(VideoEntry.COLUMN_VIDEO_TITLE)));
//            res.moveToNext();
//        }
//        return all_videos_array_list;
//    }

    private ArrayList<Uri> getAllVideoUris() {
        ArrayList<Uri> all_uris_arraylist = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from videoList", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            all_uris_arraylist.add(Uri.parse(res.getString(res.getColumnIndex(VideoEntry.COLUMN_VIDEO_FILEPATH))));
            res.moveToNext();
        }
        return all_uris_arraylist;
    }


    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, VideoEntry.TABLE_NAME);
        return numRows;
    }


    // for loading all videos, change selection, and String array to null, right now just loading sporsight/recordings
    public void loadVideosFromStorage(Context context) {
        Log.d(TAG, "Starting LoadVideosFromStorage()");

        Uri queryUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        //Uri queryUri = MediaStore.Video.Media.INTERNAL_CONTENT_URI;


//        Cursor storageCursor = context.getContentResolver().query(
//                queryUri,
//                LOAD_VIDEOS_PROJECTION,
//                MediaStore.Audio.Media.DATA + " like ? ",
//                new String[]{"%Downlaods%"},
//                MediaStore.Files.FileColumns.DATE_ADDED + " DESC");


        Cursor storageCursor = context.getContentResolver().query(
                queryUri,
                LOAD_VIDEOS_PROJECTION,
                null,
                null,
                MediaStore.Files.FileColumns.DATE_ADDED + " DESC");

        int count = storageCursor.getCount();

        for (int i = 0; i < count; i++) {


            // Setting the video FilePath
            int col_index_filePath = storageCursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            storageCursor.moveToPosition(i);
            String video_filePath = storageCursor.getString(col_index_filePath);
            Log.d(TAG, "video_filepath: " + video_filePath);

            // TODO: Remove check here, this is just for my sanity while developing.
//            if (!video_filePath.matches(".*(?i)sporsight.*")) {
//                Log.v(TAG,"Skipping video: " + video_filePath);
//                continue;
//            }

            // Performing a check to make sure we don't load the same video twice.
            if (containsUri(Uri.parse(video_filePath))) {
//                Log.v(TAG,"Duplicate file found at filepath: " + video_filePath);
                continue;
            }


            // Setting the video DisplayName
            int col_index_display_name = storageCursor.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME);
            storageCursor.moveToPosition(i);
            String video_display_name = storageCursor.getString(col_index_display_name);
            Log.d(TAG, "video_displayname: " + video_display_name);

            // Setting the video Author
            int col_index_artist = storageCursor.getColumnIndexOrThrow(MediaStore.Video.Media.ARTIST);
            storageCursor.moveToPosition(i);
            String video_artist = storageCursor.getString(col_index_artist);
            Log.d(TAG, "video_artist: " + video_artist);

            // Setting the video length
            int col_index_videoLength = storageCursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION);
            storageCursor.moveToPosition(i);
            Long duration = storageCursor.getLong(col_index_videoLength);
            String video_length = convertDuration(duration);
            Log.d(TAG, "video_length: " + video_length);

//            // Setting the video date added
//            int col_index_date_added = storageCursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED);
//            storageCursor.moveToPosition(i);
//            String video_date_added = storageCursor.getString(col_index_date_added);
//            Log.d(TAG, "Date Added: " + video_date_added);

            // Setting the video date modified
            int col_index_date_modified = storageCursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_MODIFIED);
            storageCursor.moveToPosition(i);
            String video_date_string = storageCursor.getString(col_index_date_modified);
            Log.d(TAG, "Date Modified: " + video_date_string);
            long video_date_long = storageCursor.getLong(col_index_date_modified);
            Date date = new Date();
            date.setTime(video_date_long * 1000);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy' 'HH:mm", Locale.US);
            String date_converted = simpleDateFormat.format(date);
            Log.d(TAG, "Converted Date: " + date_converted);


            // Default for now
            String video_sport = "Golf";
            String video_category = "Practice";
            Boolean video_annotated = false;
            Boolean video_favorited = false;


            // Setting the video's favorited identifier
//            Boolean video_favorited = storageCursor.getColumnIndexOrThrow(MediaStore.Video.Media.BOOKMARK);


            // Setting the video's annotated identifier


            //Checking to make sure newly added videos have a unique filepath
//            if(!containsFilePath(context, Uri.parse(video_filePath))){
//
//            }
            if (!containsUri(Uri.parse(video_filePath))) {
                Log.v(TAG, "New file found at filepath: " + video_filePath);
                insertVideo(video_display_name, video_sport, video_length, video_artist, video_category, video_annotated, video_favorited, date_converted, video_filePath);
            }


        }
        storageCursor.close();
        Log.v(TAG, "Finished LoadingVideosFromStorage()");
    }


    private String convertDuration(Long vd) {
        String video_length = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(vd),
                TimeUnit.MILLISECONDS.toMinutes(vd) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(vd)),
                TimeUnit.MILLISECONDS.toSeconds(vd) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(vd)));
        return video_length;
    }


    public void toggleFavoritedVideo(int position) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor favoritedVideoCursor = getAllVideos();
        Log.d(TAG, "TOGGLING FAVORITED VIDEO");

        favoritedVideoCursor.moveToPosition(position);
        int col_index_favorited = favoritedVideoCursor.getColumnIndexOrThrow(VideoEntry.COLUMN_VIDEO_FAVORITED);
        Boolean favorited_status = Boolean.valueOf(favoritedVideoCursor.getString(col_index_favorited));

        Log.d(TAG, "Position: " + position);
        Log.d(TAG, "Was Favorited: " + favorited_status);

        favorited_status = !favorited_status;
        Log.d(TAG, "Now Favorited:  " + favorited_status);


        ContentValues contentValues = new ContentValues();
        contentValues.put("video_favorited", favorited_status);

        favoritedVideoCursor.moveToPosition(position);
        int col_index_title = favoritedVideoCursor.getColumnIndexOrThrow(VideoEntry.COLUMN_VIDEO_TITLE);
        String video_title = favoritedVideoCursor.getString(col_index_title);

        favoritedVideoCursor.moveToPosition(position);
        int col_index_id = favoritedVideoCursor.getColumnIndexOrThrow(VideoEntry._ID);
        String video_id = favoritedVideoCursor.getString(col_index_id);

        Log.v(TAG, "Column Video Title: " + video_title);
        Log.v(TAG, "Column Video ID: " + video_id);

        db.update(VideoEntry.TABLE_NAME, contentValues, "_id=" + video_id, null);
        favoritedVideoCursor.close();

    }


    public Uri getVideoUri(int position) {
        Cursor startVideoCursor = getAllVideos();
        startVideoCursor.moveToPosition(position);
        int col_index_filepath = startVideoCursor.getColumnIndexOrThrow(VideoEntry.COLUMN_VIDEO_FILEPATH);
        Uri mVideoUri = Uri.parse(startVideoCursor.getString(col_index_filepath));
        startVideoCursor.close();
        return mVideoUri;
    }

    public Uri getVideoContentUri(int position) {
        Cursor contentCursor = getAllVideos();
        contentCursor.moveToPosition(position);
        int col_index_filepath = contentCursor.getColumnIndexOrThrow(VideoEntry.COLUMN_VIDEO_FILEPATH);
        String dataString = contentCursor.getString(col_index_filepath);
        String authorities = mContext.getPackageName() + ".FileProvider";
        Log.v(TAG, "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$__AUTHORITIES:" + authorities);
        contentCursor.close();
        return FileProvider.getUriForFile(mContext, authorities, new File(dataString));
    }


    public String getVideoTitle(int position) {
        Cursor titleVideoCursor = getAllVideos();
        titleVideoCursor.moveToPosition(position);
        int col_index_filepath = titleVideoCursor.getColumnIndexOrThrow(VideoEntry.COLUMN_VIDEO_TITLE);
        String videoTitle = titleVideoCursor.getString(col_index_filepath);
        titleVideoCursor.close();
        return videoTitle;

    }


    public int getVideoID(int position) {
        Cursor videoIDCursor = getAllVideos();

        videoIDCursor.moveToPosition(position);
        int col_index_ID = videoIDCursor.getColumnIndexOrThrow("_id");
        int _id = videoIDCursor.getInt(col_index_ID);
        videoIDCursor.close();
        return _id;
    }


    public boolean containsFilePath(Context context, Uri uri) {

        Uri queryUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        Cursor filePathCheckCursor = context.getContentResolver().query(
                queryUri,
                LOAD_VIDEOS_PROJECTION,
                null,
                null,
                MediaStore.Files.FileColumns.DATE_ADDED + " DESC");

        int count = filePathCheckCursor.getCount();
        int col_index_filePath = filePathCheckCursor.getColumnIndexOrThrow("_id");

        for (int i = 0; i < count; i++) {

            filePathCheckCursor.moveToPosition(i);

            String cursor_video_filePath = filePathCheckCursor.getString(col_index_filePath);
            String lookup_video_filePath = uri.toString();

            if (cursor_video_filePath.equals(lookup_video_filePath)) {
                return true;
            }

        }
        filePathCheckCursor.close();
        return false;

    }

    private boolean containsUri(Uri uri) {
        ArrayList<Uri> uriArrayList = getAllVideoUris();
        return uriArrayList.contains(uri);
    }


}
