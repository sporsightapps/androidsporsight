package com.dev.sporsightmobile.entities.video;

import android.provider.BaseColumns;

/*
    VideoContract Class is used to structure the SQLite Database for the Videos loaded from storage
 */
public class VideoContract {

    private VideoContract() {
    }

    public static final class VideoEntry implements BaseColumns {

        public static final String DATABASE_NAME = "videoList.db";
        public static final int DATABASE_VERSION = 18;

        public static final String TABLE_NAME = "videoList";
        public static final String COLUMN_VIDEO_TITLE = "video_title";
        public static final String COLUMN_VIDEO_SPORT = "video_sport";
        public static final String COLUMN_VIDEO_LENGTH = "video_length";
        public static final String COLUMN_VIDEO_CREATOR = "video_creator";
        public static final String COLUMN_VIDEO_CATEGORY = "video_category";
        public static final String COLUMN_VIDEO_FILEPATH = "video_filepath";
        public static final String COLUMN_VIDEO_ANNOTATED = "video_annotated";
        public static final String COLUMN_VIDEO_FAVORITED = "video_favorited";
        public static final String COLUMN_TIMESTAMP = "video_timestamp";
    }
}
