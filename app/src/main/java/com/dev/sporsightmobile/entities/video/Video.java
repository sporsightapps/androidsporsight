package com.dev.sporsightmobile.entities.video;

import android.net.Uri;
import androidx.annotation.NonNull;

public class Video {


    //TODO: NEED TO ADD VIDEO_URI TO THE ENTITY AND REMOVE FILEPATH
    private String id;
    private String title;
    private String sport;
    private String length;
    private String creator;
    private String category;
    private Boolean annotated;
    private Boolean favorited;
    private String str_path;
    private String str_thum;
    private String filePath;
    private String fileType;
    private String dateAdded;
    private String dateModified;


    public Video() {
    }


    public Video(String id, String title, String sport, String length,
                 String creator, String category, Boolean annotated, Boolean favorited,
                 String str_thum, String filePath, String fileType) {

        this.title = title;
        this.sport = sport;
        this.length = length;
        this.creator = creator;
        this.category = category;
        this.annotated = annotated;
        this.favorited = favorited;
        this.str_thum = str_thum;
        this.filePath = filePath;
        this.fileType = fileType;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }


    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }


    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public Boolean getAnnotated() {
        return annotated;
    }

    public void setAnnotated(Boolean annotated) {
        this.annotated = annotated;
    }


    public Boolean getFavorited() {
        return favorited;
    }

    public void setFavorited(Boolean favorited) {
        this.favorited = favorited;
    }


    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }


    public String getStr_thum() {
        return str_thum;
    }

    public void setStr_thum(String str_thum) {
        this.str_thum = str_thum;
    }


    public String getStr_path() {
        return str_path;
    }

    public void setStr_path(String str_path) {
        this.str_path = str_path;
    }


    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }


    @NonNull
    public Uri getUri() {
        Uri uri = Uri.parse(filePath);
        if (!uri.toString().startsWith("file://") && !uri.toString().startsWith("content://"))
            uri = Uri.parse(String.format("file://%s", uri.toString()));
        return uri;
    }


}
