//package com.dev.sporsightmobile.utilities;
//
//import android.content.ContentProvider;
//import android.content.ContentUris;
//import android.content.ContentValues;
//import android.content.Context;
//import android.content.UriMatcher;
//import android.database.Cursor;
//import android.database.SQLException;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//import android.database.sqlite.SQLiteQueryBuilder;
//import android.net.Uri;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.text.TextUtils;
//
//import java.util.HashMap;
//
//public class VideosProvider extends ContentProvider {
//
//    static final String PROVIDER_NAME = "com.dev.sporsightmobile.VideosProvider";
//    static final String URL = "content://" + PROVIDER_NAME + "/videos";
//    static final Uri CONTENT_URI = Uri.parse(URL);
//
//    static final String _ID = "_id";
//    static final String NAME = "name";
//    static final String GRADE = "grade";
//
//
//
//    private static HashMap<String, String> VIDEOS_PROJECTION_MAP;
//
//    static final int VIDEOS = 1;
//    static final int VIDEOS_ID = 2;
//
//
//    static final UriMatcher uriMatcher;
//    static{
//        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
//        uriMatcher.addURI(PROVIDER_NAME, "videos", VIDEOS);
//        uriMatcher.addURI(PROVIDER_NAME, "videos/#", VIDEOS_ID);
//    }
//
//
//
//    private SQLiteDatabase db;
//    static final String DATABASE_NAME =  "Videos";
//    static final String RECORDEDVIDEOS_TABLE_NAME = "recordedVideos";
//    static final int DATABASE_VERSION = 1;
//    static final String CREATE_DB_TABLE =
//            " CREATE TABLE " + RECORDEDVIDEOS_TABLE_NAME +
//                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
//                    " name TEXT NOT NULL, " +
//                    " grade TEXT NOT NULL);";
//
//
//    private static class DatabaseHelper extends SQLiteOpenHelper{
//
//        DatabaseHelper(Context context){
//            super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        }
//
//
//        @Override
//        public void onCreate(SQLiteDatabase db) {
//            db.execSQL(CREATE_DB_TABLE);
//        }
//
//        @Override
//        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//            db.execSQL("DROP TABLE IF EXISTS " + RECORDEDVIDEOS_TABLE_NAME);
//            onCreate(db);
//        }
//    }
//
//
//
//
//
//    @Override
//    public boolean onCreate() {
//        Context context = getContext();
//        DatabaseHelper dbHelper = new DatabaseHelper(context);
//        db = dbHelper.getWritableDatabase();
//        return db != null;
//    }
//
//
//
//
//    @Nullable
//    @Override
//    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
//
//        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
//        queryBuilder.setTables(RECORDEDVIDEOS_TABLE_NAME);
//
//        switch(uriMatcher.match(uri)){
//            case VIDEOS:
//                queryBuilder.setProjectionMap(VIDEOS_PROJECTION_MAP);
//            break;
//
//            case VIDEOS_ID:
//                queryBuilder.appendWhere( _ID + "=" + uri.getPathSegments().get(1));
//            break;
//
//            default:
//        }
//
//        if(sortOrder == null || sortOrder == "") {
////            BY DEFAULT WE SORT BY NAME
//            sortOrder = NAME;
//        }
//            Cursor cursor = queryBuilder.query(
//                    db, projection, selection, selectionArgs, null, null, sortOrder);
//
//            cursor.setNotificationUri(getContext().getContentResolver(), uri);
//            return cursor;
//
//    }
//
//
//
//
//    @Nullable
//    @Override
//    public String getType(@NonNull Uri uri) {
//        switch (uriMatcher.match(uri)){
//
//            case VIDEOS:
//                return "vnd.android.cursor.dir/vnd.dev.students";
//
//            case VIDEOS_ID:
//                return "vnd.android.cursor.item/vnd.dev.students";
//            default:
//                throw new IllegalArgumentException("Unsupported URI: " + uri);
//        }
//    }
//
//
//
//
//    @Override
//    public Uri insert(@NonNull Uri uri, ContentValues values) {
//        long rowID = db.insert(	RECORDEDVIDEOS_TABLE_NAME, "", values);
//        if(rowID > 0){
//            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
//            getContext().getContentResolver().notifyChange(_uri, null);
//            return _uri;
//        }
//        throw new SQLException("Failed to add a record into " + uri);
//
//    }
//
//
//
//
//    @Override
//    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
//        int count = 0;
//
//        switch (uriMatcher.match(uri)) {
//
//            case VIDEOS:
//                count = db.delete(RECORDEDVIDEOS_TABLE_NAME, selection, selectionArgs);
//                break;
//
//            case VIDEOS_ID:
//                String id = uri.getPathSegments().get(1);
//                count = db.delete(RECORDEDVIDEOS_TABLE_NAME, _ID + " = " + id +
//                        (!TextUtils.isEmpty(selection) ?
//                                " AND (" + selection + ')' : ""), selectionArgs);
//                break;
//
//                default:
//                throw new IllegalArgumentException("Unknown URI " + uri);
//        }
//        getContext().getContentResolver().notifyChange(uri, null);
//        return count;
//    }
//
//
//
//    @Override
//    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
//        int count = 0;
//        switch (uriMatcher.match(uri)) {
//            case VIDEOS:
//                count = db.update(RECORDEDVIDEOS_TABLE_NAME, values, selection, selectionArgs);
//                break;
//
//            case VIDEOS_ID:
//                count = db.update(RECORDEDVIDEOS_TABLE_NAME, values,
//                        _ID + " = " + uri.getPathSegments().get(1) +
//                                (!TextUtils.isEmpty(selection) ? " " +
//                                        "AND (" +selection + ')' : ""), selectionArgs);
//                break;
//            default:
//                throw new IllegalArgumentException("Unknown URI " + uri );
//        }
//
//        getContext().getContentResolver().notifyChange(uri, null);
//        return count;
//    }
//
//
//
//
//
//}
