package com.dev.sporsightmobile.Retrofit;

import com.dev.sporsightmobile.interfaces.UploadFileAPI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitControllerUploadFile {

    // The URL we are sending our GET and POST requests to
    static final public String BASE_URL = "https://ata-vm.eastus.cloudapp.azure.com";
    //static final public String BASE_URL = "http://10.0.2.2:8080";

    // We initialize all of our variables to allow us to use Retrofit properly
    public OkHttpClient.Builder okHttpBuilder;
    HttpLoggingInterceptor logging;
    Gson gson;
    Retrofit retrofit;

    // We also initialize an instance of our UploadFileAPI interface
    UploadFileAPI uploadFileAPI;

    public RetrofitControllerUploadFile(){

        // Our setters to initialize all of our properties
        setHttpBuilder();
        setHttpLoggingInterceptor();
        setGson();
        setRetrofit();
        setUploadFileAPI();
    }

    // We set http builder in order for it to allow us to make http requests
    public void setHttpBuilder() {
        this.okHttpBuilder = new OkHttpClient.Builder();
    }

    // In order to see information in our logcat regarding the GET and POST requests that have been sent
    public void setHttpLoggingInterceptor() {
        this.logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
    }


    // GSON allows us to have make Java objects from JSON
    public void setGson() {
        gson = new GsonBuilder()
                .setLenient()
                .create();
    }

    // Where we formally set up an instance of Retrofit
    public void setRetrofit() {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(this.gson))
                .client(this.okHttpBuilder.build())
                .build();
    }

    // An instance of our UploadFileAPI is created
    public void setUploadFileAPI() {
        this.uploadFileAPI = this.retrofit.create(UploadFileAPI.class);
    }

    // Our getters for our respective properties
    public OkHttpClient.Builder getOkHttpBuilder() {
        return this.okHttpBuilder;
    }

    public Gson getGson() {
        return this.gson;
    }

    public Retrofit getRetrofit() {
        return this.retrofit;
    }

    // In order to access the methods present in the UploadFileAPI we must first get an instance of it
    public UploadFileAPI getUploadFileAPI() {
        return this.uploadFileAPI;
    }
}
