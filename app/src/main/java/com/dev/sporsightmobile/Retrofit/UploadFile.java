package com.dev.sporsightmobile.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

// A JSON response containing a message as to whether the sent video has finished processing can be determined by setting a message field
// A JSON response containing the name of the file that is sent for processing can be accessed through this implementation
public class UploadFile {

    @SerializedName("filename")
    @Expose
    private String filename;

    @SerializedName("message")
    @Expose
    private String message;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}