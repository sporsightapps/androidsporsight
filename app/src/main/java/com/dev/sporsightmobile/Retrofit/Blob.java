package com.dev.sporsightmobile.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

// Each blob has certain information associated with it,
// the information returned through the blob API is the name of the blob as well as the date
// This class is a Java class associated with the JSON response which will contain the aforementioned information
// It is recommended to look into Gson to see the relationship between JSON responses and the Java classes that can be generated by them
public class Blob {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("date")
    @Expose
    private String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
