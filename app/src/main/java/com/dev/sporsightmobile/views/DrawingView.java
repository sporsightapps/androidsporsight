package com.dev.sporsightmobile.views;

import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.util.Pair;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.activities.BaseAnnotatorActivity;
import com.dev.sporsightmobile.fragments.AnnotationFragment;
import com.dev.sporsightmobile.fragments.ExoPlayerFragment;
import com.dev.sporsightmobile.model.FrameAnalysisResult;
import com.dev.sporsightmobile.model.PrescriptiveFeedbackData;
import com.dev.sporsightmobile.services.DrawingViewScalingService;
import com.dev.sporsightmobile.services.FeedbackVisualization;
import com.dev.sporsightmobile.services.PrescriptiveFeedbackService;
import com.dev.sporsightmobile.services.SevenKeyFrameService;
import com.dev.sporsightmobile.services.VideoAnalysisService;
import com.dev.sporsightmobile.utilities.FrameUtilities;
import com.dev.sporsightmobile.utilities.PoseEstimationUtilities;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.lite.examples.posenet.lib.BodyPart;
import org.tensorflow.lite.examples.posenet.lib.KeyPoint;
import org.tensorflow.lite.examples.posenet.lib.Person;
import org.tensorflow.lite.examples.posenet.lib.Position;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.dev.sporsightmobile.utilities.PoseUtility.findKeypointByBodyPart;


public class DrawingView extends View {
	private static final String TAG = DrawingView.class.getSimpleName();

	private PoseEstimationUtilities poseEstimationUtilities = new PoseEstimationUtilities();

	//Booleans to track the type of drawing mode the user currently using
	private Boolean mDrawingFreeHandMode;
	private Boolean mDrawingCircleMode;
	private Boolean mDrawingRectangleMode;
	private Boolean mDrawingArrowMode;
	//
	private Boolean mDrawingStraightLineMode;
	// private Boolean mDrawingPoseMode;

	//Booleans to determine the type of drawing action the user currently using
	private Boolean mIsDrawingFreeHand;
	private Boolean mIsDrawingRectangle;
	private Boolean mIsDrawingCircle;
	private Boolean mIsDrawingArrow;

	public ImageButton trajectoryButton;

	//
	private Boolean mIsDrawingStraightLine;

	private Path mDrawPath;
	private Path mArrowTipPath;

	private Paint mBackgroundPaint;
	private Paint mDrawPaint;
	private Paint mShapePaint;
	private Canvas mDrawCanvas;


	public ArrayList<String> selectedJoints = new ArrayList<>();
	private ArrayList<Path> mPaths = new ArrayList<>();
	private ArrayList<Paint> mPaints = new ArrayList<>();
	private ArrayList<Path> mUndonePaths = new ArrayList<>();
	private ArrayList<Paint> mUndonePaints = new ArrayList<>();
	private ArrayList<Person> personList = new ArrayList<>(Arrays.asList(
			new Person(), new Person()
	));


	private PointF beginingCoordinate;
	private PointF endingCoordinate;

	private List<BodyPart> keyPointsToDraw;
	private List<BodyPart> bodyKeyPoints = Arrays.asList(
			BodyPart.LEFT_SHOULDER,
			BodyPart.RIGHT_SHOULDER,
			BodyPart.LEFT_ELBOW,
			BodyPart.RIGHT_ELBOW,
			BodyPart.LEFT_WRIST,
			BodyPart.RIGHT_WRIST,
			BodyPart.LEFT_HIP,
			BodyPart.RIGHT_HIP,
			BodyPart.LEFT_KNEE,
			BodyPart.RIGHT_KNEE,
			BodyPart.LEFT_ANKLE,
			BodyPart.RIGHT_ANKLE
	);

	private List<BodyPart> leftKeyPoints = Arrays.asList(
			BodyPart.LEFT_SHOULDER,
			BodyPart.LEFT_ELBOW,
			BodyPart.LEFT_WRIST,
			BodyPart.LEFT_HIP,
			BodyPart.LEFT_KNEE,
			BodyPart.LEFT_ANKLE
	);
	private List<BodyPart> rightKeyPoints = Arrays.asList(
			BodyPart.RIGHT_SHOULDER,
			BodyPart.RIGHT_ELBOW,
			BodyPart.RIGHT_WRIST,
			BodyPart.RIGHT_HIP,
			BodyPart.RIGHT_KNEE,
			BodyPart.RIGHT_ANKLE
	);

	private List<Pair> pairToDraw;
	private List<Pair> allPairs = Arrays.asList(
			new Pair(BodyPart.LEFT_WRIST, BodyPart.LEFT_ELBOW),
			new Pair(BodyPart.LEFT_ELBOW, BodyPart.LEFT_SHOULDER),
			new Pair(BodyPart.LEFT_SHOULDER, BodyPart.RIGHT_SHOULDER),
			new Pair(BodyPart.RIGHT_SHOULDER, BodyPart.RIGHT_ELBOW),
			new Pair(BodyPart.RIGHT_ELBOW, BodyPart.RIGHT_WRIST),
			new Pair(BodyPart.LEFT_SHOULDER, BodyPart.LEFT_HIP),
			new Pair(BodyPart.LEFT_HIP, BodyPart.RIGHT_HIP),
			new Pair(BodyPart.RIGHT_HIP, BodyPart.RIGHT_SHOULDER),
			new Pair(BodyPart.LEFT_HIP, BodyPart.LEFT_KNEE),
			new Pair(BodyPart.LEFT_KNEE, BodyPart.LEFT_ANKLE),
			new Pair(BodyPart.RIGHT_HIP, BodyPart.RIGHT_KNEE),
			new Pair(BodyPart.RIGHT_KNEE, BodyPart.RIGHT_ANKLE)
	);
	private List<Pair> leftPairs = Arrays.asList(
			new Pair(BodyPart.LEFT_WRIST, BodyPart.LEFT_ELBOW),
			new Pair(BodyPart.LEFT_ELBOW, BodyPart.LEFT_SHOULDER),
			new Pair(BodyPart.LEFT_SHOULDER, BodyPart.LEFT_HIP),
			new Pair(BodyPart.LEFT_HIP, BodyPart.LEFT_KNEE),
			new Pair(BodyPart.LEFT_KNEE, BodyPart.LEFT_ANKLE)
	);
	private List<Pair> rightPairs = Arrays.asList(
			new Pair(BodyPart.RIGHT_SHOULDER, BodyPart.RIGHT_ELBOW),
			new Pair(BodyPart.RIGHT_ELBOW, BodyPart.RIGHT_WRIST),
			new Pair(BodyPart.RIGHT_HIP, BodyPart.RIGHT_SHOULDER),
			new Pair(BodyPart.RIGHT_HIP, BodyPart.RIGHT_KNEE),
			new Pair(BodyPart.RIGHT_KNEE, BodyPart.RIGHT_ANKLE)
	);

	private boolean displayPoseLoadingToast = true;
	private boolean displayPoseFinishedToast = true;

	// Set default values
	private static final int BACKGROUND = Color.parseColor("#00000000");
	private int mBackgroundColor = BACKGROUND;
	private int mPaintColor = 0xFF660000;
	private int mStrokeWidth = 10;

	public FrameUtilities frameUtilities;

	public List<FrameAnalysisResult> analysisResult = null;

	private Paint feedbackVisualizationPaint;

	public DrawingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Log.d(TAG, "Drawing View loaded");
		mIsDrawingPose = false;
		VideoAnalysisService.analysisResult.thenAccept(analysisResult -> {
			this.analysisResult = analysisResult;
			this.invalidate();
		});


		frameUtilities = new FrameUtilities(context);
		trajectoryButton = findViewById(R.id.viewTrajectory);
		init();
		initDrawingType();
		KeyPoint keyPointA = new KeyPoint();
		Position positionA = new Position();
		positionA.setX(500);
		positionA.setY(500);
		keyPointA.setBodyPart(BodyPart.LEFT_WRIST);
		keyPointA.setPosition(positionA);
		KeyPoint keyPointB = new KeyPoint();
		Position positionB = new Position();
		positionB.setX(400);
		positionB.setY(400);
		keyPointB.setBodyPart(BodyPart.LEFT_WRIST);
		keyPointB.setPosition(positionB);
		personList.get(0).setKeyPoints(new ArrayList<>(Arrays.asList(
				keyPointA
		)));
		personList.get(1).setKeyPoints(new ArrayList<>(Arrays.asList(
				keyPointB
		)));

		Paint feedbackVisualizationPaint = new Paint();
		feedbackVisualizationPaint.setStyle(Paint.Style.STROKE);
		feedbackVisualizationPaint.setStrokeWidth(6f);
		feedbackVisualizationPaint.setColor(-0x333301);
		this.feedbackVisualizationPaint = feedbackVisualizationPaint;
	}

	private void init() {
		mDrawPath = new Path();
		mArrowTipPath = new Path();
		mBackgroundPaint = new Paint();
		beginingCoordinate = new PointF();
		endingCoordinate = new PointF();
		initPaint();
	}


	private void initPaint() {
		mDrawPaint = new Paint();
		mDrawPaint.setColor(mPaintColor);
		mDrawPaint.setAntiAlias(true);
		mDrawPaint.setStrokeWidth(mStrokeWidth);
		mDrawPaint.setStyle(Paint.Style.STROKE);
		mDrawPaint.setStrokeJoin(Paint.Join.ROUND);
		mDrawPaint.setStrokeCap(Paint.Cap.ROUND);
		mShapePaint = mDrawPaint;
		mShapePaint.setStyle(Paint.Style.STROKE);
	}


	private void initDrawingType() {
		mIsDrawingFreeHand = false;
		mIsDrawingCircle = false;
		mIsDrawingRectangle = false;
		mIsDrawingArrow = false;
		mIsDrawingStraightLine = false;
		BaseAnnotatorActivity.trajectorySelected = false;
		mDrawingFreeHandMode = false;
		mDrawingCircleMode = false;
		mDrawingRectangleMode = false;
		mDrawingArrowMode = false;
		mDrawingStraightLineMode = false;
	}


	//TODO: THIS CAN POSSIBLY BE REMOVED.
	private void drawBackground(Canvas canvas) {
		mBackgroundPaint.setColor(mBackgroundColor);
		mBackgroundPaint.setStyle(Paint.Style.FILL);
		canvas.drawRect(0, 0, this.getWidth(), this.getHeight(), mBackgroundPaint);
	}


	private void drawPaths(Canvas canvas) {
		int i = 0;
		for (Path p : mPaths) {
			canvas.drawPath(p, mPaints.get(i));
			i++;
		}
	}

	// Given the json string, an arraylist of person will be created.
	private ArrayList<Person> parseJSON(String json) {

		ArrayList<Person> person_list = (ArrayList<Person>) (new Gson().fromJson(json, new TypeToken<ArrayList<Person>>() {
		}.getType()));

		return person_list;
	}

	// Will load a json file from the assets folder into a string.
	public String loadJSONFromAsset(Context context) {
		String json = null;
		try {
			InputStream is = context.getAssets().open("front_data1.json");

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");


		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}


	private void drawTrajectory(Canvas canvas) {

		try {
			Paint past1 = new Paint();
			past1.setColor(Color.parseColor("#ff9b00"));
			past1.setStrokeWidth(mStrokeWidth);
			Paint future1 = new Paint();
			future1.setColor(Color.parseColor("#00afff"));
			future1.setStrokeWidth(mStrokeWidth);
			Paint past2 = new Paint();
			past2.setColor(Color.parseColor("#308014"));
			past2.setStrokeWidth(mStrokeWidth);
			Paint future2 = new Paint();
			future2.setColor(Color.parseColor("#d41a1f"));
			future2.setStrokeWidth(mStrokeWidth);
			Paint seekerColor = new Paint();
			seekerColor.setColor(Color.parseColor("#9400D3"));
			seekerColor.setStrokeWidth(mStrokeWidth);

			for (int selectedJointIndex = 0; selectedJointIndex < selectedJoints.size(); selectedJointIndex++) {
				String selectedJoint = selectedJoints.get(selectedJointIndex);

				List<Position> selectedJointPositions = this.analysisResult
						.stream()
						.flatMap(frameAnalysisResult -> frameAnalysisResult.getScaledPoseEstimation().getKeyPoints().stream())
						.filter(keyPoint -> keyPoint.getBodyPart() == BodyPart.valueOf(selectedJoint))
						.map(keyPoint -> keyPoint.getPosition())
						.collect(Collectors.toList());

				Paint pastPaint = selectedJointIndex == 0 ? past1 : past2;
				Paint futurePaint = selectedJointIndex == 0 ? future1 : future2;
				Boolean swappedColors = false;

				for (int i = 0; i < selectedJointPositions.size(); i++) {

					Paint selectedPaint = ExoPlayerFragment.exoPlayer.getCurrentPosition() < this.analysisResult.get(i).getTime() ? futurePaint : pastPaint;

					if (i != 0) {
						canvas.drawLine(
								selectedJointPositions.get(i - 1).getX(),
								selectedJointPositions.get(i - 1).getY(),
								selectedJointPositions.get(i).getX(),
								selectedJointPositions.get(i).getY(),
								selectedPaint
						);
					}
					if (!swappedColors && selectedPaint != pastPaint) {
						swappedColors = true;
						canvas.drawCircle(
								selectedJointPositions.get(i - 1).getX(),
								selectedJointPositions.get(i - 1).getY(),
								16,
								seekerColor
						);
					}
					// Used to draw circles between lines.
//                    canvas.drawCircle(
//                            selectedJointPositions.get(i).getX(),
//                            selectedJointPositions.get(i).getY(),
//                            3,
//                            selectedPaint
//                    );
				}
			}
		} catch (Exception e) {
			Log.d(TAG, "We failed to draw the Trajectory I guess");
		}
	}

	// private Timer timer;
	private Handler trajectoryHandler = new Handler();
	private Runnable trajectoryRunnable;

	void initTrajectoryTimer(Canvas canvas) {
		int delay = 200;
		View drawingView = this;
		trajectoryRunnable = new Runnable() {
			@Override
			public void run() {
				drawingView.invalidate();
				if (BaseAnnotatorActivity.trajectorySelected) {
					trajectoryHandler.postDelayed(this, delay);
				} else {
					setDrawingType("Clear");
				}
			}
		};

		trajectoryHandler.postDelayed(trajectoryRunnable, delay);
	}

	@RequiresApi(api = Build.VERSION_CODES.P)
	@Override
	protected void onDraw(Canvas canvas) {
		Log.v(TAG, " CALLING ONDRAW()");
		drawBackground(canvas);

		if (displayPoseLoadingToast) {
			displayPoseLoadingToast = false;
			Toast.makeText(getContext(), "Some features are still loading, please wait on this screen.", Toast.LENGTH_LONG).show();
		}

		if (displayPoseFinishedToast && analysisResult != null) {
			displayPoseFinishedToast = false;
			Toast.makeText(getContext(), "Extended features are now ready!", Toast.LENGTH_LONG).show();
		}

		if (!BaseAnnotatorActivity.trajectorySelected) {
			drawPaths(canvas);
			canvas.drawPath(mDrawPath, mDrawPaint);
		}

		if (BaseAnnotatorActivity.trajectorySelected) {
			drawTrajectory(canvas);
			initTrajectoryTimer(canvas);
		}
		// If we tapped on the pose button on the far left it sets mIsDrawingPose to true
		else if (mIsDrawingPose) {
				// The properties of the lines themselves are determined here
				Paint paint = new Paint();
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(6f);
		paint.setColor(Color.GRAY);

		// The properties of the key-points themselves are determined here
		Paint paintKeyPoints = new Paint();
		paintKeyPoints.setAntiAlias(true);
		paintKeyPoints.setColor(Color.RED);
		paintKeyPoints.setStyle(Paint.Style.FILL_AND_STROKE);
		paintKeyPoints.setStrokeWidth(6f);

		switch (PoseEstimationUtilities.bodyPosition) {
			case SIDE_LEFT:
				pairToDraw = leftPairs;
				keyPointsToDraw = leftKeyPoints;
				break;

			case SIDE_RIGHT:
				pairToDraw = rightPairs;
				keyPointsToDraw = rightKeyPoints;
				break;

			case FRONT_LEFT:
			case FRONT_RIGHT:
			default:
				pairToDraw = allPairs;
				keyPointsToDraw = bodyKeyPoints;
				break;
		}

		List<KeyPoint> keypoints = frameAnalysisResult.getScaledPoseEstimation().getKeyPoints();

		for (BodyPart bp : keyPointsToDraw) {
			KeyPoint kp = findKeypointByBodyPart(keypoints, bp);
			Position coords = kp.getPosition();
			float x = coords.getX();
			float y = coords.getY();
			canvas.drawCircle(x, y, 3, paintKeyPoints);
		}

		for (Pair p : pairToDraw) {
			KeyPoint kp1 = findKeypointByBodyPart(keypoints, (BodyPart) p.first);
			KeyPoint kp2 = findKeypointByBodyPart(keypoints, (BodyPart) p.second);
			Position coords1 = kp1.getPosition();
			Position coords2 = kp2.getPosition();
			float x1 = coords1.getX();
			float y1 = coords1.getY();
			float x2 = coords2.getX();
			float y2 = coords2.getY();
			canvas.drawLine(x1, y1, x2, y2, paint);
		}

//			new PostProcessing(canvas, PoseEstimationUtilities.bodyPosition, keypoints, frameAnalysisResult.getScaledImageSegmentation().getBitmapMaskOnly(), swingPlaneTerminalEndpoint).runPostProcessing();

		feedbackVisualizationPaths.forEach(path -> canvas.drawPath(path, feedbackVisualizationPaint));

		VideoAnalysisService.analysisResult.thenAccept(analysisResult -> {

			PrescriptiveFeedbackService.INSTANCE.prescriptiveFeedbackPromise = CompletableFuture.supplyAsync(() -> {
				PrescriptiveFeedbackData prescriptiveFeedbackData = new PrescriptiveFeedbackData(
						frameAnalysisResult,
						analysisResult,
						PoseEstimationUtilities.bodyPosition,
						swingPlaneTerminalEndpoint
				);

				List<String> prescriptiveFeedbackResult = PrescriptiveFeedbackService.gatherPrescriptiveFeedback(getContext(), prescriptiveFeedbackData);
				PrescriptiveFeedbackService.INSTANCE.prescriptiveFeedbackResult = prescriptiveFeedbackResult;
				return prescriptiveFeedbackResult;
			});
		});
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		Bitmap mCanvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		mDrawCanvas = new Canvas(mCanvasBitmap);
	}

	public static boolean waitingForSwingPlaneInput = false;
	private Pair swingPlaneTerminalEndpoint;
	public static FrameAnalysisResult frameAnalysisResult;
	public static List<Path> feedbackVisualizationPaths = null;
	public static boolean mIsDrawingPose = false;

	public void drawPose(Context myContext, DrawingView drawingView) {
		frameAnalysisResult = frameUtilities.processFrameForVisualization(
				ExoPlayerFragment.mVideoUri.toString(),
				ExoPlayerFragment.currentPosition * 1000
		);

		feedbackVisualizationPaths = FeedbackVisualization.INSTANCE.calculateFeedbackVisualization(frameAnalysisResult, swingPlaneTerminalEndpoint);

		// It's possible that we want to draw a pose on the athlete, we set this variable to true in case they want to
		mIsDrawingPose = true;
		initDrawingType();

		// Any type of analysis requires the drawing view, and we make it visible and allow for the ability to use the onDraw method
		drawingView.setVisibility(View.VISIBLE);
		drawingView.invalidate();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (waitingForSwingPlaneInput) {
			waitingForSwingPlaneInput = false;
			swingPlaneTerminalEndpoint = new Pair(event.getX(), event.getY());
			drawPose(getContext(), this);
			AnnotationFragment.waitingOnPose = true;
			ExoPlayerFragment.exoPlayer.setPlayWhenReady(true); // Start video playback
			return true;
		}

		if (mDrawingFreeHandMode) {
			freeHandModeEventListener(event);
		} else if (mDrawingArrowMode) {
			arrowModeEventListener(event);
		}

		//
		else if (mDrawingStraightLineMode) {
			straightLineModeEventListener(event);
		} else if (mDrawingRectangleMode) {
			rectangleModeEventListener(event);
		} else if (mDrawingCircleMode) {
			circleModeEventListener(event);
		} else {
//            ViewGroup row = (ViewGroup) DrawingView.;
//            TextView textView = (TextView) row.findViewById(R.id.childID);
		}
//        if (mDrawingPoseMode) {
//            poseModeEventListener(event);
//        }

		invalidate(); //Tells the view that the canvas needs to be redrawn
		return true;
	}


	public void clearCanvas() {
		mPaths.clear();
		mPaints.clear();
		mUndonePaths.clear();
		mUndonePaints.clear();
		mDrawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
		invalidate(); //Tells the view that the canvas needs to be redrawn

	}

	// Public setter for the drawing type mode to be called by the calling fragment
	public void setDrawingType(String flag) {
		Log.v(TAG, "SETTING THE DRAWING TYPE");

		if (flag.equals("Circle")) {
			Log.v(TAG, "---------Circle");
			initDrawingType();
			mDrawingCircleMode = true;
		}

		if (flag.equals("Rectangle")) {
			Log.v(TAG, "---------Rectangle");
			initDrawingType();
			mDrawingRectangleMode = true;
		}

		if (flag.equals("FreeHand")) {
			Log.v(TAG, "---------FreeHand");
			initDrawingType();
			mDrawingFreeHandMode = true;
		}
		if (flag.equals("Arrow")) {
			Log.v(TAG, "---------Arrow");
			initDrawingType();
			mDrawingArrowMode = true;
		}

		//
		if (flag.equals("StraightLine")) {
			Log.v(TAG, "---------StraightLine");
			initDrawingType();
			mDrawingStraightLineMode = true;
		}

		if (flag.equals("Trajectory")) {
			Log.v(TAG, "---------Trajectory");
			initDrawingType();
			BaseAnnotatorActivity.trajectorySelected = true;
			invalidate();
		}

		if (flag.equals("Clear")) {
			Log.v(TAG, "---------Clear");
			clearCanvas();
			initDrawingType();
			invalidate();
		}

//        if (flag.equals("Pose")) {
//            initDrawingType();
//            mDrawingPoseMode = true;
//        }

	}


	public String getDrawingType() {
		String drawingType = "";

		if (mDrawingArrowMode) drawingType = "Arrow";
		if (mDrawingCircleMode) drawingType = "Circle";
		if (mDrawingRectangleMode) drawingType = "Rectangle";
		if (mDrawingFreeHandMode) drawingType = "FreeHand";
		//
		if (mDrawingStraightLineMode) drawingType = "StraightLine";
		//if (mDrawingPoseMode) drawingType = "Pose";


		return drawingType;
	}

//================================ USER CALLED DRAWING FUNCTIONS =================================//


	// Public setter for current color of paint
	public void setPaintColor(int color) {
		mPaintColor = color;
		mDrawPaint.setColor(mPaintColor);


	}


	// Public setter for current paint stroke width
	public void setPaintStrokeWidth(int strokeWidth) {
		mStrokeWidth = strokeWidth;
		mDrawPaint.setStrokeWidth(mStrokeWidth);
	}


	/**
	 * Allows the user to undo the previously added path
	 */
	public void undo() {
		if (mPaths.size() > 0) {
			mUndonePaths.add(mPaths.remove(mPaths.size() - 1));
			mUndonePaints.add(mPaints.remove(mPaints.size() - 1));
			invalidate(); //Tells the view that the canvas needs to be redrawn

		}
	}

	/**
	 * Allows the user to redo the previously removed path
	 */
	public void redo() {
		if (mUndonePaths.size() > 0) {
			mPaths.add(mUndonePaths.remove(mUndonePaths.size() - 1));
			mPaints.add(mUndonePaints.remove(mUndonePaints.size() - 1));
			invalidate(); //Tells the view that the canvas needs to be redrawn

		}
	}


	// Handling the onTouchEvent(MotionEvent) when in freehand drawing mode
	public void freeHandModeEventListener(MotionEvent event) {
		Log.v(TAG, "Drawing FreeHand by TouchEvent");

		switch (event.getAction()) {
			//Action when user presses down
			case MotionEvent.ACTION_DOWN:
				mDrawPath.moveTo(event.getX(), event.getY());
				break;
			//Action when user moves pointer
			case MotionEvent.ACTION_MOVE:
				mDrawPath.lineTo(event.getX(), event.getY());
				break;
			//Action when user lifts up pointer
			case MotionEvent.ACTION_UP:
				mDrawPath.lineTo(event.getX(), event.getY());
				mPaths.add(mDrawPath);
				mPaints.add(mDrawPaint);
				mDrawPath = new Path();
				initPaint();
				break;
			default:
				break;
		}
	}


	// Handling the onTouchEvent(MotionEvent) when in arrow drawing mode
	public void arrowModeEventListener(MotionEvent event) {
		Log.v(TAG, "Drawing Arrow by TouchEvent");
		switch (event.getAction()) {

			//Action when user presses down
			case MotionEvent.ACTION_DOWN:

				mDrawPath.reset();
				mDrawPath.moveTo(event.getX(), event.getY());

				beginingCoordinate = new PointF(event.getX(), event.getY());
				endingCoordinate = new PointF();

				invalidate();
				break;


			//Action when user moves pointer
			case MotionEvent.ACTION_MOVE:
				mIsDrawingArrow = true;
				mIsDrawingStraightLine = true;
				mArrowTipPath = new Path();

				endingCoordinate.x = event.getX();
				endingCoordinate.y = event.getY();

				invalidate();
				break;


			//Action when user lifts up pointer
			case MotionEvent.ACTION_UP:
				mDrawPath.lineTo(event.getX(), event.getY());


				float deltaX = endingCoordinate.x - beginingCoordinate.x;
				float deltaY = endingCoordinate.y - beginingCoordinate.y;
				float frac = (float) 0.1;

				float point_x1 = beginingCoordinate.x + ((1 - frac) * deltaX + frac * deltaY);
				float point_y1 = beginingCoordinate.y + ((1 - frac) * deltaY - frac * deltaX);

				float point_x2 = endingCoordinate.x;
				float point_y2 = endingCoordinate.y;

				float point_x3 = beginingCoordinate.x + ((1 - frac) * deltaX - frac * deltaY);
				float point_y3 = beginingCoordinate.y + ((1 - frac) * deltaY + frac * deltaX);


				mDrawPath.moveTo(point_x1, point_y1);
				mDrawPath.lineTo(point_x2, point_y2);
				mDrawPath.lineTo(point_x3, point_y3);
//                mDrawPath.lineTo(point_x1, point_y1);
//                mDrawPath.close(); //This fills in the drawn arrow

				endingCoordinate.x = event.getX();
				endingCoordinate.y = event.getY();


//                    mDrawPath.op(mArrowTipPath,Path.Op.UNION);
				mPaths.add(mDrawPath);
//                mDrawPaint.setStyle(Style.FILL);
				mPaints.add(mDrawPaint);


				mDrawPath = new Path();
//                    mArrowTipPath = new Path();

				initPaint();
				invalidate();
				break;
			default:
				break;
		}
	}

	//
	public void straightLineModeEventListener(MotionEvent event) {
		Log.v(TAG, "Drawing Straight Line by TouchEvent");
		switch (event.getAction()) {

			//Action when user presses down
			case MotionEvent.ACTION_DOWN:

				mDrawPath.reset();
				mDrawPath.moveTo(event.getX(), event.getY());

				beginingCoordinate = new PointF(event.getX(), event.getY());

				endingCoordinate = new PointF();

				invalidate();
				break;


			//Action when user moves pointer
			case MotionEvent.ACTION_MOVE:
				mIsDrawingStraightLine = true;

				endingCoordinate.x = event.getX();
				endingCoordinate.y = event.getY();

				invalidate();
				break;


			//Action when user lifts up pointer
			case MotionEvent.ACTION_UP:
				mDrawPath.lineTo(event.getX(), event.getY());


				endingCoordinate.x = event.getX();
				endingCoordinate.y = event.getY();

				mPaths.add(mDrawPath);
//                mDrawPaint.setStyle(Style.FILL);
				mPaints.add(mDrawPaint);

				mDrawPath = new Path();

				initPaint();
				invalidate();
				break;
			default:
				break;
		}
	}


	// Handling the onTouchEvent(MotionEvent) when in rectangle drawing mode
	public void rectangleModeEventListener(MotionEvent event) {
		Log.v(TAG, "Drawing Rectangle by TouchEvent");

		switch (event.getAction()) {

			case MotionEvent.ACTION_DOWN:
				mIsDrawingRectangle = true;

				beginingCoordinate.x = event.getX();
				beginingCoordinate.y = event.getY();
				endingCoordinate.x = event.getX();
				endingCoordinate.y = event.getY();
				invalidate(); //Tells the view that the canvas needs to be redrawn
				break;


			case MotionEvent.ACTION_MOVE:

				endingCoordinate.x = event.getX();
				endingCoordinate.y = event.getY();
				invalidate(); //Tells the view that the canvas needs to be redrawn
				break;

			case MotionEvent.ACTION_UP:
				mIsDrawingRectangle = false;

				Float leftX = Math.min(beginingCoordinate.x, endingCoordinate.x);
				Float rightX = Math.max(beginingCoordinate.x, endingCoordinate.x);
				Float topY = Math.min(beginingCoordinate.y, endingCoordinate.y);
				Float bottomY = Math.max(beginingCoordinate.y, endingCoordinate.y);
				mDrawPath.addRect(leftX, topY, rightX, bottomY, Path.Direction.CW);
				mPaths.add(mDrawPath);
				mPaints.add(mShapePaint);

				mDrawPath = new Path();
				initPaint();
				break;
			default:
				break;
		}
	}


	// Handling the onTouchEvent(MotionEvent) when in circle drawing mode
	public void circleModeEventListener(MotionEvent event) {
		Log.v(TAG, "Drawing Circle by TouchEvent");

		switch (event.getAction()) {

			case MotionEvent.ACTION_DOWN:
				mIsDrawingCircle = true;

				beginingCoordinate.x = event.getX();
				beginingCoordinate.y = event.getY();
				endingCoordinate.x = event.getX();
				endingCoordinate.y = event.getY();
				invalidate(); //Tells the view that the canvas needs to be redrawn
				break;

			case MotionEvent.ACTION_MOVE:

				endingCoordinate.x = event.getX();
				endingCoordinate.y = event.getY();
				invalidate(); //Tells the view that the canvas needs to be redrawn (For active reshaping)
				break;

			case MotionEvent.ACTION_UP:
				mIsDrawingCircle = false;
				Float leftX = Math.min(beginingCoordinate.x, endingCoordinate.x);
				Float rightX = Math.max(beginingCoordinate.x, endingCoordinate.x);
				Float topY = Math.min(beginingCoordinate.y, endingCoordinate.y);
				Float bottomY = Math.max(beginingCoordinate.y, endingCoordinate.y);
				mDrawPath.addOval(leftX, topY, rightX, bottomY, Path.Direction.CW);
				mPaths.add(mDrawPath);
				mPaints.add(mShapePaint);

				mDrawPath = new Path();
				initPaint();
				break;
			default:
				break;
		}
	}

	public void poseModeEventListener(MotionEvent event) {

	}


//=================================== USER CALLED FUNCTIONS ======================================//


}
