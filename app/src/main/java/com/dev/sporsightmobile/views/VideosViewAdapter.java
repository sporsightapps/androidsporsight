package com.dev.sporsightmobile.views;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.activities.GalleryAdapter;
import com.dev.sporsightmobile.entities.video.Video;
import com.dev.sporsightmobile.entities.video.VideoContract;
import com.dev.sporsightmobile.entities.video.VideoDBHelper;
import com.dev.sporsightmobile.activities.BaseAnnotatorActivity;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.Writer;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.dev.sporsightmobile.Retrofit.RetrofitControllerUploadFile;
import com.dev.sporsightmobile.Retrofit.UploadFile;
import com.google.gson.JsonObject;


public class VideosViewAdapter extends RecyclerView.Adapter<VideosViewAdapter.ViewHolder> {

    private static final String TAG = VideosViewAdapter.class.getSimpleName();

    View videosView;
    private List<Video> mVideosList;
    private Context mContext;
    private Cursor mVideoCursor;
    private final LayoutInflater inflater;
    VideoDBHelper videoDBHelper;

    private Intent mBaseAnnotatorActivity;
    private Intent mBlobStorageActivity;
    private Uri mVideoUri;
    private String mVideoTitle;

    private String videoFileName;
    private RetrofitControllerUploadFile retrofitControllerUploadFile;
    File uploadFile;
    String videoPath;
    File downloadFile;
    Uri fileUri;
    public static String JSONFileName;

    public VideosViewAdapter(Context context, String category) {
        mContext = context;
        inflater = LayoutInflater.from(context);
        videoDBHelper = new VideoDBHelper(context);

        // Abdool Shakur's addition to enable category selection
        if(category.equals("All")) {
            mVideoCursor = videoDBHelper.getAllVideos();
        }
        else {
            mVideoCursor = videoDBHelper.getFilteredVideos(category);
        }
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideosViewAdapter.ViewHolder(
                LayoutInflater.from(mContext).inflate(R.layout.gallery_item, parent, false)
        );
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Log.v(TAG, "___________ BINDING VIEWHOLDER ON POSITION:" + position);

        if (mVideoCursor == null || !mVideoCursor.moveToPosition(position)) {
            return;
        }
        mVideoCursor.moveToPosition(position);

        int col_index_display_name = mVideoCursor.getColumnIndexOrThrow(VideoContract.VideoEntry.COLUMN_VIDEO_TITLE);
        String video_display_name = mVideoCursor.getString(col_index_display_name);
//        holder.getTextView_VideoTitle().setText(video_display_name);

        int col_index_artist = mVideoCursor.getColumnIndexOrThrow(VideoContract.VideoEntry.COLUMN_VIDEO_CREATOR);
        String video_artist = mVideoCursor.getString(col_index_artist);

//        holder.getTextView_VideoAuthor().setText(video_artist);
//        holder.getTextView_VideoAuthor().setText(R.string.coach);


        int col_index_filePath = mVideoCursor.getColumnIndexOrThrow(VideoContract.VideoEntry.COLUMN_VIDEO_FILEPATH);
        String video_filepath = mVideoCursor.getString(col_index_filePath);

        //These will control the tags displayed for each video

        if (video_filepath.contains("Stitched")) {
 //           holder.getVideo_stitched().setText(mContext.getString(R.string.stiched_tag));
        } else {
//            holder.getVideo_stitched().setText(" ");
        }

        if (video_filepath.contains("Camera")) {
 //           holder.getTextView_VideoAnnotated().setText(mContext.getString(R.string.external));
        } else if (video_filepath.contains("Recordings")) {
//            holder.getTextView_VideoAnnotated().setText(mContext.getString(R.string.in_app));
        } else if (video_filepath.contains("Annotations")) {
  //          holder.getTextView_VideoAnnotated().setText(" ");
  //          holder.getVideo_stitched().setText("Annotated");
        }


        int col_index_sport = mVideoCursor.getColumnIndexOrThrow(VideoContract.VideoEntry.COLUMN_VIDEO_SPORT);
        String video_sport = mVideoCursor.getString(col_index_sport);
  //      holder.getTextView_VideoSport().setText(video_sport);

        int col_index_category = mVideoCursor.getColumnIndexOrThrow(VideoContract.VideoEntry.COLUMN_VIDEO_CATEGORY);
        String video_category = mVideoCursor.getString(col_index_category);
   //     holder.getTextView_VideoCategory().setText(video_category);

        int col_index_date = mVideoCursor.getColumnIndexOrThrow(VideoContract.VideoEntry.COLUMN_TIMESTAMP);
        String video_date = mVideoCursor.getString(col_index_date);


//       SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z",Locale.US);
//       DateFormat dateFormat = SimpleDateFormat.getDateTimeInstance();


//        holder.getTextView_VideoDate().setText(video_date);

        int col_index_videoLength = mVideoCursor.getColumnIndexOrThrow(VideoContract.VideoEntry.COLUMN_VIDEO_LENGTH);
        String video_length = mVideoCursor.getString(col_index_videoLength);
    //    holder.getTextView_VideoLength().setText(video_length);


//        int col_index_video_favorited = mVideoCursor.getColumnIndexOrThrow(VideoContract.VideoEntry.COLUMN_VIDEO_FAVORITED);
//        String video_favorited = mVideoCursor.getString(col_index_video_favorited);
//
//        if (!video_favorited.equals("1")) {
//            holder.getImageView_VideoFavorited().setImageAlpha(17); // Semi-transparent
//        } else {
//            holder.getImageView_VideoFavorited().setImageAlpha(255); // Not transparent.
//        }


        //TODO: FIX THE TOGGLE FOR ANNOTATED VIDEO
//                if (!video_item.getAnnotated()){ holder.getTextView_VideoAnnotated().setText(""); }


        Glide.with(mContext)
                .load(getUriFromDatabase(position))
//                .centerCrop()
//                .override(96, 96)
                .into(holder.getImageView());
//            }
//        }
    }


    //=====START ================VIEW HOLDER CLASS ==============================================//
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnCreateContextMenuListener {

        public TextView video_title;
        public TextView video_author;
        public TextView video_sport;
        public TextView video_category;
        public TextView video_date;
        public TextView video_annotated;
        public TextView video_stitched;
        public TextView video_length;
        public ImageView video_favorited;
        private final ImageView mImageView; // TODO: rename to video_thumbnail

        public TextView getTextView_VideoTitle() {
            return video_title;
        }

        public TextView getTextView_VideoAuthor() {
            return video_author;
        }

        public TextView getTextView_VideoSport() {
            return video_sport;
        }

        public TextView getTextView_VideoCategory() {
            return video_category;
        }

        public TextView getTextView_VideoDate() {
            return video_date;
        }

        public TextView getTextView_VideoAnnotated() {
            return video_annotated;
        }

        public TextView getVideo_stitched() {
            return video_stitched;
        }

        public TextView getTextView_VideoLength() {
            return video_length;
        }

        public ImageView getImageView_VideoFavorited() {
            return video_favorited;
        }

        public ImageView getImageView() {
            return mImageView;
        }


        // ======START ===============VIEW HOLDER CONSTRUCTOR ====================================//
        public ViewHolder(View itemView) {
            super(itemView);


//            video_title = itemView.findViewById(R.id.video_title);
//            video_author = itemView.findViewById(R.id.video_author);
//            video_sport = itemView.findViewById(R.id.video_sport);
//            video_category = itemView.findViewById(R.id.video_category);
//            video_date = itemView.findViewById(R.id.video_date);
//            video_annotated = itemView.findViewById(R.id.video_annotated);
//            video_stitched = itemView.findViewById(R.id.video_stiched);
//            video_length = itemView.findViewById(R.id.video_length);
            mImageView = itemView.findViewById(R.id.image);


//            // Handling the click of the favorited star.
//            video_favorited = itemView.findViewById(R.id.video_favorited);
//            video_favorited.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(mContext, "Favorited: " + getAdapterPosition(), Toast.LENGTH_LONG).show(); //TODO: UPDATE THIS TO VIDEO NAME
//                    videoDBHelper.toggleFavoritedVideo(getAdapterPosition());
//                    if (video_favorited.getImageAlpha() > 17) {
//                        video_favorited.setImageAlpha(16); // Removing the color
//                    } else {
//                        video_favorited.setImageAlpha(255); // adding the color back
//                    }
//                }
//            });


            // Handling click on the Previewed Image ThumbNail
            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mBaseAnnotatorActivity = new Intent(mContext, BaseAnnotatorActivity.class);
                    mVideoUri = videoDBHelper.getVideoUri(getAdapterPosition());
                    mBaseAnnotatorActivity.putExtra("EXTRA_URI", mVideoUri);
                    mBaseAnnotatorActivity.putExtra("EXTRA_STARTCODE", "Player");
                    mContext.startActivity(mBaseAnnotatorActivity);

                }
            });


            itemView.setOnCreateContextMenuListener(this);
        }
        // ======END ===============VIEW HOLDER CONSTRUCTOR ======================================//


        @Override
        public void onClick(View v) {
            getOnClickUri(getAdapterPosition());
        }


        // The context menu provides users with more options for a selected video
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            MenuItem menuItem_Play = menu.add(Menu.NONE, 0, getAdapterPosition(), "Play");
            MenuItem menuItem_Share = menu.add(Menu.NONE, 1, getAdapterPosition(), "Share");
            MenuItem menuItem_Annotate = menu.add(Menu.NONE, 3, getAdapterPosition(), "Annotate");
            MenuItem menuItem_Upload = menu.add(Menu.NONE, 5, getAdapterPosition(), "Process The Video");
            MenuItem menuItem_Download = menu.add(Menu.NONE, 6, getAdapterPosition(), "Download The Video");

            menuItem_Play.setOnMenuItemClickListener(videoContextMenuListener);
            menuItem_Share.setOnMenuItemClickListener(videoContextMenuListener);
            menuItem_Annotate.setOnMenuItemClickListener(videoContextMenuListener);
            menuItem_Upload.setOnMenuItemClickListener(videoContextMenuListener);
            menuItem_Download.setOnMenuItemClickListener(videoContextMenuListener);


        }

    }


    private final MenuItem.OnMenuItemClickListener videoContextMenuListener = new MenuItem.OnMenuItemClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.P)
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            int clickedMenuItemPosition = item.getOrder();
            retrofitControllerUploadFile = new RetrofitControllerUploadFile();

            switch (item.getItemId()) {


                case 0: //Play - opens the video player activity with the selected video
                    Log.v(TAG, "Pressed play in the menu");
                    mBaseAnnotatorActivity = new Intent(mContext, BaseAnnotatorActivity.class);
                    mVideoUri = videoDBHelper.getVideoUri(clickedMenuItemPosition);
                    mBaseAnnotatorActivity.putExtra("EXTRA_URI", mVideoUri);
                    mBaseAnnotatorActivity.putExtra("EXTRA_STARTCODE", "Player");
                    mContext.startActivity(mBaseAnnotatorActivity);

                    break;


                case 1: //Share - opens the share feature with the selected video

                    Uri contentUri = videoDBHelper.getVideoContentUri(clickedMenuItemPosition);

                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                    shareIntent.setType("video/*");


                    //TODO: IS THIS THE CORRECT WAY TO CALL IT???
                    mContext.startActivity(Intent.createChooser(shareIntent, mContext.getResources().getText(R.string.send_to)));

                    break;


                case 3: //Annotate
                    mBaseAnnotatorActivity = new Intent(mContext, BaseAnnotatorActivity.class);
                    mVideoUri = videoDBHelper.getVideoUri(clickedMenuItemPosition);
                    mVideoTitle = videoDBHelper.getVideoTitle(clickedMenuItemPosition);
                    mBaseAnnotatorActivity.putExtra("EXTRA_URI", mVideoUri);
                    mBaseAnnotatorActivity.putExtra("EXTRA_TITLE", mVideoTitle);
                    mBaseAnnotatorActivity.putExtra("EXTRA_STARTCODE", "Annotator");
                    mContext.startActivity(mBaseAnnotatorActivity);


                    File mediaStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Camera", "SporSight");
                    File jsonStorageDirectory = new File(mediaStorageDirectory, "JSON");

                    String basename = FilenameUtils.getBaseName(mVideoTitle);
                    String jsonFileName = "processed_" + basename + ".json";
                    File jsonFile = new File(jsonStorageDirectory, jsonFileName);


                    if (jsonFile.exists()) {

                        JSONFileName = jsonFileName;

                    } else {
                        JSONFileName = "";
                    }

                    break;

//                case 4: //Edit
//                    Toast.makeText(mContext, "You Pressed Edit on: " + clickedMenuItemPosition,Toast.LENGTH_SHORT).show();
//
//                    //TESTING THE DRAWING FRAGMENT HERE
//                    mBaseAnnotatorActivity = new Intent(mContext, BaseAnnotatorActivity.class);
//                    mBaseAnnotatorActivity.putExtra("EXTRA_STARTCODE","Drawing");
//                    mContext.startActivity(mBaseAnnotatorActivity);
//                    break;


                //////
                //  Sending the video
                //////

                case 5: //Upload to VM to process the video

                    // We notify the user that they tapped on 'Process the Video'
                    Toast.makeText(mContext, "You pressed on Process the Video", Toast.LENGTH_SHORT).show();

                    // We get the URI information of the video we just tapped on
                    fileUri = videoDBHelper.getVideoUri(clickedMenuItemPosition);

                    // Get file path
                    videoPath = fileUri.getPath();

                    Toast.makeText(mContext, "Video URI: " + videoPath, Toast.LENGTH_SHORT).show();

                    // We make sure that our path is not null
                    if (videoPath != null) {
//                        PosenetWrapper posenet = new PosenetWrapper(mContext, videoPath);
//                        String json = posenet.processFrameByFrameNumber(0);
                        String json = "";
                        Toast.makeText(mContext, "Test: " + json, Toast.LENGTH_LONG).show();
                    }

                    break;


                ///////////
                // Getting the Video back
                ///////////
                case 6:

                    // In order to avoid file saving issues, we do our file saving tasks on the main thread
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);

                    // We get the URI information of the video we just tapped on
                    fileUri = videoDBHelper.getVideoUri(clickedMenuItemPosition);

                    // We make a new file based on the path of fileUri, which is the file we just uploaded
                    uploadFile = new File(fileUri.getPath());
                    videoFileName = "processed_" + uploadFile.getName();

                    // Our API sends back a message as to whether the file is processing or finished
                    // We use these messages to compare to the message sent back
                    final String messageNotDone = "File Is processing";
                    final String messageDone = "Processing Finished";

                    // We send a GET request with the file name of our uploaded video to our API
                    // to check whether the file has finished processing on our API
                    Call<UploadFile> checkIfFileIsCreatedCall = retrofitControllerUploadFile.getUploadFileAPI().getMessage(videoFileName);
                    checkIfFileIsCreatedCall.enqueue(new Callback<UploadFile>() {
                        @Override
                        public void onResponse(Call<UploadFile> call, Response<UploadFile> response) {

                            // We check if our response message is equal to 'messageNotDone'
                            if (response.body().getMessage().equals(messageNotDone)) {

                                // We send out a toast notifying the user the video is currently unavailable
                                Toast.makeText(mContext, "Video Unavailable For Download", Toast.LENGTH_LONG).show();

                                // The message we have received indicates that our video is done processing
                            } else if (response.body().getMessage().equals(messageDone)) {

                                // We send out a toast notifying the user the video is currently still processing
                                Toast.makeText(mContext, "Processing Completed", Toast.LENGTH_LONG).show();

                                // We make a GET request passing in the name of our uploaded video in order to retrieve it form the VM
                                Call<ResponseBody> downloadCall = retrofitControllerUploadFile.getUploadFileAPI().getVideo(videoFileName);
                                downloadCall.enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                        try {

                                            // We specify where our uploaded video will be saved in our Android device
                                            File mediaStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Camera", "SporSight");
                                            File storageDirectory = new File(mediaStorageDirectory, "Recordings");


                                            // We check if the directory we want to store our file exists
                                            if (!storageDirectory.exists()) {

                                                // We make our storage directory and check whether it has been made successfully
                                                if (!storageDirectory.mkdirs()) {

                                                    // If not, we send out a log message indicated the storage directory wasn't created
                                                    Log.d(TAG, "failed to create directory");
                                                }
                                            }

                                            // We make a new file for the processed video we want to download
                                            downloadFile = new File(storageDirectory, videoFileName);

                                            // We write the bytes of our video to 'downloadFile'
                                            OutputStream out = new FileOutputStream(downloadFile);
                                            out.write(response.body().bytes());
                                            out.close();

                                            // We notify where the video has been saved
                                            Toast.makeText(mContext, "Video Is Saved At: " + downloadFile.toString(), Toast.LENGTH_LONG).show();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable e) {

                                        // We output a log message detailing the failure in our POST request
                                        Log.d("downloadCall", e.getLocalizedMessage());
                                        e.printStackTrace();
                                    }

                                });

                                // After the video is processed on the VM we also generate a JSON file containing the keypoints of every frame
                                // Our extract the name of our uploaded file without its extension
                                String basename = FilenameUtils.getBaseName(uploadFile.getName());

                                // The name of the JSON generated on the VM is prepended with "processed_" and ends with ".json"
                                JSONFileName = "processed_" + basename + ".json";

                                // We retrieve the generated JSON file associated with our uploade file by passing in the name of the JSON file
                                // that was generated after processing
                                final Call<JsonObject> jsonCall = retrofitControllerUploadFile.getUploadFileAPI().getJSONFile(JSONFileName);
                                jsonCall.enqueue(new Callback<JsonObject>() {
                                    @Override
                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                                        try {

                                            // We specify where we are going to store our JSON file in our Android device
                                            File mediaStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Camera", "SporSight");
                                            File jsonStorageDirectory = new File(mediaStorageDirectory, "JSON");

                                            // We check if the directory exists, if not we create one
                                            if (!jsonStorageDirectory.exists()) {

                                                // If the directory fails to be created we output a log message indicating so
                                                if (!jsonStorageDirectory.mkdirs()) {
                                                    Log.d(TAG, "failed to create directory");
                                                }
                                            }

                                            // We get back the contents of the JSON file and make it into a String
                                            String jsonString = response.body().toString();

                                            // We save the contents the JSON file into 'jsonFile'
                                            Writer out = null;
                                            File jsonFile = new File(jsonStorageDirectory + File.separator, JSONFileName);
                                            out = new BufferedWriter(new FileWriter(jsonFile));
                                            out.write(jsonString);
                                            out.close();

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                        // We print the stack trace in case our GET request to retrieve the JSON file fails
                                        t.printStackTrace();
                                    }
                                });

                            }

                        }

                        @Override
                        public void onFailure(Call<UploadFile> call, Throwable t) {
                            // If our uploadCall fails we output the stack trace
                            t.printStackTrace();
                        }
                    });

                    break;

            }
            return true;


        }
    };


    // Required by the adapter.
    @Override
    public int getItemCount() {
        return videoDBHelper.numberOfRows();
//        return (mVideoCursor == null) ? 0 : mVideoCursor.getCount();
    }


    // ===================================HANDLING THE URI FOR THE VIDEO ====================//
    private Uri getUriFromDatabase(int position) {
        mVideoCursor.moveToPosition(position);
        int dataIndex = mVideoCursor.getColumnIndex(VideoContract.VideoEntry.COLUMN_VIDEO_FILEPATH);
        String dataString = mVideoCursor.getString(dataIndex);
        Uri mediaUri = Uri.parse("file://" + dataString);
        return mediaUri;
    }


    //TODO : FIGURE OUT WHEN THIS IS NEEDED
    //This gets the file Uri with the prefix "content://" for to assure proper access when sharing Uri
    private void getOnClickUri(int position) {

        int dataIndex = mVideoCursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
        mVideoCursor.moveToPosition(position);
        String dataString = mVideoCursor.getString(dataIndex);

        String authorities = mContext.getPackageName() + ".fileprovider";
        Uri mediaUri = FileProvider.getUriForFile(mContext, authorities, new File(dataString));


    }

    // ===================================HANDLING THE URI FOR THE VIDEO ====================//


}