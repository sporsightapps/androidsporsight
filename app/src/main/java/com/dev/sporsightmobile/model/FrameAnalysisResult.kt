package com.dev.sporsightmobile.model

import org.tensorflow.lite.examples.posenet.lib.ModelExecutionResult
import org.tensorflow.lite.examples.posenet.lib.Person

data class FrameAnalysisResult(
    @Deprecated("Use frameInfo instead")
    val time: Long,
    val poseEstimation: Person,
    val imageSegmentation: ModelExecutionResult?,
    val scaledPoseEstimation: Person,
    val scaledImageSegmentation: ModelExecutionResult?,
    val croppedWidth: Int,
    val croppedHeight: Int,
    val frameInfo: FrameInfo
) {}