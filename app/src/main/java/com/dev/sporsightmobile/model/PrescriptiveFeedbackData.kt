package com.dev.sporsightmobile.model

import androidx.core.util.Pair
import com.dev.sporsightmobile.enums.BodyPosition
import com.dev.sporsightmobile.utilities.PoseEstimationUtilities
import org.tensorflow.lite.examples.posenet.lib.Position

data class PrescriptiveFeedbackData(val firstFrameAnalysisResult: FrameAnalysisResult,
                                    val videoAnalysisResult: List<FrameAnalysisResult>,
                                    val bodyPosition: BodyPosition,
                                    val golfBallPosition: Pair<Int, Int>) {
}