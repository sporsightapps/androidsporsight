package com.dev.sporsightmobile.model

import android.graphics.Color

data class SelectedJoint(val jointName:String, val pastColor:Color, val futureColor:Color) {

}