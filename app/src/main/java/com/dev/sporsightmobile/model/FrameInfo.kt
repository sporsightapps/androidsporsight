package com.dev.sporsightmobile.model

import android.graphics.Bitmap

data class FrameInfo(val index: Int, val timeInMilliseconds: Long) {
}