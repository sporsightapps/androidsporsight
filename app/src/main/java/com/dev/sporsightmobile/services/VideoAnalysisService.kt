package com.dev.sporsightmobile.services

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.dev.sporsightmobile.model.FrameAnalysisResult
import com.dev.sporsightmobile.utilities.FrameUtilities
import java.security.AccessController.getContext
import java.util.concurrent.CompletableFuture
import java.util.function.Supplier

object VideoAnalysisService {
    fun setTimeoutSync(runnable: Runnable, delay: Int) {
        try {
            Thread.sleep(delay.toLong())
            runnable.run()
        } catch (e: Exception) {
            System.err.println(e)
        }
    }

    @JvmField
    var analysisResult: CompletableFuture<List<FrameAnalysisResult>>? = null;
    var startTime = System.nanoTime();

    @RequiresApi(Build.VERSION_CODES.P)
    @JvmStatic
    fun processVideo(videoPath: Uri, context: Context) {
        this.analysisResult = CompletableFuture.supplyAsync(Supplier<List<FrameAnalysisResult>> {
            startTime = System.nanoTime();
            return@Supplier FrameUtilities(context).processEntireVideo(videoPath);
        }).thenApply { analysisResult ->
            val time = (System.nanoTime() - startTime) / 1000000
            Log.d("VideoAnalysisService", "Time to execute background thread for video processing was " + time + "ms")
            return@thenApply analysisResult;
        }
    }
}