package com.dev.sporsightmobile.services;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

//import com.microsoft.azure.storage.CloudStorageAccount;
//import com.microsoft.azure.storage.blob.CloudBlobClient;
//import com.microsoft.azure.storage.blob.CloudBlobContainer;
//import com.microsoft.azure.storage.blob.CloudBlockBlob;
//import com.microsoft.azure.storage.blob.ListBlobItem;

import androidx.fragment.app.FragmentActivity;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;

public class BlobStorageDownloadVideo extends AsyncTask {

    public Context context;

    public Activity activity;

    public String filename;

    public File downloadedFile;


    private static final String storageURL = "https://sporsightmobile.blob.core.windows.net/containerpublic  sporsightmobile";
    private static final String storageContainer = "video-storage";
    private static final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=sporsightmobile;AccountKey=/Je3FmjgOQ8rswYUAEMkXf1mIwB9twwyw5cZZkHLijiiSwSxwvVfftM6z/X1SX9KuS3O6mtzW6i3Gfhr2p9mNA==;EndpointSuffix=core.windows.net";

    public BlobStorageDownloadVideo(Activity parent) {
        this.activity = parent;
    }

    public BlobStorageDownloadVideo(FragmentActivity activity, String filename, File downloadedFile) {
        this.activity = activity;
        this.filename = filename;
        this.downloadedFile = downloadedFile;
    }

    @Override
    protected Boolean doInBackground(Object... params) {
        try {
            // Download the blob to a local file
            // Append the string "DOWNLOAD" before the .txt extension so that you can see both files.
            //File file;

            //File downloadedFile = new File(localPath + downloadFileName);

            CloudStorageAccount storageAccount = null;
            try {
                storageAccount = CloudStorageAccount.parse(storageConnectionString);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.createCloudBlobClient();

            // Retrieve reference to a previously created container.
            try {
                CloudBlobContainer container = blobClient.getContainerReference(storageContainer);
                CloudBlockBlob blob = container.getBlockBlobReference(filename);

                blob.downloadToFile(downloadedFile.getAbsolutePath());

            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (StorageException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Success!");
            return true;
        } catch (Exception e) {
            // Output the stack trace.
            System.out.println("Failed to download Blob!");
            e.printStackTrace();
            return false;

        }
    }


}