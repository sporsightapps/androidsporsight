package com.dev.sporsightmobile.services

import android.util.Log
import org.tensorflow.lite.examples.posenet.lib.Position

object LineCheck {
    fun distance(a: Position, b: Position): Long {
        val xSquared = (a.x - b.x) * (a.x - b.x)
        val ySquared = (a.y - b.y) * (a.y - b.y)
        return Math.round(Math.sqrt(xSquared.toDouble() + ySquared.toDouble()))
    }

    fun findAngle(pointA: Position, pointB: Position): Double {
        val hypotenuse = distance(pointA, pointB)
        val adjacent = distance(pointA, Position(pointB.x, pointA.y))
        val angle = Math.acos(adjacent.toDouble() / hypotenuse)
        Log.d("LineCheck", "Angle was $angle")
        return angle;
    }

    // Not for vertical lines.

    fun findLineEquation(pointA: Position, pointB: Position): (Int) -> Int {
        // m = y2-y1 / x2- x1
        val m = (pointA.y - pointB.y).toDouble() / (pointA.x - pointB.x).toDouble()

        // y = mx + b -> b = y - mx
        val b = pointA.y - (m * pointA.x).toDouble()

        return { x: Int -> Math.round((m * x) + b).toInt() }
    }

    fun isAboveLine(point: Position, line: (Int) -> Int): Boolean {
        return point.y < line(point.x)
    }

    fun isBelowLine(point: Position, line: (Int) -> Int): Boolean {
        return point.y > line(point.x)
    }

    // Seperate functions dealing with vertical lines.

    fun isVerticalLine(pointA: Position, pointB: Position): Boolean {
        return pointA.x == pointB.x
    }

    fun findVerticalLineEquation(point: Position): Int {
        return point.x
    }

    fun isRightOfVerticalLine(point: Position, line: Int): Boolean {
        return point.x > line
    }

    fun isLeftOfVerticalLine(point: Position, line: Int): Boolean {
        return point.x < line
    }
}