package com.dev.sporsightmobile.services

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import com.dev.sporsightmobile.utilities.FrameUtilities
import com.google.gson.Gson
import org.tensorflow.lite.examples.posenet.lib.ModelExecutionResult
import org.tensorflow.lite.examples.posenet.lib.Person
import org.tensorflow.lite.examples.posenet.lib.Position
import kotlin.math.roundToInt


object DrawingViewScalingService {
    val displayMetrics = Resources.getSystem().getDisplayMetrics()
    val displayAspectRatio = displayMetrics.widthPixels.toDouble() / displayMetrics.heightPixels
    val drawingViewVerticalOffsetDP = 36
    val drawingViewVerticalOffsetPX = drawingViewVerticalOffsetDP.toDouble() * displayMetrics.density

    fun getDrawingViewScalingFactor(videoScalingInformation: VideoScalingInformation): Double {
        val (originalVideoWidth, originalVideoHeight) = videoScalingInformation;

        val videoAspectRatio = originalVideoWidth.toDouble() / originalVideoHeight

        return if (displayAspectRatio - videoAspectRatio < 0)
            (displayMetrics.heightPixels.toDouble() / originalVideoHeight)
        else (displayMetrics.widthPixels.toDouble() / originalVideoWidth)
    }

    fun scalePositionToDrawingView(position: Position, videoScalingInformation: VideoScalingInformation): Position {
        val (originalVideoWidth, originalVideoHeight, croppedWidth, croppedHeight) = videoScalingInformation;
        val widthBeforeScaling = originalVideoWidth - croppedWidth
        val heightBeforeScaling = originalVideoHeight - croppedHeight

        // Adjust heights and widths to fit our video frame
        val xScaledToOriginalVideoResolution = (position.x.toFloat() / FrameUtilities.modelWidth * widthBeforeScaling) + (croppedWidth / 2)
        val yScaledToOriginalVideoResolution = (position.y.toFloat() / FrameUtilities.modelHeight * heightBeforeScaling) + (croppedHeight / 2)

        val scalingFactor = getDrawingViewScalingFactor(videoScalingInformation)

        val displayViewVideoHeight = originalVideoHeight * scalingFactor
        val displayViewVideoWidth = originalVideoWidth * scalingFactor

        val horizontalOffset = (displayMetrics.widthPixels - displayViewVideoWidth) / 2
        val verticalOffset = ((displayMetrics.heightPixels - displayViewVideoHeight) / 2) - drawingViewVerticalOffsetPX

        position.x = ((xScaledToOriginalVideoResolution * scalingFactor) + horizontalOffset).roundToInt()
        position.y = ((yScaledToOriginalVideoResolution * scalingFactor) + verticalOffset).roundToInt()

        return position;
    }

    fun scalePoseEstimationToDrawingView(poseEstimation: Person, videoScalingInformation: VideoScalingInformation): Person {
        val gson = Gson()
        val scaledPoseEstimation = gson.fromJson(gson.toJson(poseEstimation), Person::class.java)

        scaledPoseEstimation.keyPoints.parallelStream().forEach { keyPoint ->
            scalePositionToDrawingView(keyPoint.position, videoScalingInformation)
        };

        return scaledPoseEstimation;
    }

    fun transformBitmap(bitmap: Bitmap, scaledVideoWidth: Double, scaledVideoHeight: Double, horizontalOffset: Double, verticalOffset: Double): Bitmap {
        val transformedBitmap = Bitmap.createBitmap(
                scaledVideoWidth.roundToInt() + 1,
                scaledVideoHeight.roundToInt() + 1,
                bitmap.config
        )

        val canvas = Canvas(transformedBitmap)

        val matrix = Matrix();
        matrix.preScale((scaledVideoWidth / FrameUtilities.modelWidth).toFloat(), (scaledVideoHeight / FrameUtilities.modelHeight).toFloat())
        matrix.postTranslate(horizontalOffset.toFloat(), verticalOffset.toFloat())

        canvas.drawBitmap(bitmap, matrix, Paint())

        return transformedBitmap;
    }

    fun scaleImageSegmentationToDrawingView(imageSegmentation: ModelExecutionResult, videoScalingInformation: VideoScalingInformation): ModelExecutionResult {
        val (originalVideoWidth, originalVideoHeight, croppedWidth, croppedHeight) = videoScalingInformation;

        val scalingFactor = getDrawingViewScalingFactor(videoScalingInformation)

        val drawingViewScaledWidth = (originalVideoWidth.toDouble() - croppedWidth) * scalingFactor
        val drawingViewScaledHeight = (originalVideoHeight.toDouble() - croppedHeight) * scalingFactor

        val horizontalOffset = ((displayMetrics.widthPixels.toDouble() - drawingViewScaledWidth) / 2)
        val verticalOffset = ((displayMetrics.heightPixels.toDouble() - drawingViewScaledHeight) / 2) - drawingViewVerticalOffsetPX

        val scaledImageSegmentation = transformBitmap(imageSegmentation.bitmapMaskOnly, drawingViewScaledWidth, drawingViewScaledHeight, horizontalOffset, verticalOffset)

        return ModelExecutionResult(imageSegmentation.bitmapResult,
                imageSegmentation.bitmapOriginal,
                scaledImageSegmentation,
                imageSegmentation.executionLog,
                imageSegmentation.itemsFound);
    }
}