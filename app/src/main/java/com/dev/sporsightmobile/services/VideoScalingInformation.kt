package com.dev.sporsightmobile.services

data class VideoScalingInformation(val originalVideoWidth: Int, val originalVideoHeight: Int, val croppedWidth: Int, val croppedHeight: Int) {
}