package com.dev.sporsightmobile.services

import android.graphics.*
import androidx.core.util.Pair
import com.dev.sporsightmobile.enums.BodyPosition
import com.dev.sporsightmobile.model.FrameAnalysisResult
import com.dev.sporsightmobile.services.LineCheck.distance
import com.dev.sporsightmobile.utilities.ImageMaskUtilities
import com.dev.sporsightmobile.utilities.PoseEstimationUtilities
import com.dev.sporsightmobile.utilities.PoseUtility
import org.tensorflow.lite.examples.posenet.lib.BodyPart
import org.tensorflow.lite.examples.posenet.lib.KeyPoint
import org.tensorflow.lite.examples.posenet.lib.Position

object FeedbackVisualization {
    private val imageMaskUtilities = ImageMaskUtilities()

    private var paint: Paint? = null
    private var scaledKeyPoints: List<KeyPoint>? = null
    private var scaledImageSegmentation: Bitmap? = null
    private var swingPlaneTerminalEndpoint: Pair<Float, Float>? = null
    private var feedbackVisualizationPaths: ArrayList<Path>? = null

    init {

    }

    private fun drawFaceBoundingBoxSideView(
            ear: BodyPart
    ) {
        val earKeyPoint = PoseUtility.findKeypointByBodyPart(scaledKeyPoints, ear)
        val earPosition = earKeyPoint.position
        val firstBound = if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_RIGHT) imageMaskUtilities.findMaxX(
                scaledImageSegmentation!!,
                earPosition.x,
                earPosition.y,
                scaledImageSegmentation!!.width,
                earPosition.y) else imageMaskUtilities.findMinX(
                scaledImageSegmentation!!,
                0,
                earPosition.y,
                earPosition.x,
                earPosition.y)
        val boxRadiusPosition = Position()
        boxRadiusPosition.x = firstBound
        boxRadiusPosition.y = earPosition.y
        val boxRadius = distance(earPosition, boxRadiusPosition) * 1.3
        val leftBound = earPosition.x - boxRadius
        val rightBound = earPosition.x + boxRadius
        val topBound = earPosition.y - boxRadius
        val bottomBound = earPosition.y + boxRadius
        val faceBoundingBox = RectF(
                leftBound.toFloat(),
                topBound.toFloat(),
                rightBound.toFloat(),
                bottomBound.toFloat()
        )
        val path = Path()
        path.addRect(faceBoundingBox, Path.Direction.CW)
        feedbackVisualizationPaths!!.add(path)
    }

    private fun drawFaceBoundingBoxNonSideView() {
        val leftEarKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                BodyPart.LEFT_EAR
        )
        val rightEarKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                BodyPart.RIGHT_EAR
        )
        val shoulderKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                BodyPart.RIGHT_SHOULDER
        )
        val leftEarPosition = leftEarKeyPoint.position
        val rightEarPosition = rightEarKeyPoint.position
        val shoulderPosition = shoulderKeyPoint.position
        val leftBound = rightEarPosition.x
        val rightBound = leftEarPosition.x
        val bottomBound = shoulderPosition.y
        val topBound = leftEarPosition.y +
                (leftEarPosition.y - shoulderPosition.y)
        val boxPadding = 20
        val faceBoundingBox = RectF(
                (leftBound - boxPadding).toFloat(),
                (topBound - boxPadding).toFloat(),
                (rightBound + boxPadding).toFloat(),
                (bottomBound + boxPadding).toFloat()
        )
        val path = Path()
        path.addRect(faceBoundingBox, Path.Direction.CW)
        feedbackVisualizationPaths!!.add(path)
    }

    private fun drawFaceBoundingBox() {
        if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_RIGHT) {
            drawFaceBoundingBoxSideView(
                    BodyPart.RIGHT_EAR
            )
        }
        if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_LEFT) {
            drawFaceBoundingBoxSideView(
                    BodyPart.LEFT_EAR
            )
        }
        if (PoseEstimationUtilities.bodyPosition == BodyPosition.FRONT_LEFT ||
                PoseEstimationUtilities.bodyPosition == BodyPosition.FRONT_RIGHT) {
            drawFaceBoundingBoxNonSideView()
        }
    }

    private fun drawBackLine() {
        val hipKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_RIGHT) BodyPart.RIGHT_HIP else BodyPart.LEFT_HIP
        )
        val shoulderKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_RIGHT) BodyPart.RIGHT_SHOULDER else BodyPart.LEFT_SHOULDER
        )
        val hipPosition = hipKeyPoint.position
        val shoulderPosition = shoulderKeyPoint.position
        val midpoint = Position()
        midpoint.x = (shoulderKeyPoint.position.x +
                hipKeyPoint.position.x) /
                2
        midpoint.y = (hipKeyPoint.position.y +
                shoulderKeyPoint.position.y) /
                2
        val minY = imageMaskUtilities.findMinY(
                scaledImageSegmentation!!,
                midpoint.x,
                0,
                midpoint.x,
                midpoint.y
        )
        val translationAmount = minY - midpoint.y

        val path = Path()
        path.moveTo(hipPosition.x.toFloat(),
                hipPosition.y + translationAmount.toFloat())
        path.lineTo(shoulderPosition.x.toFloat(),
                shoulderPosition.y + translationAmount.toFloat())
        feedbackVisualizationPaths!!.add(path)
    }

    private fun drawHorizontalDriftBoundary() {
        val hipKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_RIGHT) BodyPart.RIGHT_HIP else BodyPart.LEFT_HIP
        )
        val hipPosition = hipKeyPoint.position
        val startY = hipPosition.y - 20
        val endY = hipPosition.y + 20
        val x = if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_RIGHT) imageMaskUtilities.findMinX(
                scaledImageSegmentation!!,
                0,
                startY,
                hipPosition.x,
                endY
        ) else imageMaskUtilities.findMaxX(
                scaledImageSegmentation!!,
                hipPosition.x,
                startY,
                scaledImageSegmentation!!.width - 1,
                endY
        )

        val path = Path()
        path.moveTo(x.toFloat(), startY - 300.toFloat())
        path.lineTo(x.toFloat(), endY + 300.toFloat())
        feedbackVisualizationPaths!!.add(path)
    }

    private fun drawSwingPlaneLine() {
        val shoulderKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_RIGHT) BodyPart.RIGHT_SHOULDER else BodyPart.LEFT_SHOULDER
        )
        val hipKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (PoseEstimationUtilities.bodyPosition == BodyPosition.SIDE_RIGHT) BodyPart.RIGHT_HIP else BodyPart.LEFT_HIP
        )
        val shoulderPosition = shoulderKeyPoint.position
        val hipPosition = hipKeyPoint.position

        val upperBoundPath = Path()
        upperBoundPath.moveTo(shoulderPosition.x.toFloat(),
                shoulderPosition.y.toFloat())
        upperBoundPath.lineTo(swingPlaneTerminalEndpoint!!.first!!,
                swingPlaneTerminalEndpoint!!.second!!)
        feedbackVisualizationPaths!!.add(upperBoundPath)


        val lowerBoundPath = Path()
        lowerBoundPath.moveTo(hipPosition.x.toFloat(),
                hipPosition.y.toFloat())
        lowerBoundPath.lineTo(swingPlaneTerminalEndpoint!!.first!!,
                swingPlaneTerminalEndpoint!!.second!!)
        feedbackVisualizationPaths!!.add(lowerBoundPath)
    }

    private fun drawStanceLines() {
        val isRightHanded = PoseEstimationUtilities.bodyPosition == BodyPosition.FRONT_RIGHT
        val offShoulderKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (isRightHanded) BodyPart.LEFT_SHOULDER else BodyPart.RIGHT_SHOULDER
        )
        val offAnkleKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (isRightHanded) BodyPart.LEFT_ANKLE else BodyPart.RIGHT_ANKLE
        )
        val dominantHipKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (isRightHanded) BodyPart.RIGHT_HIP else BodyPart.LEFT_HIP
        )
        val dominantAnkleKeyPoint = PoseUtility.findKeypointByBodyPart(
                scaledKeyPoints,
                if (isRightHanded) BodyPart.RIGHT_ANKLE else BodyPart.LEFT_ANKLE
        )
        val offShoulderPosition = offShoulderKeyPoint.position
        val offAnklePosition = offAnkleKeyPoint.position
        val dominantHipPosition = dominantHipKeyPoint.position
        val dominantAnklePosition = dominantAnkleKeyPoint.position
        val offHandX = if (isRightHanded) imageMaskUtilities.findMaxX(
                scaledImageSegmentation!!,
                offAnklePosition.x,
                offAnklePosition.y - 50,
                scaledImageSegmentation!!.width,
                offAnklePosition.y + 50
        ) else imageMaskUtilities.findMinX(
                scaledImageSegmentation!!,
                0,
                offAnklePosition.y - 50,
                offAnklePosition.x,
                offAnklePosition.y + 50
        )

        val offHandPath = Path()
        offHandPath.moveTo(offHandX.toFloat(),
                offShoulderPosition.y.toFloat())
        offHandPath.lineTo(offHandX.toFloat(),
                offAnklePosition.y.toFloat())
        feedbackVisualizationPaths!!.add(offHandPath)


        val dominantHandPath = Path()
        dominantHandPath.moveTo(dominantHipPosition.x.toFloat(),
                dominantHipPosition.y.toFloat())
        dominantHandPath.lineTo(dominantAnklePosition.x.toFloat(),
                dominantAnklePosition.y.toFloat())
        feedbackVisualizationPaths!!.add(dominantHandPath)
    }

    fun calculateFeedbackVisualization(frameAnalysisResult: FrameAnalysisResult, swingPlaneTerminalEndpoint: Pair<Float, Float>): ArrayList<Path>? {
        this.scaledKeyPoints = frameAnalysisResult.scaledPoseEstimation.keyPoints
        this.scaledImageSegmentation = frameAnalysisResult.scaledImageSegmentation!!.bitmapMaskOnly
        this.swingPlaneTerminalEndpoint = swingPlaneTerminalEndpoint
        feedbackVisualizationPaths = ArrayList()

        drawFaceBoundingBox()
        if (PoseEstimationUtilities.bodyPosition!!.isSideView) {
            drawHorizontalDriftBoundary()
            drawBackLine()
            drawSwingPlaneLine()
        } else {
            drawStanceLines()
        }

        return feedbackVisualizationPaths
    }
}