package com.dev.sporsightmobile.services

import Quadruple
import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.dev.sporsightmobile.enums.BodyPosition
import com.dev.sporsightmobile.model.FrameAnalysisResult
import com.dev.sporsightmobile.model.PrescriptiveFeedbackData
import org.tensorflow.lite.examples.posenet.lib.BodyPart
import org.tensorflow.lite.examples.posenet.lib.BodyPart.*
import org.tensorflow.lite.examples.posenet.lib.KeyPoint
import org.tensorflow.lite.examples.posenet.lib.Position
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.stream.Collectors
import kotlin.collections.ArrayList


object PrescriptiveFeedbackService {
    //    @JvmField
//    var prescriptiveFeedbackPromise: CompletableFuture<List<FrameAnalysisResult>>? = null;
    val scoreThreshold = 0.7

    fun convertPairToPosition(pair: androidx.core.util.Pair<Int, Int>): Position {
        val position = Position()
        position.x = pair.first!!
        position.y = pair.second!!
        return position
    }

    fun findKeypoint(keypoints: List<KeyPoint>): (BodyPart) -> KeyPoint {
        return { bodyPart: BodyPart -> keypoints.find { keyPoint -> keyPoint.bodyPart == bodyPart }!! }
    }

    fun getBodyScale(prescriptiveFeedbackData: PrescriptiveFeedbackData): Int {
        val (firstFrameAnalysisResult, videoAnalysisResult, bodyPosition, golfBallPositionAsPair) = prescriptiveFeedbackData

        val firstFramekeypoints = firstFrameAnalysisResult.scaledPoseEstimation.keyPoints
        val findInFirstFrame = findKeypoint(firstFramekeypoints);
        val (shoulder, hip, knee, ankle) = if (bodyPosition == BodyPosition.SIDE_LEFT)
            Quadruple(findInFirstFrame(LEFT_SHOULDER), findInFirstFrame(LEFT_HIP), findInFirstFrame(LEFT_KNEE), findInFirstFrame(LEFT_ANKLE))
        else Quadruple(findInFirstFrame(RIGHT_SHOULDER), findInFirstFrame(RIGHT_HIP), findInFirstFrame(RIGHT_KNEE), findInFirstFrame(RIGHT_ANKLE))


        val distance = LineCheck::distance;
        val bodyScale = (distance(shoulder.position, hip.position)
                + distance(hip.position, knee.position)
                + distance(knee.position, ankle.position)).toInt()
        Log.d(PrescriptiveFeedbackService.toString(), "Bodyscale was $bodyScale")
        return bodyScale
    }

    fun millisecondToTimestamp(millisecond: Long): String {
        val date = Date(millisecond)
        // dropping "HH:mm" from the format as I don't think we'll need hour/minute precision
        val formatter = SimpleDateFormat("ss.SSS")
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        return formatter.format(date)
    }

    fun createEvents(eventFrames: List<FrameAnalysisResult>, message: String): String? {
        val events: ArrayList<ArrayList<FrameAnalysisResult>> = ArrayList()
        var groupedEventFrames: ArrayList<FrameAnalysisResult> = ArrayList()
        for (i in eventFrames.indices) {
            if (!(groupedEventFrames.isEmpty() || (eventFrames[i].frameInfo.index - groupedEventFrames.last().frameInfo.index < 5))) {
                events.add(groupedEventFrames)
                groupedEventFrames = ArrayList()
            }
            if (i == eventFrames.lastIndex) {
                events.add(groupedEventFrames)
            }
            groupedEventFrames.add((eventFrames[i]))
        }

        val timeRanges = events
                .filter { groupOfFrames -> groupOfFrames.size >= 3 }
                .map { groupOfFrames ->
                    val startTime = millisecondToTimestamp(groupOfFrames.first().frameInfo.timeInMilliseconds)
                    val endTime = millisecondToTimestamp(groupOfFrames.last().frameInfo.timeInMilliseconds)
                    return@map "$startTime - $endTime"
                }.joinToString();

        val completedString = message + " " + timeRanges
        return if (timeRanges != "") completedString else null
    }

    fun gatherSwingPlaneFeedback(prescriptiveFeedbackData: PrescriptiveFeedbackData, impactFrameIndex: Int): String? {
        val (firstFrameAnalysisResult, videoAnalysisResult, bodyPosition, golfBallPositionAsPair) = prescriptiveFeedbackData

        val firstFramekeypoints = firstFrameAnalysisResult.scaledPoseEstimation.keyPoints
        val (shoulderBodyPart, hipBodyPart, wristBodyPart) = if (bodyPosition == BodyPosition.SIDE_LEFT)
            Triple(LEFT_SHOULDER, LEFT_HIP, LEFT_WRIST)
        else Triple(RIGHT_SHOULDER, RIGHT_HIP, RIGHT_WRIST)

        val shoulderKeypoint = firstFramekeypoints.find { keyPoint -> keyPoint.bodyPart == shoulderBodyPart }
        val hipKeypoint = firstFramekeypoints.find { keyPoint -> keyPoint.bodyPart == hipBodyPart }
        val golfBallBallPosition = convertPairToPosition(golfBallPositionAsPair)

        val upperBound = LineCheck.findLineEquation(golfBallBallPosition, shoulderKeypoint!!.position)
        val lowerBound = LineCheck.findLineEquation(golfBallBallPosition, hipKeypoint!!.position)

        val swingPlaneMisses = videoAnalysisResult.stream().filter { frameAnalysis ->
            val wristKeyPoint = frameAnalysis.scaledPoseEstimation.keyPoints.find { keypoint -> keypoint.bodyPart == wristBodyPart }
            val wristPosition = wristKeyPoint!!.position
            return@filter wristKeyPoint.score > scoreThreshold && frameAnalysis.frameInfo.index < impactFrameIndex && (LineCheck.isAboveLine(wristPosition, upperBound) || LineCheck.isBelowLine(wristPosition, lowerBound))
        }.collect(Collectors.toList())

        return createEvents(swingPlaneMisses, "Your wrists left the swing plane at these times:")
    }

    fun gatherRearPositionFeedback(prescriptiveFeedbackData: PrescriptiveFeedbackData, impactFrameIndex: Int): String? {
        val (firstFrameAnalysisResult, videoAnalysisResult, bodyPosition, golfBallPositionAsPair) = prescriptiveFeedbackData

        val firstFramekeypoints = firstFrameAnalysisResult.scaledPoseEstimation.keyPoints

        val hipBodyPart = if (bodyPosition == BodyPosition.SIDE_LEFT)
            LEFT_HIP
        else RIGHT_HIP

        val initialHipPosition = firstFramekeypoints.find { keypoint -> keypoint.bodyPart == hipBodyPart }!!.position
        val sanitizedInitialHipPosition = Position(initialHipPosition.x, 0)
        val driftThreshold = 1.0 / 16 * getBodyScale(prescriptiveFeedbackData)

        val hipDriftEvents = videoAnalysisResult.stream().filter { frameAnalysis ->
            val hipKeyPoint = frameAnalysis.scaledPoseEstimation.keyPoints.find { keypoint -> keypoint.bodyPart == hipBodyPart }
            val hipPosition = hipKeyPoint!!.position
            val sanitizedHipPosition = Position(hipPosition.x, 0)
            return@filter hipKeyPoint.score > scoreThreshold && frameAnalysis.frameInfo.index < impactFrameIndex && (LineCheck.distance(sanitizedInitialHipPosition, sanitizedHipPosition) > driftThreshold)
        }.collect(Collectors.toList())

        return createEvents(hipDriftEvents, "Your hips moved at these times:")
    }

    fun gatherBackAngleFeedback(prescriptiveFeedbackData: PrescriptiveFeedbackData, impactFrameIndex: Int): String? {
        val (firstFrameAnalysisResult, videoAnalysisResult, bodyPosition, golfBallPositionAsPair) = prescriptiveFeedbackData

        val firstFramekeypoints = firstFrameAnalysisResult.scaledPoseEstimation.keyPoints

        val findInFirstFrame = findKeypoint(firstFramekeypoints);

        val (hip, shoulder) = if (bodyPosition == BodyPosition.SIDE_LEFT)
            Pair(LEFT_HIP, LEFT_SHOULDER)
        else Pair(RIGHT_HIP, RIGHT_SHOULDER)

        val initialHipAngle = LineCheck.findAngle(findInFirstFrame(hip).position, findInFirstFrame(shoulder).position)
        Log.d(PrescriptiveFeedbackService.toString(), "Initial hip angle was $initialHipAngle")
        var angleThreshold = 0.3490659 // 20 degrees in radians

        val backAngleEvents = videoAnalysisResult.stream().filter { frameAnalysis ->
            val hipKeyPoint = frameAnalysis.scaledPoseEstimation.keyPoints.find { keypoint -> keypoint.bodyPart == hip }!!
            val shoulderKeyPoint = frameAnalysis.scaledPoseEstimation.keyPoints.find { keypoint -> keypoint.bodyPart == shoulder }!!

            val angle = LineCheck.findAngle(hipKeyPoint.position, shoulderKeyPoint.position)

            return@filter (hipKeyPoint.score > scoreThreshold && frameAnalysis.frameInfo.index < impactFrameIndex && shoulderKeyPoint.score > scoreThreshold && frameAnalysis.frameInfo.index < impactFrameIndex) && (Math.abs(angle - initialHipAngle) > angleThreshold)
        }.collect(Collectors.toList())

        return createEvents(backAngleEvents, "Your back angle changed at these times:")
    }

    fun gatherHeadDriftSideFeedback(prescriptiveFeedbackData: PrescriptiveFeedbackData, impactFrameIndex: Int): String? {
        val (firstFrameAnalysisResult, videoAnalysisResult, bodyPosition, golfBallPositionAsPair) = prescriptiveFeedbackData

        val firstFramekeypoints = firstFrameAnalysisResult.scaledPoseEstimation.keyPoints
        val findInFirstFrame = findKeypoint(firstFramekeypoints);

        val ear = if (bodyPosition == BodyPosition.SIDE_LEFT)
            LEFT_EAR
        else RIGHT_EAR

        val initialEarPosition = findInFirstFrame(ear).position
        val driftThreshold = 1.0 / 6 * getBodyScale(prescriptiveFeedbackData)

        val headDriftEvents = videoAnalysisResult.stream().filter { frameAnalysis ->
            val earKeyPoint = frameAnalysis.scaledPoseEstimation.keyPoints.find { keypoint -> keypoint.bodyPart == ear }!!
            val earPosition = earKeyPoint.position
            val distance = LineCheck.distance(initialEarPosition, earPosition)
            return@filter earKeyPoint.score > scoreThreshold && frameAnalysis.frameInfo.index < impactFrameIndex && (distance > driftThreshold)
        }.collect(Collectors.toList())

        return createEvents(headDriftEvents, "Your head moved at these times:")
    }

    fun gatherHeadDriftFrontFeedback(prescriptiveFeedbackData: PrescriptiveFeedbackData, impactFrameIndex: Int): String? {
        val (firstFrameAnalysisResult, videoAnalysisResult, bodyPosition, golfBallPositionAsPair) = prescriptiveFeedbackData

        val firstFramekeypoints = firstFrameAnalysisResult.scaledPoseEstimation.keyPoints
        val findInFirstFrame = findKeypoint(firstFramekeypoints);

        val initialNosePosition = findInFirstFrame(NOSE).position
        val driftThreshold = 1.0 / 6 * getBodyScale(prescriptiveFeedbackData)

        val headDriftEvents = videoAnalysisResult.stream().filter { frameAnalysis ->
            val noseKeyPoint = frameAnalysis.scaledPoseEstimation.keyPoints.find { keypoint -> keypoint.bodyPart == NOSE }!!
            val nosePosition = noseKeyPoint.position
            val distance = LineCheck.distance(initialNosePosition, nosePosition)
            return@filter noseKeyPoint.score > scoreThreshold && frameAnalysis.frameInfo.index < impactFrameIndex && (distance > driftThreshold)
        }.collect(Collectors.toList())

        return createEvents(headDriftEvents, "Your head moved at these times:")
    }

    fun gatherLegsFeedback(prescriptiveFeedbackData: PrescriptiveFeedbackData, impactFrameIndex: Int): String? {
        val (firstFrameAnalysisResult, videoAnalysisResult, bodyPosition, golfBallPositionAsPair) = prescriptiveFeedbackData

        val firstFramekeypoints = firstFrameAnalysisResult.scaledPoseEstimation.keyPoints
        val findInFirstFrame = findKeypoint(firstFramekeypoints);

        // TODO: Midpoint knee + ankle
        val hip = if (bodyPosition == BodyPosition.FRONT_RIGHT) RIGHT_HIP else LEFT_HIP
        val initialHipPosition = Position(findInFirstFrame(hip).position.x, 0)
        val driftThreshold = 1.0 / 8 * getBodyScale(prescriptiveFeedbackData)

        val hipDriftEvents = videoAnalysisResult.stream().filter { frameAnalysis ->
            val hipKeyPoint = frameAnalysis.scaledPoseEstimation.keyPoints.find { keypoint -> keypoint.bodyPart == hip }!!
            val hipPosition = Position(hipKeyPoint.position.x, 0)
            val distance = LineCheck.distance(initialHipPosition, hipPosition)
            return@filter hipKeyPoint.score > scoreThreshold && frameAnalysis.frameInfo.index < impactFrameIndex && (distance > driftThreshold)
        }.collect(Collectors.toList())

        return createEvents(hipDriftEvents, "Your legs moved at these times:")
    }

    @JvmField
    var prescriptiveFeedbackResult: List<String>? = null

    @JvmField
    var prescriptiveFeedbackPromise: CompletableFuture<List<String>>? = null

    @RequiresApi(Build.VERSION_CODES.P)
    @JvmStatic
    fun gatherPrescriptiveFeedback(context: Context, prescriptiveFeedbackData: PrescriptiveFeedbackData): List<String> {
        val startTime = System.nanoTime();
        val (firstFrameAnalysisResult, videoAnalysisResult, bodyPosition, golfBallPositionAsPair) = prescriptiveFeedbackData


        val keys = SevenKeyFrameService(context, videoAnalysisResult)
        val keyFrameIndices: List<Int> = SevenKeyFrameService.indices;
        val keyFrameBitmaps: List<Bitmap> = SevenKeyFrameService.frames
        Log.d("Impact frame time", keyFrameIndices[4].toString());

        val impactFrameIndex = keyFrameIndices[4]

        val prescriptiveFeedback: ArrayList<String?> = ArrayList()

        if (bodyPosition.isSideView) {
            prescriptiveFeedback.add(gatherSwingPlaneFeedback(prescriptiveFeedbackData, impactFrameIndex))
            prescriptiveFeedback.add(gatherRearPositionFeedback(prescriptiveFeedbackData, impactFrameIndex))
            prescriptiveFeedback.add(gatherBackAngleFeedback(prescriptiveFeedbackData, impactFrameIndex))
            prescriptiveFeedback.add(gatherHeadDriftSideFeedback(prescriptiveFeedbackData, impactFrameIndex))
        } else {
            prescriptiveFeedback.add(gatherHeadDriftFrontFeedback(prescriptiveFeedbackData, impactFrameIndex))
            prescriptiveFeedback.add(gatherLegsFeedback(prescriptiveFeedbackData, impactFrameIndex))
        }

        Log.d("PrescriptiveFeedbackService", "Time to process prescriptive feedback was " + (System.nanoTime() - startTime) / 1000000 + "ms")
        return prescriptiveFeedback.filterNotNull();
    }

//
//        this.prescriptiveFeedbackPromise = CompletableFuture.supplyAsync(Supplier<List<FrameAnalysisResult>> {
//            startTime = System.nanoTime();
//            return@Supplier FrameUtilities(context).processEntireVideo(videoPath);
//        }).thenApply { analysisResult ->
//            val time = (System.nanoTime() - startTime) / 1000000
//            Log.d("PrescriptiveFeedbackService", "Time to process prescriptive feedback was " + time + "ms")
//            return@thenApply analysisResult;
//        }
}