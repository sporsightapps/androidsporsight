package com.dev.sporsightmobile.services;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;

//import com.microsoft.azure.storage.CloudStorageAccount;
//import com.microsoft.azure.storage.blob.CloudBlobClient;
//import com.microsoft.azure.storage.blob.CloudBlobContainer;
//import com.microsoft.azure.storage.blob.CloudBlockBlob;
//import com.microsoft.azure.storage.blob.ListBlobItem;

import com.dev.sporsightmobile.activities.VideoGallery;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.ListBlobItem;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.List;

public class BlobStorageGetVideos extends AsyncTask {

    public Context context;

    public Activity activity;


    private static final String storageURL = "https://sporsightmobile.blob.core.windows.net/containerpublic  sporsightmobile";
    private static final String storageContainer = "video-storage";
    private static final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=sporsightmobile;AccountKey=/Je3FmjgOQ8rswYUAEMkXf1mIwB9twwyw5cZZkHLijiiSwSxwvVfftM6z/X1SX9KuS3O6mtzW6i3Gfhr2p9mNA==;EndpointSuffix=core.windows.net";

    public BlobStorageGetVideos(Activity parent) {
        this.activity = parent;
    }

    @Override
    protected Iterable<ListBlobItem> doInBackground(Object[] params) {
        try {
            SharedPreferences prefs = activity.getSharedPreferences("Sporsight", Context.MODE_PRIVATE);
            String userId = prefs.getString("userId", "");

            // Retrieve storage account from connection-string.
            CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.createCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.getContainerReference(storageContainer);

            // Create or overwrite the blob (with the name "example.jpeg") with contents from a local file.
//            CloudBlockBlob blob = container.getBlockBlobReference(videoTitle);
//            File source = new File(videoUri.toString());
//            blob.download(new FileInputStream(source), source.length());
            List<ListBlobItem> blobItems = new ArrayList<ListBlobItem>();

            container.listBlobs(userId).forEach(blob -> blobItems.add(blob));
            System.out.println("Success!");
            return blobItems;
        } catch (Exception e) {
            // Output the stack trace.
            System.out.println("Failed upload to Blob!");
            e.printStackTrace();
            return null;

        }
    }


}