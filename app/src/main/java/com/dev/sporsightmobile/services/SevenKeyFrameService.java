package com.dev.sporsightmobile.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.util.Pair;

import com.dev.sporsightmobile.fragments.ExoPlayerFragment;
import com.dev.sporsightmobile.model.FrameAnalysisResult;
import com.dev.sporsightmobile.utilities.PoseEstimationUtilities;

import org.json.JSONException;
import org.json.JSONObject;
import org.tensorflow.lite.examples.posenet.lib.KeyPoint;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class SevenKeyFrameService {

    public static ArrayList<Integer> indices;
    public static ArrayList<Bitmap> frames;

    private Context context;
    private List<FrameAnalysisResult> poseResults;

    @RequiresApi(api = Build.VERSION_CODES.P)
    public SevenKeyFrameService(Context context, List<FrameAnalysisResult> poseResults)
    {
            this.context = context;
            this.poseResults = poseResults;

            getFrames();
    }

    public JSONObject keyFrameParser(Context context, String path)
    {
        try
        {
            InputStream is = context.getAssets().open(path);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            JSONObject obj = new JSONObject(json);
            return obj;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public Pair angle_length(double [] p1, double [] p2)
    {
        double angle = Math.atan2(- (p2[0]) + (p1[0]), (p2[1]) - (p1[1])) * 180/Math.PI;
        double length = Math.hypot((p2[1]) - (p1[1]), - (p2[0]) + (p1[0]));

        return Pair.create(Math.round(angle), Math.round(length));
    }

    public ArrayList<Integer> matching(ArrayList<Pair> values, ArrayList<Pair> frame)
    {
        int angle_deviation = 30;
        int size_deviation = 1;

        ArrayList<Integer> devs = new ArrayList<>();

        // get anchors
        int templ_anchor = ((Long) values.get(0).second).intValue();
        int targ_anchor = ((Long) frame.get(0).second).intValue();

        // calculate angle and length deviations
        int n = values.size();
        for(int i = 0; i < n; i++)
        {
            Pair<Integer, Integer> angles = new Pair(((Long) values.get(i).first).intValue(), ((Long) frame.get(i).first).intValue());
            int diff_angle = Math.max(angles.first, angles.second) - Math.min(angles.first, angles.second);

            Pair<Integer, Integer> templ_size_pair = new Pair(((Long) values.get(i).second).intValue(), templ_anchor);
            int templ_size;

            try
            {
                templ_size = Math.abs( Math.min(templ_size_pair.first, templ_size_pair.second) / Math.max(templ_size_pair.first, templ_size_pair.second) );
            } catch (ArithmeticException e)
            {
                templ_size = 0;
            }

            Pair<Integer, Integer> tar_size_pair = new Pair(((Long) frame.get(i).second).intValue(), targ_anchor);
            int tar_size;

            try
            {
                tar_size = Math.abs( Math.min(tar_size_pair.first, tar_size_pair.second) / Math.max(tar_size_pair.first, tar_size_pair.second) );
            } catch (ArithmeticException e)
            {
                tar_size = 0;
            }

            if(diff_angle > angle_deviation)
            {
                devs.add(i);
            }
            else if(Math.max(tar_size, templ_size) - Math.min(tar_size, templ_size) > size_deviation)
            {
                devs.add(i);
            }
        }

        return devs;
    }

    public ArrayList<Integer> getMatches(JSONObject reference, ArrayList<ArrayList<Pair>> frames_values)
    {
        // list of potential frame numbers
        ArrayList<Integer> matches = new ArrayList<>();

        // array of body joint pairs (1st index: corresponding posenet body joint, 2nd index: corresponding posenet body joint)
        ArrayList<Pair> pairs = new ArrayList<>();
        pairs.add(new Pair(5,6));
        pairs.add(new Pair(5,7));
        pairs.add(new Pair(6,8));
        pairs.add(new Pair(7,9));
        pairs.add(new Pair(8,10));
        pairs.add(new Pair(11,12));
        pairs.add(new Pair(5,11));
        pairs.add(new Pair(6,12));
        pairs.add(new Pair(11,13));
        pairs.add(new Pair(12,14));
        pairs.add(new Pair(13,15));
        pairs.add(new Pair(14,16));

        // get a list of angle_length pairs for the reference's frame to compare
        ArrayList<Pair> values = new ArrayList<>();
        int m = pairs.size();
        for(int j = 0; j < m; j++)
        {

            int bodyjoint_1 = (int) pairs.get(j).first;
            int bodyjoint_2 = (int) pairs.get(j).second;

            double x1 = 0;
            double y1 = 0;
            try {
                x1 = reference.getJSONArray("keyPoints").getJSONObject(bodyjoint_1).getJSONObject("position").getInt("x");
                y1 = reference.getJSONArray("keyPoints").getJSONObject(bodyjoint_1).getJSONObject("position").getInt("y");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            double x2 = 0;
            double y2 = 0;
            try {
                x2 = reference.getJSONArray("keyPoints").getJSONObject(bodyjoint_2).getJSONObject("position").getInt("x");
                y2 = reference.getJSONArray("keyPoints").getJSONObject(bodyjoint_2).getJSONObject("position").getInt("y");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            double [] p1 = new double[2];
            p1[0] = x1;
            p1[1] = y1;

            double [] p2 = new double[2];
            p2[0] = x2;
            p2[1] = y2;

            Pair<Integer, Integer> pair = angle_length(p1, p2);
            values.add(pair);
        }

        // store the differences of angles/lengths from every frame to the reference
        int smallest_length = 10000; // number of angles never exceeds this amount
        ArrayList<ArrayList<Integer>> differences = new ArrayList<ArrayList<Integer>>();
        int len = frames_values.size();
        for(int i = 0; i < len; i++)
        {
            ArrayList<Integer> currentDiff = matching(values, frames_values.get(i));

            if(currentDiff.size() < smallest_length)
            {
                smallest_length = currentDiff.size();
            }

            differences.add(currentDiff);
        }

        // find the best match aka potentials
        int n = differences.size();
        for(int i = 0; i < n; i++)
        {
            ArrayList<Integer> currentDiff = differences.get(i);
            if(currentDiff.size() == smallest_length)
            {
                matches.add(i);
            }
        }

        return matches;
    }

    public ArrayList<Integer> runSevenKeyFrame(Context context, List<FrameAnalysisResult> analysisResult)
    {
        // arrays of referenced key frames
        ArrayList<JSONObject> references = new ArrayList<>();

        // references both frontal and side
        JSONObject f_address = keyFrameParser(context, "f_address.json");
        JSONObject f_takeaway = keyFrameParser(context, "f_takeaway.json");
        JSONObject f_backswing = keyFrameParser(context, "f_backswing.json");
        JSONObject f_downswing = keyFrameParser(context, "f_downswing.json");
        JSONObject f_impact = keyFrameParser(context, "f_impact.json");
        JSONObject f_follow = keyFrameParser(context, "f_follow.json");

        JSONObject s_address = keyFrameParser(context, "s_address.json");
        JSONObject s_takeaway = keyFrameParser(context, "s_takeaway.json");
        JSONObject s_backswing = keyFrameParser(context, "s_backswing.json");
        JSONObject s_downswing = keyFrameParser(context, "s_downswing.json");
        JSONObject s_impact = keyFrameParser(context, "s_impact.json");
        JSONObject s_follow = keyFrameParser(context, "s_follow.json");

        // flag to decide whether to use frontal or side references
        Boolean isFront = !PoseEstimationUtilities.isSide;

        // use appropriate references (0 - 6 indexed)
        if(isFront == true)
        {
            references.add(f_address);
            references.add(f_takeaway);
            references.add(f_backswing);
            references.add(f_downswing);
            references.add(f_impact);
            references.add(f_follow);
        }
        else
        {
            references.add(s_address);
            references.add(s_takeaway);
            references.add(s_backswing);
            references.add(s_downswing);
            references.add(s_impact);
            references.add(s_follow);
        }

        // array of body joint pairs (1st index: corresponding posenet body joint, 2nd index: corresponding posenet body joint)
        ArrayList<Pair> pairs = new ArrayList<>();
        pairs.add(new Pair(5,6));
        pairs.add(new Pair(5,7));
        pairs.add(new Pair(6,8));
        pairs.add(new Pair(7,9));
        pairs.add(new Pair(8,10));
        pairs.add(new Pair(11,12));
        pairs.add(new Pair(5,11));
        pairs.add(new Pair(6,12));
        pairs.add(new Pair(11,13));
        pairs.add(new Pair(12,14));
        pairs.add(new Pair(13,15));
        pairs.add(new Pair(14,16));

        // create array of frames
        List<FrameAnalysisResult> frames = analysisResult;

        // create an array of every frame's angle and length values
        int n = analysisResult.size();
        int m = pairs.size();
        ArrayList<ArrayList<Pair>> frames_values = new ArrayList<>();

        // analysis results per frame i
        for(int i = 0; i < n; i++)
        {
            ArrayList<Pair> pair_values = new ArrayList<>();

            // pairs index by j
            for(int j = 0; j < m; j++)
            {
                FrameAnalysisResult currentFrame = frames.get(i);
                List<KeyPoint> keyPoints = currentFrame.getScaledPoseEstimation().getKeyPoints();

                int bodyjoint_1 = (int) pairs.get(j).first;
                int bodyjoint_2 = (int) pairs.get(j).second;

                double x1 = keyPoints.get(bodyjoint_1).getPosition().getX();
                double y1 = keyPoints.get(bodyjoint_1).getPosition().getY();

                double [] p1 = new double[2];
                p1[0] = x1;
                p1[1] = y1;

                double x2 = keyPoints.get(bodyjoint_2).getPosition().getX();
                double y2 = keyPoints.get(bodyjoint_2).getPosition().getY();

                double [] p2 = new double[2];
                p2[0] = x2;
                p2[1] = y2;

                Pair<Integer, Integer> pair = angle_length(p1, p2);
                pair_values.add(pair);
            }

            frames_values.add(pair_values);
        }

        // get potential frames for each key frames
        ArrayList<Integer> address_matches   = getMatches(references.get(0), frames_values);
        ArrayList<Integer> takeaway_matches  = getMatches(references.get(1), frames_values);
        ArrayList<Integer> backswing_matches = getMatches(references.get(2), frames_values);
        ArrayList<Integer> downswing_matches = getMatches(references.get(3), frames_values);
        ArrayList<Integer> impact_matches    = getMatches(references.get(4), frames_values);
        ArrayList<Integer> follow_matches    = getMatches(references.get(5), frames_values);

        // these can be tweaked
        Integer address = address_matches.get(0);
        Integer impact = impact_matches.get(impact_matches.size() - 1);
        Integer backswing = backswing_matches.get(0);
        Integer takeaway = takeaway_matches.get(0);
        Integer downswing = downswing_matches.get(0);
        Integer follow = follow_matches.get(0);
        Integer extension = ((impact + follow) / 2) + 1;

        // get 7 key frames
        ArrayList<Integer> keyFrames = new ArrayList<>();
        keyFrames.add(address);
        keyFrames.add(takeaway);
        keyFrames.add(backswing);
        keyFrames.add(downswing);
        keyFrames.add(impact);
        keyFrames.add(extension);
        keyFrames.add(follow);

        // store frame indices for outside use
        indices = keyFrames;

        return keyFrames;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public ArrayList<Bitmap> getFrames()
    {
        // data structure to retrieve videos
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();

        // get uri of selected video
//        String uri = getIntent().getStringExtra("URI");
        String uri = ExoPlayerFragment.mVideoUri.toString();

        // retrieve video from uri source
        retriever.setDataSource(uri);

        // get duration of video in milliseconds
        int duration = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

        // get total amount of frames of video
        int framesCount = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_FRAME_COUNT));

        // find the amount of milliseconds per frame
        int time_per_frame = duration/framesCount;

        // keep an array of the 7 keyframes
        ArrayList<Bitmap> sequence = new ArrayList<>();

        // get the 7 key frame indices
        ArrayList<Integer> indices = runSevenKeyFrame(context, poseResults);

        // get 7 frames
        for(int i = 0; i < 7; i++)
        {
            sequence.add(retriever.getFrameAtIndex(indices.get(i)));
        }

        // store frames for outside use if needed
        frames = sequence;

        return sequence;
    }


}
