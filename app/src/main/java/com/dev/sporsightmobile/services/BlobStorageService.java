package com.dev.sporsightmobile.services;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

import com.dev.sporsightmobile.activities.BaseAnnotatorActivity;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.util.EnumSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BlobStorageService extends AsyncTask {

    public Context context;
    public String videoTitle;
    public Uri videoUri;

    private static final String storageURL = "https://sporsightmobile.blob.core.windows.net/containerpublic  sporsightmobile";
    private static final String storageContainer = "video-storage";
    private static final String storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=sporsightmobile;AccountKey=/Je3FmjgOQ8rswYUAEMkXf1mIwB9twwyw5cZZkHLijiiSwSxwvVfftM6z/X1SX9KuS3O6mtzW6i3Gfhr2p9mNA==;EndpointSuffix=core.windows.net";


    public interface AsyncResponse {
        void processFinish(Boolean output);
    }


    AsyncResponse asyncResponse = null;

    public BlobStorageService(AsyncResponse asyncResponse) {
        this.asyncResponse = asyncResponse;
    }


    @Override
    protected Object doInBackground(Object[] params) {
        try {
            // Retrieve storage account from connection-string.
            CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);

            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.createCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.getContainerReference(storageContainer);

            // Create or overwrite the blob (with the name "example.jpeg") with contents from a local file.
            CloudBlockBlob blob = container.getBlockBlobReference(videoTitle + ".mp4");
            File source = new File(videoUri.toString());
            blob.upload(new FileInputStream(source), source.length());
            System.out.println("Uploaded to Blob!");
        } catch (Exception e) {
            // Output the stack trace.
            System.out.println("Failed upload to Blob!");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);

        asyncResponse.processFinish(true);
    }

}
