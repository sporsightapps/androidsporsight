package com.dev.sporsightmobile.fragments;

import android.database.sqlite.SQLiteDatabase;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.utilities.PermissionsHelper;
import com.dev.sporsightmobile.entities.video.VideoDBHelper;
import com.dev.sporsightmobile.interfaces.OnFragmentInteractionListener;
import com.dev.sporsightmobile.views.VideosViewAdapter;


public class MyVideosFragment extends Fragment {

    private static final String TAG = MyVideosFragment.class.getSimpleName();

    VideosViewAdapter videosAdapter;
    OnFragmentInteractionListener mCallback;

    private RecyclerView recyclerView;
    VideoDBHelper dbHelper;
    private Intent mBaseAnnotatorActivity;
    private Uri mVideoUri;

    String category = "All";
    Boolean check;

    public static MyVideosFragment newInstance() {
        return new MyVideosFragment();
    }

    public interface ClickListener {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        dbHelper = new VideoDBHelper(getContext());
        SQLiteDatabase mDatabase = dbHelper.getWritableDatabase();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_videos, container, false);

        // setting up the categories dropdown bar
        check = false;
        String [] categories = getResources().getStringArray(R.array.categories);
        AppCompatSpinner dropdown = rootView.findViewById(R.id.categories);
        ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //SF - Switch the Video Adapter with the selected category
                    category = parent.getItemAtPosition(position).toString();

                    videosAdapter = new VideosViewAdapter(getContext(), category);
                    recyclerView.setAdapter(videosAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });
        dropdown.setAdapter(adapter);
        // end of setting up categories dropdown bar


        recyclerView = rootView.findViewById(R.id.my_videos_fragment_recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // this gets called to load the videos and display into the adapter
        // Abdool Shakur addition: adding a new parameter to include loading only certain types
        videosAdapter = new VideosViewAdapter(getContext(), category);
        recyclerView.setAdapter(videosAdapter);
//        recyclerView.swapAdapter(videosAdapter, true);

        setHasOptionsMenu(true);

        return rootView;
    }


//    PopupMenu.OnMenuItemClickListener videoMenuClickListener = new PopupMenu.OnMenuItemClickListener() {
//        @Override
//        public boolean onMenuItemClick(MenuItem item) {
//            return false;
//        }
//    };


    //    @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        MenuItem menuItem = menu.add("Search");
//        menuItem.setIcon(android.R.drawable.ic_menu_search);
//        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//
//        SearchView searchView = new SearchView(getActivity());
//        searchView.setOnQueryTextListener(this);
//        menuItem.setActionView(searchView);
//        super.onCreateOptionsMenu(menu, inflater);
//    }


//
//    @Override public void onListItemClick(ListView l, View v, int position, long id) {
//
//        // Notify the parent activity of selected item
//        Log.i(TAG, "Item clicked: " + id);
//        mCallback.onFragmentMessage(TAG,position);
//
//        // Set the item as checked to be highlighted when in two-pane updated_camera_fragment
//        //getListView().setItemChecked(position, true);
//
//    }


//
//
//    //=============== IMPLEMENTING SEARCHVIEW=================================================//
//    @Override public boolean onQueryTextSubmit(String query) {
//        return false;
//    }
//    @Override public boolean onQueryTextChange(String newText) {
//        mCurFilter = !isEmpty(newText) ? newText : null;
//        getLoaderManager().restartLoader(0, null, this);
//        return true;
//    }
//    //=============== IMPLEMENTING SEARCHVIEW=================================================//
//
//


    //== START ================= RECYCLER TOUCH LISTENER CLASS ========================================//
    public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;
        private OnFragmentInteractionListener onFragmentInteractionListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {

            this.clickListener = clickListener;
//            this.onFragmentInteractionListener = onFragmentInteractionListener;

            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }


                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());

                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildAdapterPosition(child));
//                        onFragmentInteractionListener.onFragmentMessage(TAG,);
                    }

                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());

            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildAdapterPosition(child));
            }
            return false;
        }


        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }


        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

    }
//== END ================= RECYCLER TOUCH LISTENER CLASS ========================================//


    //= START  ======== HANDLING THE LISTENER FOR FRAGMENT ATTACH/DETACH================================
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    //WAS FOR RELOADING ACTIVITY POSSIBLY REMOVE
    @Override
    public void onResume() {
        super.onResume();
        this.onCreate(null);
    }

//= END  ========== HANDLING THE LISTENER FOR FRAGMENT ATTACH/DETACH================================


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        Log.d(TAG, "<---------------------------------------------IN ONREQUESTPERMISSIONRESULT()");

        switch (requestCode) {

            case PermissionsHelper.REQUESTCODE_READ_STORAGE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Storage Permission Granted.");
                    Log.v(TAG, "ABOUT TO CALL -> dbhelper.loadvideosfromstorage()");
                    Toast.makeText(getActivity(), "Storage Permission Granted", Toast.LENGTH_SHORT).show();

                    dbHelper.loadVideosFromStorage(getContext());

                    //Testing for the myvideos reload after permission request.
                    //TODO: NEED TO FIX SO THAT THIS LOADS ONCE PERMISSIONS WERE GIVEN

//                    FragmentTransaction ft = getFragmentManager().beginTransaction();
//                    ft.detach(this).attach(this).commit();

                    //THIS DIDN'T WORK EITHER ^^^^

//                    onResume();
//                    onDetach();


//                    getActivity().recreate();


//
//                    try {
//                        getActivity().recreate();
//                    } catch (NullPointerException e){
//                        Log.d(TAG,e.toString());
//                    }

                } else {
                    Log.v(TAG, "Permission Denied");
                    //TODO: SOMETHING. DISABLE LOADFROMSTORAGE()?
                }
                return;

            }

        }


    }//endof OnRequestPermissionResult()


}