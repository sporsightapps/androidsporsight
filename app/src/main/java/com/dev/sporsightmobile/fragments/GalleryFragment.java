package com.dev.sporsightmobile.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.activities.BaseAnnotatorActivity;
import com.dev.sporsightmobile.activities.CompareVideosActivity;
import com.dev.sporsightmobile.activities.GalleryAdapter;
import com.dev.sporsightmobile.activities.VideoGallery;
import com.dev.sporsightmobile.services.BlobStorageDownloadVideo;
import com.dev.sporsightmobile.views.VideosViewAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GalleryFragment extends Fragment {

    RecyclerView recyclerView;
    GalleryAdapter galleryAdapter;
    List<String> videos;
    TextView galleryNumber;
    String category;
    View rootView;
    List<String> selectedVideos;


    private static final int MY_READ_PERMISSION_CODE = 101;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_gallery, container, false);


        Button select = (Button) rootView.findViewById(R.id.select);
        Button cancel = (Button) rootView.findViewById(R.id.cancel);
        Button compare = (Button) rootView.findViewById(R.id.compare);


        select.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                    selectedVideos = new ArrayList<>();
                    select.setSelected(true);
                    select.setVisibility(View.INVISIBLE);
                    cancel.setVisibility(View.VISIBLE);
                    compare.setVisibility(View.VISIBLE);
                    loadVideos(rootView, true);
            }
        });

        cancel.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                    select.setSelected(false);
                    cancel.setVisibility(View.INVISIBLE);
                    compare.setVisibility(View.INVISIBLE);
                    select.setVisibility(View.VISIBLE);
                    loadVideos(rootView, false);
            }
        });

        compare.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {

                    if(selectedVideos.size() == 2)
                    {
                        Intent intent = new Intent(getContext(), CompareVideosActivity.class);

                        Log.v("Video1", selectedVideos.get(0));
                        Log.v("Video2", selectedVideos.get(1));

                        intent.putExtra("Video1", selectedVideos.get(0));
                        intent.putExtra("Video2", selectedVideos.get(1));
                        getContext().startActivity(intent);

                    }
                    else
                    {
                        Toast.makeText(getContext(), "Please Select Two Videos", Toast.LENGTH_LONG).show();
                    }

            }
        });

        galleryNumber = rootView.findViewById(R.id.gallery_number);
        recyclerView = rootView.findViewById(R.id.recyclerview_gallery_images);

        if(ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{ Manifest.permission.READ_EXTERNAL_STORAGE} , MY_READ_PERMISSION_CODE);
        }
        else
        {
            loadVideos(rootView, false);
        }

        return rootView;

    }

    private void loadVideos(View rootView, boolean isSelected) {

        String [] categories = getResources().getStringArray(R.array.categories);
        AppCompatSpinner dropdown = rootView.findViewById(R.id.categories);
        ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //SF - Switch the Video Adapter with the selected category
                category = parent.getItemAtPosition(position).toString();
                Log.d("category", category);
                try {
                    videos = VideoGallery.listOfVideos(getActivity(), getContext(), category);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(videos.size() == 1)
                {
                    galleryNumber.setText(videos.size() + " Video");
                }
                else
                {
                    galleryNumber.setText(videos.size() + " Videos");
                }


                galleryAdapter = new GalleryAdapter(getContext(), videos, new GalleryAdapter.VideoListener() {
                    @Override
                    public void onVideoClick(String path, View itemView) {

                        Log.d("selected", String.valueOf(isSelected));
                        if(isSelected) {

                            ImageView checkbox = (ImageView) itemView.findViewById(R.id.checkbox);

                            if (checkbox.getVisibility() == View.VISIBLE) {
                                if(selectedVideos.get(0) == path)
                                {
                                    selectedVideos.remove(0);
                                }
                                else if(selectedVideos.get(1) == path)
                                {
                                    selectedVideos.remove(1);
                                }

                                checkbox.setVisibility(View.INVISIBLE);
                            } else {
                                if(selectedVideos.size() < 2)
                                {
                                    checkbox.setVisibility(View.VISIBLE);
                                    selectedVideos.add(path);
                                }
                            }


                        }
                        else{
                            Intent intent = new Intent(getContext(), BaseAnnotatorActivity.class);

                            if(path.contains("http"))
                            {
                                String filename = path.substring(path.lastIndexOf("/") + 1);
                                //If this is a cloud video, temporarily download it before proceeding.

                                File downloadedFile = new File(getActivity().getDataDir(), filename);

                                BlobStorageDownloadVideo task = new BlobStorageDownloadVideo(getActivity(), filename, downloadedFile);

                                Boolean success = false;
                                try {
                                    success = (Boolean) task.execute().get();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                if(success)
                                {
                                    Uri videoUri = Uri.fromFile(downloadedFile);

                                    intent.putExtra("EXTRA_URI", videoUri);
                                    intent.putExtra("EXTRA_STARTCODE", "Annotator");

                                    getActivity().finish();
                                    getContext().startActivity(intent);
                                }
                            }
                            else
                            {
                                Uri videoUri = Uri.parse(path);
                                intent.putExtra("EXTRA_URI", videoUri);
                                intent.putExtra("EXTRA_STARTCODE", "Annotator");
                                getContext().startActivity(intent);
                            }



                        }
                    }
                });

                recyclerView.setAdapter(galleryAdapter);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });
        dropdown.setAdapter(adapter);

        if(category == null)
        {
            category = "All";
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        try {
            videos = VideoGallery.listOfVideos(getActivity(), getContext(), category);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        galleryAdapter = new GalleryAdapter(this.getContext(), videos, new GalleryAdapter.VideoListener() {
            @Override
            public void onVideoClick(String path, View itemView) {

                Log.d("selected", String.valueOf(isSelected));
                if(isSelected) {

                    ImageView checkbox = (ImageView) itemView.findViewById(R.id.checkbox);

                    if (checkbox.getVisibility() == View.VISIBLE) {

                        if(selectedVideos.get(0) == path)
                        {
                            selectedVideos.remove(0);
                        }
                        else if(selectedVideos.get(1) == path)
                        {
                            selectedVideos.remove(1);
                        }

                        checkbox.setVisibility(View.INVISIBLE);
                    } else {
                        checkbox.setVisibility(View.VISIBLE);

                        selectedVideos.add(path);

                    }
                }
                else{
                    Intent intent = new Intent(getContext(), BaseAnnotatorActivity.class);
                    Uri videoUri = Uri.parse(path);
                    intent.putExtra("EXTRA_URI", videoUri);
                    intent.putExtra("EXTRA_STARTCODE", "Annotator");
                    getContext().startActivity(intent);
                }

            }
        });


        recyclerView.setAdapter(galleryAdapter);

        galleryNumber.setText(videos.size() + " Videos");
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == MY_READ_PERMISSION_CODE)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(getContext(), "Read External Storage Permission Granted", Toast.LENGTH_SHORT).show();
                loadVideos(rootView, false);
            }
            else
            {
                Toast.makeText(getContext(), "Read External Storage Permission Denied", Toast.LENGTH_SHORT).show();

            }
        }
    }

}
