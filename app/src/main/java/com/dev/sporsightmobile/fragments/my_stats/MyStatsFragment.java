/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dev.sporsightmobile.fragments.my_stats;

import androidx.fragment.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.interfaces.OnFragmentInteractionListener;


// MyStatsFragment currently not used

public class MyStatsFragment extends ListFragment {

    private static final String TAG = MyStatsFragment.class.getSimpleName();

    OnFragmentInteractionListener mListener;

    public static MyStatsFragment newInstance() { return new MyStatsFragment(); }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // We need to use a different list item updated_camera_fragment for devices older than Honeycomb
        int layout = android.R.layout.simple_list_item_activated_1;

        // Create an array adapter for the list view, using the Ipsum headlines array

        String[] stats_headlines = getResources().getStringArray(R.array.statistics_headlines);
        setListAdapter(new ArrayAdapter<>(getActivity(), layout, stats_headlines));
    }


    @Override
    public void onStart() {
        super.onStart();
        // When in two-pane updated_camera_fragment, set the listview to highlight the selected list item
        // (We do this during onStart because at the point the listview is available.)
        if (getFragmentManager().findFragmentById(R.id.my_stats_report_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }



    //TODO: FIX THIS?
    @SuppressWarnings("unchecked")
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Notify the parent activity of selected item
        mListener.onFragmentMessage(TAG,position);

        // Set the item as checked to be highlighted when in two-pane updated_camera_fragment
        getListView().setItemChecked(position, true);
    }



//TODO: CHECK IF THIS HELPS CLEAN UP - EASY RUN IT TEST
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }






}//endof MyStatsFragment class
