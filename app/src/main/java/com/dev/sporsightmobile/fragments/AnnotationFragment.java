package com.dev.sporsightmobile.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dev.sporsightmobile.activities.BaseAnnotatorActivity;
import com.dev.sporsightmobile.activities.FramesActivity;
import com.dev.sporsightmobile.activities.SilhouetteScreen;

import com.dev.sporsightmobile.model.FrameAnalysisResult;
import com.dev.sporsightmobile.services.VideoAnalysisService;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.core.content.ContextCompat;

import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.dialogs.StrokeSelectorDialog;
import com.dev.sporsightmobile.dialogs.colorpicker.ColorPickerDialog;
import com.dev.sporsightmobile.dialogs.colorpicker.ColorPickerSwatch;
import com.dev.sporsightmobile.interfaces.OnFragmentInteractionListener;
import com.dev.sporsightmobile.views.DrawingView;


import org.json.JSONArray;
import org.tensorflow.lite.examples.posenet.lib.Person;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class AnnotationFragment extends Fragment implements View.OnClickListener {
	private static final String TAG = AnnotationFragment.class.getSimpleName();

	//Request codes used for screen recording permission callback.
	private static final int REQUEST_CODE = 1000;
	private static final int REQUEST_PERMISSIONS = 10;

	private int mCurrentColor;
	private int mCurrentStroke;
	private static final int MAX_STROKE_WIDTH = 50;
	private FragmentActivity myContext;
	public static DrawingView mDrawingView;
	private LinearLayout mDrawingBarView;

	private Boolean isRecordingMedia;

	private ImageButton toSilhouetteScreen;
	// private ImageButton prescriptive_feedback;
	private static ArrayList<String> selectedJoints;
	private ImageButton viewTrajectory;
	private ImageButton runFrames;
	private ArrayList<Integer> x_coords_Joint0;
	private ArrayList<Integer> y_coords_Joint0;
	private ArrayList<Integer> x_coords_Joint1;
	private ArrayList<Integer> y_coords_Joint1;


	private ImageButton button_mic_on;
	private ImageButton button_mic_off;
	private ImageButton button_screenCapture;
	private ImageButton button_pose;


	//For Screen Recording Functionality
	private MediaProjection mMediaProjection;
	private MediaProjectionCallback mMediaProjectionCallback;
	private MediaProjectionManager mProjectionManager;
	private MediaRecorder mMediaRecorder;
	private VirtualDisplay mVirtualDisplay;
	private String mNextVideoAbsolutePath;


	ImageButton mButtonFreehand;
	ImageButton mButtonColorPallete;
	ImageButton mButtonGesture;
	ImageButton mButtonRedo;
	ImageButton mButtonUndo;
	ImageButton mButtonDelete;
	// ImageButton mButtonArrow;
	ImageButton mButtonSquare;
	ImageButton mButtonCircle;

	ImageButton mButtonStraightLine;

	ImageButton mFrames;

	LinearLayout linearLayout;
	public LinearLayout topLinearLayout;

	private OnFragmentInteractionListener mListener;

	//For Configuring the Screen Recording Functionality
	private int mScreenDensity;
	public static boolean mIsDrawingPose = false;
	private static final int DISPLAY_WIDTH = 720;
	private static final int DISPLAY_HEIGHT = 1280;


	private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

	static {
		ORIENTATIONS.append(Surface.ROTATION_0, 90);
		ORIENTATIONS.append(Surface.ROTATION_90, 0);
		ORIENTATIONS.append(Surface.ROTATION_180, 270);
		ORIENTATIONS.append(Surface.ROTATION_270, 180);
	}

	public static Person person;

	// An instance of JSONArray is created in order to allow for ease of use
	// of this variable in different activities and fragments
	public static JSONArray jsonArray;

	public static Integer seekToTime = -1;

	public AnnotationFragment() {
	}


	public static AnnotationFragment newInstance() {
		return new AnnotationFragment();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.v(TAG, "========================= IN ONCREATE OF ANNOTATION FRAGMENT");
		selectedJoints = new ArrayList<String>();

		isRecordingMedia = false;
		DisplayMetrics metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		mScreenDensity = metrics.densityDpi;
		mMediaRecorder = new MediaRecorder();
		mProjectionManager = (MediaProjectionManager) getActivity().getSystemService(Context.MEDIA_PROJECTION_SERVICE);

	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	@Override
	public void onResume() {
		super.onResume();
		if (seekToTime != -1) {
			Log.v(TAG, "seekToTime" + seekToTime);
			ExoPlayerFragment.exoPlayer.setPlayWhenReady(false);
			ExoPlayerFragment.exoPlayer.seekTo(Integer.toUnsignedLong(seekToTime));
			seekToTime = -1;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_annotation, container, false);
	}

	public static boolean waitingOnPose = false;

	//	private Player.EventListener poseEndListener = new Player.EventListener() {
//		@Override
//		public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//			Log.d("AnnotationFragment", "Playback state: " + playbackState);
//			if (playbackState == ExoPlayer.STATE_ENDED) {
//				ExoPlayerFragment.exoPlayer.setPlayWhenReady(false); // Stop automatically playing back
//
//				// Go to prescriptive feedback
//				Intent intent = new Intent(getContext(), PrescriptiveFeedback.class);
//				startActivity(intent);
//			}
//		}
//	};
	ImageButton trajectory;
	ImageButton frames;

	//	ImageButton sevenKeyFrames;
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		initializeUIview(view);
		initDrawingView();
		trajectory = (ImageButton) view.findViewById(R.id.viewTrajectory);
		frames = (ImageButton) view.findViewById(R.id.frames);
//		sevenKeyFrames = (ImageButton) view.findViewById(R.id.frames);
		VideoAnalysisService.analysisResult.thenAccept(analysisResult -> {
			trajectory.setImageResource(R.drawable.ic_trajectory_colored);
			frames.setImageResource(R.drawable.run_colored);
//			sevenKeyFrames.setImageResource(R.drawable.run);
		});
//		ExoPlayerFragment.exoPlayer.addListener(poseEndListener);
	}

//	@Override
//	public void onDestroyView() {
//		super.onDestroyView();
//		ExoPlayerFragment.exoPlayer.removeListener(poseEndListener);
//
//	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		myContext = (FragmentActivity) context;
		try {
			mListener = (OnFragmentInteractionListener) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString()
					+ " must implement OnFragmentInteractionListener");
		}

	}


	public void toggleDrawingBarView() {
		if (mDrawingBarView.getVisibility() == View.GONE)
			mDrawingBarView.setVisibility(View.VISIBLE);
		else
			mDrawingBarView.setVisibility(View.GONE);
	}


//
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }


	public void initializeUIview(View view) {
		mDrawingView = view.findViewById(R.id.annotation_drawingView);
		mDrawingBarView = view.findViewById(R.id.topbar_annotationFragment);

		mButtonFreehand = view.findViewById(R.id.drawing_freehand);
		mButtonColorPallete = view.findViewById(R.id.drawing_color_pallete);
		mButtonGesture = view.findViewById(R.id.drawing_gesture);
		//mButtonRedo = view.findViewById(R.id.drawing_redo);
		mButtonUndo = view.findViewById(R.id.drawing_undo);
		mButtonDelete = view.findViewById(R.id.drawing_delete);
		// mButtonArrow = view.findViewById(R.id.drawing_arrow);
		mButtonSquare = view.findViewById(R.id.drawing_rectangle);
		mButtonCircle = view.findViewById(R.id.drawing_circle);

		mButtonStraightLine = view.findViewById(R.id.drawing_straightline);

//        button_mic_on = view.findViewById(R.id.button_microphone_on);
//        button_mic_off = view.findViewById(R.id.button_microphone_off);
		button_screenCapture = view.findViewById(R.id.button_screenCapture);
		button_pose = view.findViewById(R.id.pose);
		// prescriptive_feedback = view.findViewById(R.id.prescriptive_feedback);
		toSilhouetteScreen = view.findViewById(R.id.toSilhouetteScreenButton);
		viewTrajectory = view.findViewById(R.id.viewTrajectory);
		runFrames = view.findViewById(R.id.frames);
		linearLayout = view.findViewById(R.id.topbar_annotationFragment);


		viewTrajectory.setOnClickListener(this);
		runFrames.setOnClickListener(this);
		toSilhouetteScreen.setOnClickListener(this);
		// prescriptive_feedback.setOnClickListener(this);
		mButtonFreehand.setOnClickListener(this);
		mButtonColorPallete.setOnClickListener(this);
		mButtonGesture.setOnClickListener(this);
		// mButtonRedo.setOnClickListener(this);
		mButtonUndo.setOnClickListener(this);
		mButtonDelete.setOnClickListener(this);
		// mButtonArrow.setOnClickListener(this);
		mButtonSquare.setOnClickListener(this);
		mButtonCircle.setOnClickListener(this);

//        button_mic_on.setOnClickListener(this);
//        button_mic_off.setOnClickListener(this);
		button_screenCapture.setOnClickListener(this);
		button_pose.setOnClickListener(this);

		mButtonStraightLine.setOnClickListener(this);

	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	@Override
	public void onClick(View view) {
		int input = view.getId();

		//Freehand Drawing tool.
		if (input == R.id.drawing_freehand) {
			Toast.makeText(getContext(), "Drawing FreeHand", Toast.LENGTH_SHORT).show();
			mDrawingView.setDrawingType("FreeHand");
			setUIColors();
		}

		// Allows the user to choose a different color for their path.
		else if (input == R.id.drawing_color_pallete) {
			startColorPickerDialog();
		}

		// Allows the user to choose the size of the stroke for drawing lines/shapes.
		else if (input == R.id.drawing_gesture) {
			startStrokeSelectorDialog();
		}

		// Undos the previous Path that the user has added.
		else if (input == R.id.drawing_undo) {
			mDrawingView.undo();
		}

		// Redos the most recent path which has been removed with undo functionality.
//        if(input ==  R.id.drawing_redo){
//            mDrawingView.redo();
//        }

		else if (input == R.id.drawing_straightline) {
			mDrawingView.setDrawingType("StraightLine");
			setUIColors();
		}

		// Clears the canvas by removing all current Paths.
		else if (input == R.id.drawing_delete) {
			mDrawingView.clearCanvas();
		}

		//Arrow Drawing tool.
//        if(input == R.id.drawing_arrow){
//            mDrawingView.setDrawingType("Arrow");
//            setUIColors();
//        }

		//Rectangle Drawing tool.
		else if (input == R.id.drawing_rectangle) {
			mDrawingView.setDrawingType("Rectangle");
			setUIColors();
		}

		//Circle Drawing tool.
		else if (input == R.id.drawing_circle) {
			mDrawingView.setDrawingType("Circle");
			setUIColors();
		}

		// 7 key frames.
		else if (input == R.id.frames){
			if (mDrawingView.analysisResult == null)
			{
				Toast.makeText(getActivity(), "The feature's resources are still loading, please wait", Toast.LENGTH_SHORT).show();
			}
			else
			{
				Intent intent = new Intent(getContext(), FramesActivity.class);
				intent.putExtra("URI", ExoPlayerFragment.mVideoUri.toString());
				startActivity(intent);
			}
		}

//        if (input == R.id.button_microphone_on) {
//            Log.v(TAG, "Microphone On Button Pressed (Turning Microphone Off) ", null);
//            Toast.makeText(getContext(), "Mic Off", Toast.LENGTH_SHORT).show();
//
//            //TODO: CALL THE MICROPHONE TOGGLE FUNCTION
//            button_mic_on.setVisibility(View.GONE);
//            button_mic_off.setVisibility(View.VISIBLE);
//            //TODO: SHOULD HAVE USED button_microphone_off.setEnabled(false);
//
//            setMicMuted(true);
//        }
//
//        if (input == R.id.button_microphone_off) {
//            Log.v(TAG, "Microphone Off Button Pressed (Turning Microphone On) ", null);
//            Toast.makeText(getContext(), "Mic On", Toast.LENGTH_SHORT).show();
//            //TODO: CALL THE MICROPHONE TOGGLE FUNCTION
//            button_mic_off.setVisibility(View.GONE);
//            button_mic_on.setVisibility(View.VISIBLE);
//
//            setMicMuted(false);
//        }


		else if (input == R.id.button_screenCapture) {
			if (ContextCompat.checkSelfPermission(getContext(),
					Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

				if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
						|| ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.RECORD_AUDIO)) {

					isRecordingMedia = false;
					button_screenCapture.setColorFilter(Color.parseColor("Red"));

					Snackbar.make(getView().findViewById(android.R.id.content), R.string.label_permissions, Snackbar.LENGTH_INDEFINITE)
							.setAction("ENABLE",
									new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											ActivityCompat.requestPermissions(getActivity(),
													new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, REQUEST_PERMISSIONS);
										}
									}).show();
				} else {
					// Permission was denied.
					ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO}, REQUEST_PERMISSIONS);
				}
			} else {
				onToggleScreenShare();
			}
		}

		// If the pose button on the far left is clicked the R.id value passed in will be equal to R.id.pose
		else if (input == R.id.pose) {
			setUIColors(true);
			if (mDrawingView.mIsDrawingPose) {
				mDrawingView.mIsDrawingPose = false;
				mDrawingView.invalidate();
			} else {
				mDrawingView.waitingForSwingPlaneInput = true;
				Toast.makeText(myContext, "Please move video to beginning of the swing, and tap the head of the golf club", Toast.LENGTH_LONG).show();
			}
		}

		// Open the prescriptive feedback button
		// else if (input == R.id.prescriptive_feedback){
		//     if(mDrawingView.analysisResult == null){
		//         Toast.makeText(getActivity(), "The feature's resources are still loading, please wait.", Toast.LENGTH_SHORT).show();
		//     } else {
		//         Intent intent = new Intent(view.getContext(), PrescriptiveFeedback.class);
		//         startActivity(intent);
		//     }
		// }

		// Open the silhouette screen
		else if (input == R.id.toSilhouetteScreenButton) {
			Intent intent = new Intent(view.getContext(), SilhouetteScreen.class);
			startActivity(intent);
		} else if (input == R.id.viewTrajectory) {
			if (mDrawingView.analysisResult == null) {
				Toast.makeText(getActivity(), "The feature's resources are still loading, please wait.", Toast.LENGTH_SHORT).show();
			} else {
				ExoPlayerFragment exoPlayerFragment = new ExoPlayerFragment();
				ArrayList<String> jointsJSON;
				jointsJSON = makeDataJSONReadable();
				Activity parent = this.getActivity();
				LinearLayout toolBarTop = (LinearLayout) parent.findViewById(R.id.annotation_positionToggle);

				if (selectedJoints.isEmpty()) {
					Toast.makeText(getActivity(), "Select which joints to map in Trajectory Settings first.", Toast.LENGTH_SHORT).show();
				} else {
					// Change button's UI.
					if (BaseAnnotatorActivity.trajectorySelected == false) {
//						trajectory.setImageResource(R.drawable.ic_clear_white_24dp);
						linearLayout.setVisibility(View.INVISIBLE);
						toolBarTop.setVisibility(View.INVISIBLE);
//						toSilhouetteScreen.setVisibility(View.INVISIBLE);
						mDrawingView.selectedJoints = jointsJSON;
						mDrawingView.setDrawingType("Trajectory");
						setUIColors();
						BaseAnnotatorActivity.trajectorySelected = true;
					} else {
						trajectory.setImageResource(R.drawable.ic_trajectory_white);
						linearLayout.setVisibility(View.VISIBLE);
//						toSilhouetteScreen.setVisibility(View.VISIBLE);
						toolBarTop.setVisibility(View.VISIBLE);
						mDrawingView.setDrawingType("Clear");
						BaseAnnotatorActivity.trajectorySelected = false;
					}
				}
			}
		}
	}


	private ArrayList<String> makeDataJSONReadable() {

		ArrayList<String> returnValue = new ArrayList<String>();

		if (selectedJoints.isEmpty()) {
			Log.e("SelectedJoints ERROR", "SelectedJoints is empty, cannot complete method: makeDataJSONReadable.");
			return returnValue;
		}

		for (int i = 0; i < selectedJoints.size(); i++) {
			switch (selectedJoints.get(i)) {
				case "R Head":
					returnValue.add("RIGHT_EAR");
					break;
				case "L Head":
					returnValue.add("LEFT_EAR");
					break;
				case "R Shoulder":
					returnValue.add("RIGHT_SHOULDER");
					break;
				case "L Shoulder":
					returnValue.add("LEFT_SHOULDER");
					break;
				case "R Elbow":
					returnValue.add("RIGHT_ELBOW");
					break;
				case "L Elbow":
					returnValue.add("LEFT_ELBOW");
					break;
				case "R Wrist":
					returnValue.add("RIGHT_WRIST");
					break;
				case "L Wrist":
					returnValue.add("LEFT_WRIST");
					break;
				case "R Hip":
					returnValue.add("RIGHT_HIP");
					break;
				case "L Hip":
					returnValue.add("LEFT_HIP");
					break;
				case "R Knee":
					returnValue.add("RIGHT_KNEE");
					break;
				case "R Ankle":
					returnValue.add("RIGHT_ANKLE");
					break;
				case "L Ankle":
					returnValue.add("LEFT_ANKLE");
					break;
			}

		}
		return returnValue;
	}

	public void setUIColors() {
		setUIColors(false);
	}

	public void setUIColors(Boolean clear) {
		mButtonFreehand.clearColorFilter();
		//mButtonArrow.clearColorFilter();
		mButtonSquare.clearColorFilter();
		mButtonCircle.clearColorFilter();
		mButtonStraightLine.clearColorFilter();

		String drawingType = mDrawingView.getDrawingType();

		if (!clear) {
			// if(drawingType.equals("Arrow"))mButtonArrow.setColorFilter(mCurrentColor);
			if (drawingType.equals("Circle")) mButtonCircle.setColorFilter(mCurrentColor);
			if (drawingType.equals("Rectangle")) mButtonSquare.setColorFilter(mCurrentColor);
			if (drawingType.equals("FreeHand")) mButtonFreehand.setColorFilter(mCurrentColor);
			if (drawingType.equals("StraightLine"))
				mButtonStraightLine.setColorFilter(mCurrentColor);
		}
	}


	private void initDrawingView() {
		mCurrentColor = ContextCompat.getColor(getContext(), android.R.color.holo_red_light);
		mCurrentStroke = 10;

		mDrawingView.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.transparent));
		mDrawingView.setPaintColor(mCurrentColor);
		mDrawingView.setPaintStrokeWidth(mCurrentStroke);

	}


	private void startColorPickerDialog() {
		int[] colors = getResources().getIntArray(R.array.pallete);

		ColorPickerDialog dialog = ColorPickerDialog.newInstance(R.string.color_picker_default_title,
				colors,
				mCurrentColor,
				5,
				ColorPickerDialog.SIZE_SMALL);

		dialog.setOnColorSelectedListener(new ColorPickerSwatch.OnColorSelectedListener() {
			@Override
			public void onColorSelected(int color) {
				mCurrentColor = color;
				mDrawingView.setPaintColor(mCurrentColor);
				setUIColors();


			}
		});

		dialog.show(getFragmentManager(), "ColorPickerDialog");
	}


	private void startStrokeSelectorDialog() {
		StrokeSelectorDialog dialog = StrokeSelectorDialog.newInstance(mCurrentStroke, MAX_STROKE_WIDTH);
		dialog.setOnStrokeSelectedListener(new StrokeSelectorDialog.OnStrokeSelectedListener() {
			@Override
			public void onStrokeSelected(int stroke) {
				mCurrentStroke = stroke;
				mDrawingView.setPaintStrokeWidth(mCurrentStroke);
			}
		});
		dialog.show(myContext.getSupportFragmentManager(), "StrokeSelectorDialog");
	}


	//============================================================
	//              SCREEN CAPTURE FUNCTIONS BELOW
	//============================================================

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode != REQUEST_CODE) {
			Log.e(TAG, "Unknown request code: " + requestCode);
			return;
		}

		if (resultCode != Activity.RESULT_OK) {
			Toast.makeText(getContext(), "Screen Cast Permission Denied", Toast.LENGTH_SHORT).show();
			Log.v(TAG, "Screen Cast Permission Denied.");
			isRecordingMedia = false;
			button_screenCapture.setColorFilter(Color.parseColor("Red"));
			return;
		}


		mMediaProjectionCallback = new MediaProjectionCallback();
		mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
		mMediaProjection.registerCallback(mMediaProjectionCallback, null);
		mVirtualDisplay = createVirtualDisplay();

		mMediaRecorder.start();


	}


	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
		stopScreenSharing();
		mMediaRecorder.reset();
		mMediaRecorder = null;
		//TODO: Possibly add more clean up into here?
	}


	private class MediaProjectionCallback extends MediaProjection.Callback {
		@Override
		public void onStop() {
			if (isRecordingMedia) {
				onToggleScreenShare();
				mMediaRecorder.reset();
				Log.v(TAG, "Recording Stopped");
			}
			mMediaProjection = null;
			stopScreenSharing();
		}
	}


	private void stopScreenSharing() {
		if (mVirtualDisplay == null) {
			return;
		}
		mVirtualDisplay.release();
		destroyMediaProjection();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		destroyMediaProjection();
	}


	private static File getOutputMediaFile() {
		//TODO: EDIT THIS TO MAKE THE OUTPUT VIDEO BE CREATED FROM INPUT TITLE + "ANNOTATED"
		File mediaStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "SporSight");
		File storageDirectory = new File(mediaStorageDirectory, "Annotations");
		if (!storageDirectory.exists()) {
			if (!storageDirectory.mkdirs()) {
				Log.d(TAG, "failed to create directory");
			}
		}
		String timeStamp = new SimpleDateFormat("MM_dd_yyyy'_'HH_mm").format(new Date());
		return new File(storageDirectory.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
	}


	public void onToggleScreenShare() {

		if (isRecordingMedia) {
			Log.v(TAG, "Stopping ScreenShare");
			mMediaRecorder.reset();
			stopScreenSharing();
			isRecordingMedia = false;
			button_screenCapture.setColorFilter(Color.parseColor("Red"));
			Toast.makeText(getContext(), "Saved.", Toast.LENGTH_SHORT).show();
		} else {
			Log.v(TAG, "Starting ScreenShare");
			initRecorder();
			shareScreen();
			isRecordingMedia = true;
			button_screenCapture.setColorFilter(Color.parseColor("Grey"));
		}

	}


	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

		switch (requestCode) {
			case REQUEST_PERMISSIONS: {
				if ((grantResults.length > 0) && (grantResults[0] +
						grantResults[1]) == PackageManager.PERMISSION_GRANTED) {
					onToggleScreenShare();
				} else {
					isRecordingMedia = false;

					Snackbar.make(getView().findViewById(android.R.id.content), R.string.label_permissions,
							Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
							new View.OnClickListener() {


								//TODO: FIGURE OUT IF THIS IS RELOADING THE CURRENT ACTIVITY; IF IT CAN BE MODIFIED??
								@Override
								public void onClick(View v) {
									Intent intent = new Intent();

									intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);//Activity Action: Show screen of details about a particular application.
									intent.addCategory(Intent.CATEGORY_DEFAULT);
									intent.setData(Uri.parse("package:" + getActivity().getPackageName()));
									intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);//If set, the new activity is not kept in the history stack.
									intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);//If set, the new activity is not kept in the list of recently launched activities.
									startActivity(intent);
								}
							}).show();


				}

			}
		}
	}//endof onRequestPermissionsResult


	private void initRecorder() {
		try {


			mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
			mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

			mNextVideoAbsolutePath = getOutputMediaFile().toString();
			mMediaRecorder.setOutputFile(mNextVideoAbsolutePath);

			mMediaRecorder.setVideoSize(DISPLAY_WIDTH, DISPLAY_HEIGHT);
			mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
			mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

			mMediaRecorder.setVideoEncodingBitRate(512 * 1000);
			mMediaRecorder.setVideoFrameRate(60);


			int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
//            int orientation = ORIENTATIONS.get(rotation + 90);
			mMediaRecorder.setOrientationHint(rotation);


			mMediaRecorder.prepare();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void shareScreen() {
		if (mMediaProjection == null) {
			startActivityForResult(mProjectionManager.createScreenCaptureIntent(), REQUEST_CODE);
			return;
		}
		mVirtualDisplay = createVirtualDisplay();
		mMediaRecorder.start();
	}


	private VirtualDisplay createVirtualDisplay() {
		return mMediaProjection.createVirtualDisplay("AnnotationFragment",
				DISPLAY_WIDTH, DISPLAY_HEIGHT, mScreenDensity,
				DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
				mMediaRecorder.getSurface(), null, null);
	}


	private void destroyMediaProjection() {
		if (mMediaProjection != null) {
			mMediaProjection.unregisterCallback(mMediaProjectionCallback);
			mMediaProjection.stop();
			mMediaProjection = null;
		}
		Log.v(TAG, "MediaProjection Stopped");
	}


//    private void setMicMuted(boolean state) {
//
//        AudioManager mAudioManager = (AudioManager) myContext.getSystemService(Context.AUDIO_SERVICE);
//
//        // get the working mode and keep it
//        int workingAudioMode = mAudioManager.getMode();
//
//        mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
//
//        // change mic state only if needed
//        if (mAudioManager.isMicrophoneMute() != state) {
//            mAudioManager.setMicrophoneMute(state);
//        }
//
//
//        // set back the original working mode
//        mAudioManager.setMode(workingAudioMode);
//
//    }

	public void setSelectedJoints(ArrayList<String> silhouetteSelectedJoints) {
		this.selectedJoints = silhouetteSelectedJoints;
		Log.d("setter", "SelectedJoints = " + Arrays.toString(selectedJoints.toArray()));
	}

	public ArrayList<String> getSelectedJoints() {
		return selectedJoints;
	}

}
