package com.dev.sporsightmobile.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;


import com.dev.sporsightmobile.activities.BaseAnnotatorActivity;
import android.widget.Toast;

import com.dev.sporsightmobile.activities.BaseHomePageActivity;
import com.dev.sporsightmobile.activities.FramesActivity;
import com.dev.sporsightmobile.activities.PrescriptiveFeedback;
import com.dev.sporsightmobile.model.FrameAnalysisResult;
import com.dev.sporsightmobile.services.PrescriptiveFeedbackService;
import com.dev.sporsightmobile.services.VideoAnalysisService;
import com.dev.sporsightmobile.views.DrawingView;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import com.dev.sporsightmobile.R;
import com.gowtham.library.utils.TrimVideo;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;


public class ExoPlayerFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = ExoPlayerFragment.class.getSimpleName();


    private OnFragmentInteractionListener mListener;


    public static ExoPlayer exoPlayer;
    private PlayerView playerView;
    private PlayerControlView controls;
    private Boolean playWhenReady;
    private long playbackPosition;
    private int currentWindow;
    public static Uri mVideoUri;
    public static long totalDuration;
    public static long currentPosition;

    public static DrawingView mDrawingView;


    public static final String VIDEO_URI = "VIDEO_URI";


    public ExoPlayerFragment() {
    }


    public static ExoPlayerFragment newInstance(Uri videoUri) {
        Log.v(TAG, "New Instance with video Uri= " + videoUri.toString());

        ExoPlayerFragment fragment = new ExoPlayerFragment();


        Bundle args = new Bundle();
        args.putParcelable(VIDEO_URI, videoUri);

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle args = getArguments();
        if (args != null) {
            mVideoUri = args.getParcelable(VIDEO_URI);


        }



    }

    List<FrameAnalysisResult> poseResults;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v = inflater.inflate(R.layout.fragment_exo_player, container, false);

        initView(v);

        VideoAnalysisService.analysisResult.thenAccept(analysisResult -> {
            this.poseResults = analysisResult;
        });

//        poseButton = v.findViewById(R.id.pose);
//        poseButton.setOnClickListener(this);

        Bundle bundle = getArguments();
        mVideoUri = bundle.getParcelable(VIDEO_URI);



        initializeExoPlayer();
        startExoPlayer();

        return v;
    }

    private void initView(View v) {
        playerView = v.findViewById(R.id.fragment_exoplayer_view);
        controls = v.findViewById(R.id.controls);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
        //      playerView.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

//        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
//        exoPlayer.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);

        ImageButton share = (ImageButton)v.findViewById(R.id.share);
        ImageButton trim = (ImageButton)v.findViewById(R.id.trim);

        if(mVideoUri.toString().contains("file:"))
        {
            trim.setVisibility(View.INVISIBLE);
        }

      //  ImageButton back = (ImageButton)v.findViewById(R.id.backButton);
        // Abdool additions:
//        ImageButton frames = (ImageButton)v.findViewById(R.id.frames);

//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent mBaseAnnotatorActivity = new Intent(getContext(), BaseHomePageActivity.class);
//                mBaseAnnotatorActivity.putExtra("Tab", "Gallery");
//                getContext().startActivity(mBaseAnnotatorActivity);
//               // mBaseAnnotatorActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
////                getFragmentManager().beginTransaction()
////                        .replace(R.id.homepage_fragment_container, new GalleryFragment(), "GalleryFragment")
////                        .commit();
////                Fragment startingFragment = new GalleryFragment();
////                loadFragment(startingFragment, GalleryFragment.class.getSimpleName());
//
//            }
//        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, mVideoUri);
                shareIntent.setType("video/*");


                //TODO: IS THIS THE CORRECT WAY TO CALL IT???
                getContext().startActivity(Intent.createChooser(shareIntent, getContext().getResources().getText(R.string.send_to)));

            }
        });

        trim.setOnClickListener(new View.OnClickListener() {

            File path = Environment.getExternalStoragePublicDirectory((Environment.DIRECTORY_DCIM) + "/Camera");
            File mediaStorageDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"SporSight");
            File storageDirectory = new File(mediaStorageDirectory, "Recordings");


         //       String timeStamp = new SimpleDateFormat("MM_dd_yy'_'HH_mm_ss",new Locale("en")).format(new Date());

         //   File file =  new File(storageDirectory.getPath() + File.separator + "VID_" + timeStamp + ".mp4");


            @Override
            public void onClick(View v) {
                Log.d("uri", mVideoUri.toString());
                TrimVideo.activity(Uri.fromFile(new File(mVideoUri.toString())).toString())
                //        .setCompressOption(new CompressOption()) //empty constructor for default compress option
                        .setDestination(storageDirectory.getAbsolutePath())  //default output path /storage/emulated/0/DOWNLOADS
                        .setAccurateCut(true)
                        .start(getActivity()
                        );
            }
        });

        // Abdool's Addition:
//        frames.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (poseResults == null) {
//                    Toast.makeText(getActivity(), "The feature's resources are still loading, please wait.", Toast.LENGTH_SHORT).show();
//                }
//                else {
//                    Intent intent = new Intent(getContext(), FramesActivity.class);
//                    intent.putExtra("URI", mVideoUri.toString());
//                    startActivity(intent);
//                }
//            }
//        });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == TrimVideo.VIDEO_TRIMMER_REQ_CODE && data != null) {
            Uri uri = Uri.parse(TrimVideo.getTrimmedVideoPath(data));
            Log.d(TAG,"Trimmed path:: "+uri);

            Intent intent = new Intent(getContext(), BaseAnnotatorActivity.class);
            Uri videoUri = Uri.parse(uri.getPath());
            intent.putExtra("EXTRA_URI", videoUri);
            intent.putExtra("EXTRA_STARTCODE", "Annotator");
            getContext().startActivity(intent);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        initializeExoPlayer();
    }


    @Override
    public void onResume() {
        super.onResume();
        initializeExoPlayer();
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseExoPlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseExoPlayer();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        releaseExoPlayer();
    }

    private void loadFragment(Fragment fragment, String fragmentTAG) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.homepage_fragment_container, fragment, fragmentTAG);
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }



    //= START  ======== HANDLING THE LISTENER FOR FRAGMENT ATTACH/DETACH================================
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
//        }
//    }
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }


//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }


    private void releaseExoPlayer() {
        if (exoPlayer != null) {
            playbackPosition = exoPlayer.getCurrentPosition();
            currentWindow = exoPlayer.getCurrentWindowIndex();
            playWhenReady = exoPlayer.getPlayWhenReady();
            exoPlayer.release();
            exoPlayer = null;
        }
    }


    private void startExoPlayer() {
        playWhenReady = true;
        exoPlayer.setPlayWhenReady(false);
    }
    ImageButton sevenKeyFrames;
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        sevenKeyFrames = (ImageButton) view.findViewById(R.id.frames);
//        VideoAnalysisService.analysisResult.thenAccept(analysisResult -> {
//            sevenKeyFrames.setImageResource(R.drawable.run);
//        });
    }

    private void initializeExoPlayer() {
        if (exoPlayer == null) {

            //Always show Portrait Mode
            ((Activity) this.getContext()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);

            Log.v(TAG, "Initializing ExoPlayer");
            exoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), new DefaultTrackSelector());
            playerView.setPlayer(exoPlayer);
            controls.setPlayer(exoPlayer);

            playerView.setRotation(0);

            playerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("GET", "clicked!");
                    if(controls.getVisibility() == View.VISIBLE)
                    {
                        controls.setVisibility(View.GONE);
                    }
                    else
                    {
                        controls.setVisibility(View.VISIBLE);
                    }
                }
            } );

            //Setting up the Seek Parameters to use the default seek configuration
//            SeekParameters seekParameters = new SeekParameters(null,null);
            exoPlayer.setSeekParameters(null);


            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), "Sporsight"));
            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(mVideoUri);
            exoPlayer.prepare(videoSource);
            playWhenReady = true;
            exoPlayer.setPlayWhenReady(playWhenReady);
            exoPlayer.seekTo(currentWindow, playbackPosition);


            exoPlayer.addListener(new Player.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    // Each time we scroll through a video, we get the current position we are in in milliseconds
                    // and we set it to this.currentPosition
                    // Likewise we always retrieve the total duration because there are multiple instances where we need it
                    if (exoPlayer != null ) {
                        setCurrentPosition(exoPlayer.getCurrentPosition());
                        setTotalDuration(exoPlayer.getDuration());
                    }
                    if (playbackState == ExoPlayer.STATE_ENDED) {
                        ExoPlayerFragment.exoPlayer.setPlayWhenReady(false); // Stop automatically playing back
                    }
                    if (AnnotationFragment.waitingOnPose && playbackState == ExoPlayer.STATE_ENDED) {
                        AnnotationFragment.waitingOnPose = false;
                        ExoPlayerFragment.exoPlayer.setPlayWhenReady(false); // Stop automatically playing back

                        VideoAnalysisService.analysisResult.thenAccept(result -> {
                            PrescriptiveFeedbackService.INSTANCE.prescriptiveFeedbackPromise.thenAccept(prescriptiveFeedbackData -> {
                                if (!BaseAnnotatorActivity.trajectorySelected) {
                                    Intent intent = new Intent(getContext(), PrescriptiveFeedback.class);
                                    startActivity(intent);
                                }
                            });
                        });
                    }
                }
            });

            //To get the screen size
            WindowManager wm = (WindowManager) getContext().getSystemService(getContext().WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            int height = size.y;




        }
    }

    public void setCurrentPosition(long currentPosition) {
        this.currentPosition = currentPosition;
    }

    public long getCurrentPosition() {
        return this.currentPosition;
    }

    public void setTotalDuration(long totalDuration) {
        this.totalDuration = totalDuration;
    }

    public long getTotalDuration() {
        return this.totalDuration;
    }

    @Override
    public void onClick(View v) {
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



}//endof ExoPlayerFragment.java

