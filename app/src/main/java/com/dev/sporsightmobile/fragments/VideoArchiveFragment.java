package com.dev.sporsightmobile.fragments;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.dev.sporsightmobile.R;
import com.dev.sporsightmobile.entities.video.MediaObject;
import com.dev.sporsightmobile.entities.video.VerticalSpacingItemDecorator;
import com.dev.sporsightmobile.entities.video.VideoPlayerRecyclerAdapter;
import com.dev.sporsightmobile.entities.video.VideoPlayerRecyclerView;
import com.dev.sporsightmobile.Retrofit.Blob;
import com.dev.sporsightmobile.Retrofit.Blobs;
import com.dev.sporsightmobile.Retrofit.RetrofitControllerBlob;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoArchiveFragment extends Fragment {


    private VideoPlayerRecyclerView mRecyclerView;
    int counter = 0, counterTwo = 0;

    // The default thumbnail for videos is a black background
    private static String thumbnail = "http://primusdatabase.com/images/thumb/8/86/Black-background.png/1200px-Black-background.png";

    // In order to retrieve the blobs for viewing, we must have the base URL for where all of our blobs are located
    public String mediaURL = "https://sporsightmobile.blob.core.windows.net/openpose/";

    // TODO: Rename and change types and number of parameters
    public static VideoArchiveFragment newInstance() {
        return new VideoArchiveFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_video_stream, container, false);

        mRecyclerView = v.findViewById(R.id.recycler_view);

        initRecyclerView();
        //initView(v);

        return v;
    }


    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);
        VerticalSpacingItemDecorator itemDecorator = new VerticalSpacingItemDecorator(10);
        mRecyclerView.addItemDecoration(itemDecorator);

        // Make an instance of a retrofit blob
        RetrofitControllerBlob retrofitControllerBlob = new RetrofitControllerBlob();

        // We get the desired blobs from the openpose container
        Call<Blobs> blobsCall = retrofitControllerBlob.getBlobAPI().getBlobs("openpose");

        // We make an asynchronous request to get the blobs
        blobsCall.enqueue(new Callback<Blobs>() {
            @Override
            public void onResponse(Call<Blobs> call, Response<Blobs> response) {

                // We get our blobs and store it into a list of type Blob
                List<Blob> blobList = response.body().getBlobs();

                // We count the number of blobs that have a .mp4 extension
                for (Blob i : blobList) {
                    if (i.getName().contains(".mp4")) {
                        counter++;
                    }
                }


                // We make an array of MediaObject[] depending on the number of blobs with a .mp4 extension
                MediaObject[] mediaObjects = new MediaObject[counter];

                // We go through all of our blobs and make a new MediaObject for each blob that has a .mp4 extension
                for (Blob i : blobList) {
                    if (i.getName().contains(".mp4")) {
                        mediaObjects[counterTwo] = new MediaObject(i.getName(), mediaURL + i.getName(), thumbnail, i.getDate());
                        counterTwo++;
                    }
                }

                // We turn our Array of MediaObject to an ArrayList and set our Recycler view accordingly
                ArrayList<MediaObject> mediaObjectsTwo = new ArrayList<MediaObject>(Arrays.asList(mediaObjects));
                mRecyclerView.setMediaObjects(mediaObjectsTwo);
                VideoPlayerRecyclerAdapter adapter = new VideoPlayerRecyclerAdapter(mediaObjectsTwo, initGlide());
                mRecyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<Blobs> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    private RequestManager initGlide() {

        // The default view while a video is loading in Video Archive is a black background
        // The view when there is an error is black as well, this isn't particularly important since
        // the developer will know an error has occurred when a video doesn't start playing in a reasonable time frame
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.black_background)
                .error(R.drawable.black_background);

        return Glide.with(this)
                .setDefaultRequestOptions(options);
    }


    @Override
    public void onDestroy() {
        if (mRecyclerView != null)
            mRecyclerView.releasePlayer();
        super.onDestroy();
    }
}
